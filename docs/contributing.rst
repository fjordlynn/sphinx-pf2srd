.. _contributing:

############
Contributing
############

.. todo:: Write a contributor's guide.

Fjordlynn's System Reference Document for Pathfinder Second Edition
(*Fjordlynn PF2SRD*) is an open source project every one can contribute
to.

Fjordlynn is a Sphinx_ documentation project written in RestructuredText_
that use rule entries from YAML_ files.

The entries in a YAML file are for a specific rule ``kind``
(i.e. ``skill``, ``class``, ``spell``). For each kind, Sphinx
directives_ are available for integrating the rule entry information
in the document automatically
(i.e. ``pf2srd:autoskill``, ``pf2srd:autoclass``, ``pf2srd:autospell``).
The general rule guidelines are written as prose in RestructuredText.

.. _Sphinx: https://www.sphinx-doc.org/en/master/
.. _RestructuredText: https://docutils.sourceforge.io/rst.html
.. _directives: https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
.. _YAML: https://yaml.org/


How to contribute
=================

Contributing code
-----------------

Make sure you have a GitLab account, fork the project, make your changes
and submit a Merge Request. See https://docs.gitlab.com/ee/user/project/merge_requests/
for details.

Contribute feedback
-------------------

Open a ticket with your feedback here: https://gitlab.com/fjordlynn/sphinx-pf2srd/-/issues/.


Contribution ideas
==================

Data entry
    Entire rulebooks need to be manually entered as RestructuredText and YAML.

    .. todo:: Document the file structure convention for rulebooks and YAML

Entry parsing
    Most code for parsing and structuring rule entries is good enough for now.

Documentation generation
    Each rule kind need some specific code to display its content properly.
    Some kinds are not completed and need improvement.

    .. todo:: Document the API for documentation generation

Styling and website layout
    CSS and JS need improvements to help the website feel polished

    .. todo:: List GUI improvements

Adding more tables and indices
    The code under the tables and indices needs some simplification, and
    tables must be placed and layed out properly.


Todo
====

Below is a todolist.

.. todolist::
