################
Fjordlynn PF2SRD
################

.. container:: tagline

    A simple hypertext Pathfinder Second Edition System Reference
    Document with an emphasis on hyperlinks.

    Fjordlynn's SRD aims to be a lightweight, distraction-free,
    open source and community-driven SRD that you can access even without
    an internet connection.

.. toctree::
    :maxdepth: 2

    books/index
    references

Fjordlynn's System Reference Document for Pathfinder Second Edition is a
an alternative to official or otherwise popular SRDs online that prioritize
simplicity and correctness. It also is an open source initiative you can
easily have while offline.


Acknowledgement
===============

This website used a lot of data captured by the `Pathfinder 2 Sqlite`_
project.

.. _Pathfinder 2 Sqlite: https://gitlab.com/jrmiller82/pathfinder-2-sqlite

    Compatibility with Pathfinder Second Edition requires
    Pathfinder Second Edition from Paizo Inc. See paizo.com/pathfinder
    to learn more about Pathfinder.
    Paizo Inc. does not guarantee compatibility, and does not endorse this
    product.

Pathfinder is a registered trademark of Paizo Inc.
Pathfinder Second Edition and the Pathfinder Second Edition Compatibility Logo
are trademarks of Paizo Inc.
The Pathfinder-Icons font is © 2019 Paizo Inc.
These trademarks and copyrighted works are used under the
Pathfinder Second Edition Compatibility License.
See paizo.com/pathfinder/compatibility for more information on this license.

.. toctree::
    :glob:
    :maxdepth: 2

    about/index
    contributing


Indices and tables
==================

*   :ref:`pf2srd-index`
*   :ref:`genindex`
*   :ref:`search`
