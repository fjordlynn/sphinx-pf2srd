.. _corerulebook_12_appendices:

##########
Appendices
##########

Resources
=========

.. todo:: Add a character sheet

Conditions
==========

.. pf2srd:autokind:: condition


Traits
======

.. pf2srd:autokind:: trait
