.. _corerulebook_09_playingthegame:

################
Playing the Game
################

.. todo:: Many missing sections in Chapter 9

Basic Actions
=============

.. pf2srd:autoactivity:: Escape

.. pf2srd:autoactivity:: Stride

.. pf2srd:autoactivity:: Fly
