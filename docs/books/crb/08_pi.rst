.. _corerulebook_08_pi:

#########
Fjordlynn
#########

.. important:: Product Identity

    This chapter of the :pf2srd:source:`CRB` presents an overview of the
    world where the action is happening.

    For compliance with the Open Game License, we will not describe this
    world and encourage you to buy the books instead.

    In the meantime, some filler content could be placed here as placeholders.


.. world:age:: Age of Strayed Auguries
               00001

    This is one of the major historical ages.

.. world:location:: Fjordlynn
                    00001

    This is the world we play in.
