.. _corerulebook_04_skills:

######
Skills
######


    While your character’s ability scores represent their raw talent and
    potential, skills represent their training and experience at performing
    certain tasks. Each skill is keyed to one of your character’s ability
    scores and used for an array of related actions. Your character’s expertise
    in a skill comes from several sources, including their background and
    class. In this chapter, you’ll learn about skills, their scope, and
    the actions they can be used for.

A character’s acumen in skills can come from all sorts of training, from practicing
acrobatic tricks to studying academic topics to rehearsing a performing art. When
you create your character and as they advance in level, you have flexibility as to
which skills they become better at and when. Some classes depend heavily on
certain skills—such as the alchemist’s reliance on Crafting—but for most classes,
you can choose whichever skills make the most sense for your character’s theme and
backstory at 1st level, then use their adventure and downtime experiences to inform
how their skills should improve as your character levels up.

A character gains training in certain skills at 1st level: typically two skills from
their background, a small number of predetermined skills from their class, and
several skills of your choice granted by your class. This training increases your
proficiency ranks for those skills to trained instead of untrained and lets you use
more of the skills’ actions. Sometimes you might gain training in a specific skill from
multiple sources, such as if your background granted training in Crafting and you
took the alchemist class, which also grants training in Crafting. Each time after the
first that you would gain the trained proficiency rank in a given skill, you instead
allocate the trained proficiency to any other skill of your choice.

.. pf2srd:autokind:: skill
