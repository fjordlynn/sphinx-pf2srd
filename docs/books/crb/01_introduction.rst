.. _corerulebook_01_introduction:

############
Introduction
############

    Pathfinder is a fantasy tabletop roleplaying game (RPG) where you and
    a group of friends gather to tell a tale of brave heroes and cunning
    villains in a world filled with terrifying  monsters and amazing treasures.
    More importantly, Pathfinder is a game where your character’s choices
    determine how the story unfolds.

Pathfinder adventures take place in the :world:age:`00001`, a perilous
fantasy world rife with ancient empires; sprawling city-states; and countless
tombs, dungeons, and monster lairs packed with plunder. A Pathfinder character’s
adventures might take them to forsaken underwater ruins, haunted gothic
crypts, or magical universities in jungle cities. A world of endless adventure
awaits!

What Is a Roleplaying Game?
===========================

A roleplaying game is an interactive story where one player,
the Game Master (GM), sets the scene and presents challenges,
while other players take the roles of
player characters (PCs) and attempt to overcome those challenges.
Danger comes in the form of monsters, devious traps, and the machinations
of adversarial agents, but Pathfinder also provides political schemes,
puzzles, interpersonal drama, and much, much more.

The game is typically played in a group of four to seven players,
with one of those players serving as the group’s Game Master.
The GM prepares, presents, and presides over the game’s world and story,
posing challenges and playing adversaries, allies, and bystanders alike.
As each scene leads into the next, each player contributes to the story,
responding to situations according to the personality and abilities of their character.
Dice rolls, combined with preassigned statistics, add an element of chance and determine
whether characters succeed or fail at actions they attempt.

.. sidebar:: The first rule

    The first rule of Pathfinder is
    that this game is yours. Use
    it to tell the stories you want
    to tell, be the character you
    want to be, and share exciting
    adventures with friends. If
    any other rule gets in the way
    of your fun, as long as your
    group agrees, you can alter or
    ignore it to fit your story. The
    true goal of Pathfinder is for
    everyone to enjoy themselves.


The Flow of the Game
--------------------

Pathfinder is played in sessions, during which players gather in person
or online for a few hours to play the game. A complete Pathfinder story
can be as short as a single session, commonly referred to as a “one-shot,”
or it can stretch on for multiple sessions, forming a campaign that might
last for months or even years. If the Game Master enjoys telling the story
and the players are entertained, the game can go as long as you like.

A session can be mostly action, with battles with vile beasts, escapes from
fiendish traps, and the completion of heroic quests. Alternatively, it could
include negotiating with a baron for rights to a fort, infiltrating an army
of lumbering frost giants, or bargaining with an angel for a strand of hair
required for an elixir to revive a slain friend.
Ultimately it’s up to you and your group to determine what kind of game
you are playing, from dungeon exploration to a nuanced political drama,
or anything in between.

.. sidebar:: Dice

    Pathfinder requires a set of _`polyhedral dice`.
    Each die has a different number of sides—four, six, eight, or more.
    When these dice are mentioned in the text, they’re indicated by a “d”
    followed by the number of sides on the die.
    Pathfinder uses 4-sided dice (or d4), 6-sided dice (d6),
    8-sided dice (d8), 10-sided dice (d10), 12-sided dice (d12), and
    20-sided dice (d20).
    If you need to roll multiple dice, a number before the “d” tells you
    how many. For example, “4d6” means you should roll four dice, all 6-sided.
    If a rule asks for d%, you generate a number from 1 to 100 by rolling
    two 10-sided dice, treating one as the tens place and the other as the
    ones place.

The Players
-----------

Everyone involved in a Pathfinder game is a player, including the Game Master,
but for the sake of simplicity, “player” usually refers to participants
other than the GM.
Before the game begins, players invent a history and personality for their
characters, using the rules to determine their characters’ statistics,
abilities, strengths, and weaknesses.
The GM might limit the options available during character creation,
but the limits are discussed ahead of time so everyone can create interesting
heroes.
In general, the only limits to character concepts are the players’ imaginations
and the GM’s guidelines.
During the game, players describe the actions their characters take and
roll dice, using their characters’ abilities.
The GM resolves the outcome of these actions.
Some players enjoy acting out (or roleplaying) what they do as if they were
their characters, while others describe their characters’ actions as if
narrating a story.
Do whatever feels best! If this is your first experience with a roleplaying
game, it is recommended that you take on the role of a player to familiarize
yourself with the rules and the world.

The Game Master
---------------

While the other players create and control their characters,
the Game Master (or GM) is in charge of the story and world.
The GM describes all the situations player characters experience in an
adventure, considers how the actions of player characters affect the story,
and interprets the rules along the way.
The GM can create a new adventure
—crafting a narrative, selecting monsters, and assigning treasure on their own—
or they can instead rely on a published adventure, using it as a basis for
the session and modifying it as needed to accommodate their individual players
and the group’s style of play.
Some even run games that combine original and published content, mixed
together to form a new narrative.
Being the GM is a challenge, requiring you to adjudicate the rules, narrate
the story, and juggle other responsibilities.
But it can also be very rewarding and worth all the work required to run
a good game. If it is your first time running a game, remember that the
only thing that matters is that everyone has a good time, and that includes
you.
Everything else will come naturally with practice and patience.


Gaming Is for All
-----------------

Whether you are the GM or a player, participating in a tabletop roleplaying
game includes a social contract: everyone has gathered together to have
fun telling a story.
For many, roleplaying is a way to escape the troubles of everyday life.
Be mindful of everyone at the table and what they want out of the game,
so that everyone can have fun.
When a group gathers for the first time, they should talk about what they
hope to experience at the table, as well as any topics they want to avoid.
Everyone should understand that elements might come up that make some players
feel uncomfortable or even unwelcome, and everyone should agree to respect
those boundaries during play. That way, everyone can enjoy the game together.

Pathfinder is a game for everyone, regardless of their age, gender,
race or ethnicity, religion, sexual orientation, or any other identities
and life experiences. It is the responsibility of all of the players, not
just the GM, to make sure the table is fun and welcoming to all.


Tools of Play
-------------

In addition to this book, there are a few things you will need to play Pathfinder.
These supplies can be found at your local hobby shop or online at paizo.com.

Character Sheet
    Each player will need a character sheet to create their character and
    to record what happens to that character during play.
    You can find a character sheet in the back of this book and online
    as a free pdf.

Dice:
    The players and GM will need at least one set of polyhedral dice,
    although most participants bring their own.
    Six-sided dice are quite common, but all the dice in the set can be
    found at hobby game stores or online.
    See the `Dice Sidebar <polyhedral-dice>`_ for more on the different kinds
    of dice and how they are discussed in the text.

Adventure
    Every table needs an adventure to play,
    whether it’s designed by the GM or found in a published
    resource. You can find a variety of exciting adventures
    and even entire Adventure Path campaigns at
    `paizo.com <https://paizo.com>`__.

Bestiary:
    From terrifying dragons to mischievous
    gremlins, monsters are a common threat that the PCs
    might face, and each type has its own statistics and
    abilities. These can be found in the
    :pf2srd:source:`Bestiary`, an absolutely invaluable book
    for GMs. Monster statistics can also be found online for free at
    `paizo.com/prd <https://paizo.com/prd>`__.

Maps and Miniatures:
    The chaos of combat can be
    difficult to imagine, so many groups use maps to represent
    the battlefield. These maps are marked with a 1-inch grid,
    and each square represents 5 feet in the game. Miniatures
    and illustrated tokens called pawns are used to represent
    the characters and the adversaries they face.

Additional Accessories:
    There are a number of
    additional accessories you can add to your game to
    enhance the experience, including tools that help you
    track turns in combat, decks of cards for referencing
    common rules, digital character-creation tools, and even
    background music and sound-effect sets.

Basics of Play
==============

Before creating your first character or adventure, you
should understand a number of basic concepts used in
the game. New concepts are presented in bold to make
them easy to find, but this chapter is only an introduction
to the basics of play. The complete game rules are defined
in later chapters, and the :ref:`glossary` and k:ref:`pf2srd-index`
will help you find specific rules you need.

Defining Characters
-------------------

In Pathfinder, the players take on the role of **player characters** (**PCs**),
while the Game Master portrays **nonplayer characters** (**NPCs**) and
**monsters**.
While PCs and NPCs are both important to the story, they serve very
different purposes in the game. PCs are the protagonists—
the narrative is about them—while NPCs and monsters
are allies, contacts, adversaries, and villains. That said,
PCs, NPCs, and monsters share several characteristics.

**Level** is one of the most important statistics of the game,
as it conveys the approximate power and capabilities of
every individual creature. PCs range in level from 1st, at
the start of the character’s adventuring career, to 20th,
the very height of power. As the characters overcome
challenges, defeat foes, and complete adventures, they
accumulate **Experience Points** (**XP**). Every time a character
amasses 1,000 XP, they go up a level, gaining new abilities
so they can take on even greater challenges. A 1st-level PC
might face off against a giant rat or a group of bandits, but
at 20th level, that same character might be able to bring
ruin to an entire city with a single spell.

In addition to level, characters are defined by **ability scores**,
which measure a character’s raw potential and are used to calculate most
of their other statistics.
There are six ability scores in the game.
**Strength** represents a character’s physical might,
while **Dexterity** represents agility and the ability to avoid danger.
**Constitution** indicates a character’s overall health and well-being.
**Intelligence** represents raw knowledge and problem-solving ability,
while **Wisdom** measures a character’s insight and the ability to evaluate
a situation.
Finally, **Charisma** indicates charm, persuasiveness, and force of personality.
Ability scores for ordinary folk range from as low as 3 to as high as 18,
with 10 representing average human capabilities.
High-level characters can have ability scores that range much higher than 18.

An ability score that’s above the average increases your chance of success
at tasks related to the ability score, while those below the average decrease
your chance. This adjustment is called an **ability modifier**.

Your player character is also defined by some key choices you make.
The first choice is a PC’s **ancestry**, representing the character’s
parents and heritage, such as human, elf, or goblin.
Next up is the PC’s **background**, which describes their upbringing, from
lowly street urchin to wealthy noble.
Finally, and most importantly, a PC’s **class** defines the majority of
their aptitudes and abilities, like a wizard’s command of powerful
arcane spells or a druid’s power to transform into a fearsome beast!

In addition to these key choices, player characters also have a number of feats—
individual abilities selected during character creation and as the character increases
in level. Every feat has a type to denote where its explanation can be found (for
example, elf feats can be found in the elf ancestry) and its theme (wizard feats, for
example, grant abilities that deal with spells). Finally, characters have skills that
measure their ability to hide, swim, bargain, and perform other common tasks.

.. sidebar:: The World as a Participant

    Aside from characters and monsters, the world of Pathfinder itself can
    be a force at the table and in the narrative. While the presence of
    the larger world can sometimes be an obvious hazard, such as when a
    powerful storm lashes the countryside, the world can also act in subtle,
    small ways. Traps and treasures are just as important in many tales
    as cunning beasts. To help you understand these game elements, many
    of them use the same characteristics as characters and monsters.
    For example, most environmental hazards have a level, which indicates
    how dangerous they are, and the level of a magic item gives you a sense
    of its overall power and impact on a story.

Creating a Narrative
--------------------

Characters and their choices create the story of Pathfinder, but how they
interact with each other and the world around them is governed by rules.
So, while you might decide that your character undertakes an epic journey
to overcome terrifying foes and make the world a safer place, your character’s
chance of success is determined by their abilities, the choices you make,
and the roll of the dice.

The GM determines the premise and background of most adventures, although
character histories and personalities certainly play a part. Once a game
session begins, the players take turns describing what their characters
attempt to do, while the GM determines the outcome, with the table working
together toward a specific goal. The GM also describes the environment,
other characters’ actions, and events. For example, the GM might announce
that the characters’ hometown is under attack by marauding trolls.
The characters might track the trolls to a nearby swamp
—only to discover that the trolls were driven from their swamp by a fearsome dragon!
The PCs then have the choice of taking on an entire tribe of trolls, the
dragon, or both. Whatever they decide, their success depends on their choices
and the die rolls they make during play.

A single narrative—including the setup, plot, and conclusion—is called an
adventure. A series of adventures creates an even larger narrative, called
a campaign. An adventure might take several sessions to complete, whereas
a campaign might take months or even years!

Playing the Game
================

In a Pathfinder game, three modes of play determine the
pacing of each scene in the story. Most of your character’s
time is spent in **exploration**, uncovering mysteries, solving
problems, and interacting with other characters. The
:world:age:`00001` abounds with danger, however, and
characters often find themselves in an **encounter**, fighting
savage beasts and terrifying monsters. Finally, time moves
quickly when the characters enjoy **downtime**, a respite
from the world’s troubles and a chance to rest and train
for future expeditions. Throughout an adventure, game
play moves between these three modes many times, as
needed for the story. The more you play the game, the
more you’ll see that each mode has its own play style, but
moving from mode to mode has few hard boundaries.

During the game, your character will face situations
where the outcome is uncertain. A character might need
to climb a sheer cliff, track down a wounded chimera, or
sneak past a sleeping dragon, all of which are dangerous
tasks with a price for failure. In such cases, the acting
character (or characters) will be asked to attempt a **check**
to determine whether or not they succeed. A check is
usually made by rolling a single 20-sided die (a d20) and
adding a number based on the relevant ability. In such
cases, rolling high is always good.

Once a check is rolled, the GM compares the result to a
target number called the **difficulty class** (**DC**) to determine
the outcome. If the result of the check is equal to or greater
than the DC, the check is successful. If it is less, the check is
a failure. Beating the DC by 10 or more is referred to as a
**critical success**, which usually grants an especially positive
outcome. Similarly, failing the check by 10 or more is a
**critical failure** (sometimes called a fumble). This sometimes
results in additional negative effects. You also often score a
critical success by rolling a 20 on the die when attempting
a check (before adding anything). Likewise, rolling a 1 on
the die when attempting a check often results in a critical
failure. Note that not all checks have a special effect on a
critical success or critical failure and such results should
be treated just like an ordinary success or failure instead.

For example, in pursuit of the wounded chimera, your
character might find the path blocked by a fast-moving
river. You decide to swim across, but the GM declares this
a dangerous task and asks you to roll an Athletics skill
check (since swimming is covered by the Athletics skill). On
your character sheet, you see that your character has a +8
modifier for such checks. Rolling the d20, you get an 18, for
a total of 26. The GM compares this to the DC (which was
16) and finds that you got a critical success (since the result
exceeded the DC by 10). Your character swims quickly
across the river and continues the pursuit, drenched but
unharmed. Had you gotten a result less than 26 but equal
to or greater than 16, your character would have made it
halfway across the river. Had your result been less than 16,
your character might have been swept downriver or, worse,
been pulled under the current and begun to drown!

Checks like this are the heart of the game and are rolled
all the time, in every mode of play, to determine the outcome
of tasks. While the roll of the die is critical, the statistic you
add to the roll (called a **modifier**) often makes the difference
between success and failure. Every character is made up
of many such statistics governing what the character is
good at, each consisting of a relevant ability modifier plus
a **proficiency** bonus, and sometimes modified further by
other factors, such as bonuses or penalties from gear, spells,
feats, magic items, and other special circumstances.

Proficiency is a simple way of assessing your character’s
general level of training and aptitude for a given task. It
is broken into five different ranks: **untrained**, **trained**,
**expert**, **master**, and **legendary**. Each rank grants a different
proficiency bonus. If you’re untrained at a statistic, your
proficiency bonus is +0—you must rely solely on the raw
potential of your ability modifier. If your proficiency rank
for a statistic is trained, expert, master, and legendary, your
bonus equals your character’s level plus another number
based on the rank (2, 4, 6, and 8, respectively). Proficiency
ranks are part of almost every statistic in the game.


Exploration
-----------

Most of the time, your character will explore the world,
interact with characters, travel from place to place, and
overcome challenges. This is called exploration. Game play
is relatively free-form during exploration, with players
responding to the narrative whenever they have an idea of
what to do next. Leaving town via horseback, following the
trail of a marauding orc tribe, avoiding the tribe’s scouts, and
convincing a local hunter to help in an upcoming fight are
all examples of things that might occur during exploration.

Throughout this mode of play, the GM asks the players
what their characters are doing as they explore. This is
important in case a conflict arises. If combat breaks out,
the tasks the PCs undertook while exploring might give
them an edge or otherwise inform how the combat begins.

Encounters
----------

In the course of your adventures, there will be times when
a simple skill check is not enough to resolve a challenge—
when fearsome monsters stand in your character’s way
and the only choice is to do battle. In Pathfinder, this is
called an encounter. Encounters usually involve combat,
but they can also be used in situations where timing is
critical, such as during a chase or when dodging hazards.

While exploration is handled in a free-form manner,
encounters are more structured. The players and GM roll
**initiative** to determine who acts in what order. The encounter
occurs over a number of **rounds**, each of which is equal to
about 6 seconds of time in the world of the game. During
a round, each participant takes a **turn**. When it’s your
turn to act, you can use up to three **actions**. Most simple
things, such as drawing a weapon, moving a short distance,
opening a door, or swinging a sword, use a single action to
perform. There are also **activities** that use more than a single
action to perform; these are often special abilities from your
character’s class and feats. One common activity in the game
is casting a spell, which usually uses two actions.

**Free actions**, such as dropping an object, don’t count
toward the three actions you can take on your turn. Finally,
each character can use up to one **reaction** during a round.
This special type of action can be used even when it’s not
your turn, but only in response to certain events, and only
if you have an ability that allows it. Rogues, for example,
can select a feat that lets them use their reaction to dodge
an incoming attack.

Attacking another creature is one of the most common
actions in combat, and is done by using the **Strike** action.
This requires an attack roll—a kind of check made against
the **Armor Class** (**AC**) of the creature you’re attacking.
Strikes can be made using weapons, spells, or even parts
of a creature’s body, like a fist, claw, or tail. You add a
modifier to this roll based on your proficiency rank with
the type of attack you’re using, your ability scores, and
any other bonuses or penalties based on the situation.
The target’s AC is calculated using their proficiency rank
in the armor they’re wearing and their Dexterity modifier.
An attack deals damage if it hits, and rolling a critical
success results in the attack dealing double damage!

You can use more than one Strike action on your turn,
but each additional attack after the first becomes less
accurate. This is reflected by a **multiple attack penalty**
that starts at –5 on the second attack, but increases to –10
on the third. There are many ways to reduce this penalty,
and it resets at the end of your turn.

If your character finds themself the target of a magical
*lightning bolt* or the freezing breath of a fearsome white
dragon, you will be called on to attempt a **saving throw**,
representing your character’s ability to avoid danger or
otherwise withstand an assault to their mind or body. A
saving throw is a check attempted against the DC of the
spell or special ability targeting your character. There are
three types of saving throws, and a character’s proficiency
in each says a great deal about what they can endure. A
**Fortitude** saving throw is used when your character’s health
or vitality is under attack, such as from poison or disease.
A **Reflex** saving throw is called for when your character
must dodge away from danger, usually something that
affects a large area, such as the scorching blast of a fireball
spell. Finally, a **Will** saving throw is often your defense
against spells and effects that target your character’s mind,
such as a charm or confusion spell. For all saving throws,
a success lessens the harmful effect, and scoring a critical
success usually means your character escapes unscathed.

.. pf2srd:autosavingthrow:: Fortitude

.. pf2srd:autosavingthrow:: Reflex

.. pf2srd:autosavingthrow:: Will

Attacks, spells, hazards, and special abilities frequently
either deal **damage** to a character or impose one or more
**conditions**—and sometimes both. Damage is subtracted
from a creature’s **Hit Points** (**HP**)—a measure of health—
and when a creature is reduced to 0 HP, it falls unconscious
and may die! A combat encounter typically lasts until one
side has been defeated, and while this can mean retreat or
surrender, it most often happens because one side is dead or
dying. Conditions can hinder a creature for a time, limiting
the actions they can use and applying penalties to future
checks. Some conditions are even permanent, requiring a
character to seek out powerful magic to undo their effects.

Downtime
--------

Characters don’t spend every waking moment
adventuring. Instead, they recover from wounds, plan
future conquests, or pursue a trade. In Pathfinder, this is
called downtime, and it allows time to pass quickly while
characters work toward long-term tasks or objectives.
Most characters can practice a trade in downtime,
earning a few coins, but those with the right skills can
instead spend time crafting, creating new gear or even
magic items. Characters can also use downtime to retrain,
replacing one character choice with another to reflect their
evolving priorities. They might also research a problem,
learn new spells, or even run a business or kingdom!

Key Terms
---------

There are a number of important terms that you’ll need
to know as you create your first character or adventure.
Some of the most important terms mentioned on previous
pages are also included here for reference.

.. glossary::

    Ability Score
        Each creature has six ability scores: Strength, Dexterity,
        Constitution, Intelligence, Wisdom, and Charisma. These
        scores represent a creature’s raw potential and basic
        attributes. The higher the score, the greater the creature’s
        potential in that ability. Ability scores are described in full
        later in this chapter.

    Alignment
        Alignment represents a creature’s fundamental moral and
        ethical attitude.

    Ancestry
        An ancestry is the broad family of people that a character
        belongs to. Ancestry determines a character’s starting Hit
        Points, languages, senses, and Speed, and it grants access
        to ancestry feats. Ancestries can be found in Chapter 2.

    Armor Class
    AC
        All creatures in the game have an Armor Class. This score
        represents how hard it is to hit and damage a creature. It
        serves as the Difficulty Class for hitting a creature with
        an attack.

    Attack
        When a creature tries to harm another creature, it makes
        a Strike or uses some other attack action. Most attacks
        are Strikes made with a weapon, but a character might
        Strike with their fist, grapple or shove with their hands,
        or attack with a spell.

    Background
        A background represents what a character experienced
        before they took up the life of an adventurer. Each
        background grants a feat and training in one or more
        skills. You can read more about backgrounds in Chapter 2.

    Bonuses and Penalties
        Bonuses and penalties apply to checks and certain
        statistics. There are several types of bonuses and penalties.
        If you have more than one bonus of the same type, you
        use only the highest bonus. Likewise, you use only the
        worst penalty of each type.

    Class
        A class represents the adventuring profession chosen by
        a character. A character’s class determines most of their
        proficiencies, grants the character Hit Points each time
        they gain a new level, and gives access to a set of class
        feats. Classes appear in Chapter 3.

    Condition
        An ongoing effect that changes how a character can act, or
        that alters some of their statistics, is called a condition. The
        rules for the basic conditions used in the game can be found
        in the Conditions Appendix at the back of this book.

    Currency
        The most common currencies in the game are gold pieces
        (gp) and silver pieces (sp). One gp is worth 10 sp. In
        addition, 1 sp is worth 10 copper pieces (cp), and 10 gp
        are worth 1 platinum piece (pp). Characters begin play
        with 15 gp (or 150 sp) to spend on equipment.

    Feat
        A feat is an ability you can select for your character due to
        their ancestry, background, class, general training, or skill
        training. Some feats grant the ability to use special actions.

    Game Master
    GM
        The Game Master is the player who adjudicates the rules
        and narrates the various elements of the Pathfinder story
        and world that the other players explore.

    Golarion
        Pathfinder is set on the planet Golarion during the Age of
        Lost Omens. It is an ancient world with a rich diversity
        of people and cultures, exciting locations to explore, and
        deadly villains. More information on the
        :world:age:`00001`,
        the world of Golarion, and its deities can be found in Chapter 8.

    Hit Points
    HP
        Hit Points represent the amount of punishment a creature
        can take before it falls unconscious and begins dying.
        Damage decreases Hit Points on a 1-to-1 basis, while
        healing restores Hit Points at the same rate.

    Initiative
        At the start of an encounter, all creatures involved roll for
        initiative to determine the order in which they act. The
        higher the result of its roll, the earlier a creature gets to act.
        Initiative and combat are described in Chapter 9.

    Level
        A level is a number that measures something’s overall
        power. Player characters have a level, ranging from 1st
        to 20th, representing their level of experience. Monsters,
        NPCs, hazards, diseases, and poisons have levels ranging
        from –1 to 30 that measure the danger they pose. An item’s
        level, usually within the range of 0 to 20 but sometimes
        higher, indicates its power and suitability as treasure.

        Spells have levels ranging from 1st to 10th, which
        measure their power; characters and monsters can usually
        cast only a certain number of spells of any given level.

    Nonplayer Character (NPC)
        A nonplayer character, controlled by the GM, interacts
        with players and helps advance the story.

    Perception
        Perception measures your character’s ability to notice
        hidden objects or unusual situations, and it usually
        determines how quickly the character springs into action
        in combat. It is described in full in Chapter 9.

    Player Character
    PC
        This is a character created and controlled by a player.

    Proficiency
        Proficiency is a system that measures a character’s aptitude
        at a specific task or quality, and it has five ranks: untrained,
        trained, expert, master, and legendary. Proficiency gives
        you a bonus that’s added when determining the following
        modifiers and statistics: AC, attack rolls, Perception, saving
        throws, skills, and the effectiveness of spells. If you’re
        untrained, your proficiency bonus is +0. If you’re trained,
        expert, master, or legendary, your proficiency bonus equals
        your level plus 2, 4, 6, or 8, respectively.

    Rarity
        Some elements of the game have a rarity to denote how
        often they’re encountered in the game world. Rarity
        primarily applies to equipment and magic items, but
        spells, feats, and other rules elements also have a rarity.
        If no rarity appears in the traits of an item, spell, or other
        game element, it is of common rarity. Uncommon items are
        available only to those who have special training, grew up
        in a certain culture, or come from a particular part of the
        world. Rare items are almost impossible to find and are
        usually given out only by the GM, while unique ones are
        literally one-of-a-kind in the game. The GM might alter the
        way rarity works or change the rarity of individual items
        to suit the story they want to tell.

    Roleplaying
        Describing a character’s actions, often while acting from
        the perspective of the character, is called roleplaying. When
        a player speaks or describes action from the perspective of
        a character, they are “in character.”

    Round
        A round is a period of time during an encounter in which
        all participants get a chance to act. A round represents
        approximately 6 seconds in game time.

    Saving Throw
    Save

        When a creature is subject to a dangerous effect that must
        be avoided, it attempts a saving throw to mitigate the effect.
        You attempt a saving throw automatically—you don’t
        have to use an action or a reaction. Unlike for most checks,
        the character who isn’t acting rolls the d20 for a saving
        throw, and the creature who is acting provides the DC.

        There are three types of saving throws: Fortitude (to
        resist diseases, poisons, and physical effects), Reflex (to
        evade effects a character could quickly dodge), and Will
        (to resist effects that target the mind and personality).

    Skill
        A skill represents a creature’s ability to perform certain
        tasks that require instruction or practice. Skills are fully
        described in Chapter 4. Each skill includes ways anyone
        can use that skill even if untrained, as well as uses that
        require a character to be trained in the skill.

    Speed
        Speed is the distance a character can move using a single
        action, measured in feet.

    Spell
        Spells are magical effects created by performing mystical
        incantations and gestures known only to those with special
        training or inborn abilities. Casting a spell is an activity
        that usually uses two actions. Each spell specifies what it
        targets, the actions needed to cast it, its effects, and how
        it can be resisted. If a class grants spells, the basics of that
        ability are provided in the class description in Chapter 3,
        while the spells themselves are detailed in Chapter 7.

    Trait
        A trait is a keyword that conveys additional information
        about a rules element, such as a school of magic or rarity.
        Often, a trait indicates how other rules interact with an
        ability, creature, item, or another rules element that has
        that trait. All the traits used in this book appear in the
        Glossary and Index beginning on page 628.

    Turn
        During the course of a round, each creature takes a single
        turn according to initiative. A creature can typically use
        up to three actions during its turn.


Example of Play
---------------

The following example is presented to give you a better idea
of how the game of Pathfinder is played. In this adventure,
Erik is the GM. Lyz is playing Valeros, a daring human
fighter, James is playing Merisiel, a deadly elven rogue, and
Judy is taking on the role of Kyra, a fiery human cleric of
Sarenrae. The group has just defeated a horde of undead
and is making its way into an ancient mausoleum.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   The entrance to the crypt stands before you,
            a set of crumbling stairs leading down into
            darkness. A terrible smell issues forth from the
            doorway—the stench of old, rotted flesh.
    *   *   Lyz:
        *   I’m not afraid of a foul stink! I draw my sword
            and ready my shield.
    *   *   Judy:
        *   The light of Sarenrae will guide us. I cast my
            light spell on my religious symbol.
    *   *   Erik:
        *   All right, a glowing radiance spills forth,
            illuminating the stairs. They appear to go
            down only about 10 feet before opening up
            into a chamber. Puddles of stagnant water fill
            the cracks between uneven stone tiles.
    *   *   James:
        *   I should go first to make sure it’s safe. I’m going
            to draw my rapier and carefully go down the
            stairs, looking for traps as I go.
    *   *   Erik:
        *   Sure, but looking for traps is a secret check, so I’ll
            roll for you. What’s your Perception modifier?
    *   *   James:
        *   I have a +5.

.. end of table

.. epigraph::

    Erik rolls a d20 behind his GM screen, hidden from the
    players’ view, and gets a 17 on the die for a total of 22,
    more than enough to find the trip wire on the third step.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   Your caution pays off! You spot a thin wire
            located at ankle height just above the third stair.
    *   *   James:
        *   I point it out to the others and head down.
    *   *   Lyz:
        *   I follow right behind Merisiel, avoiding the wire
            but otherwise keeping an eye out for danger.
    *   *   Judy:
        *   Me too.
    *   *   Erik:
        *   Okay! You make it down the stairs to find
            yourselves in a crypt. Ancient wood coffins are
            arranged around the room, covered in cobwebs
            and dust. Directly ahead, on a raised dais, is
            a stone casket adorned with wicked-looking
            symbols. You can tell that it was once wrapped
            in iron chains, but now twisted links are scattered
            around the room, along with chunks of what
            must have been the casket’s lid. From the damage,
            it looks like it was shattered from within!
    *   *   Judy:
        *   Sarenrae protect us. I draw my blade and
            advance—I want a better look at those symbols.
    *   *   Lyz:
        *   I’ll keep pace with her. I don’t like the look of this.
    *   *   James:
        *   I think I’ll stay back here and hide behind one
            of the coffins.
    *   *   Erik:
        *   Merisiel takes cover while the two of you
            advance. As you draw near, the stench of rot
            grows stronger until it’s almost overpowering.
            Suddenly you see the source of the horrid odor.
            Rising up out of the casket is a nightmarish
            dead thing. It might have once been a human,
            but it’s hard to tell from its withered body. Its
            flesh is the color of a new bruise, pulled so
            tight across its bones that it has split in places.
            It’s hairless, with pointed ears, but worst of all,
            its mouth is lined with tiny, sharp teeth and its
            tongue is entirely too long.
    *   *   Lyz:
        *   So, not a friend?
    *   *   Erik:
        *   Most certainly not. It looks poised to leap at
            you and attack. Roll for initiative! Valeros and
            Kyra need to roll Perception, while Merisiel
            should roll Stealth.

.. epigraph::

    Everyone rolls for their initiative. Lyz rolls a 2 for Valeros,
    getting a total of 8. Judy rolls better for Kyra, getting
    a total of 14. James uses Stealth for Initiative, because
    Merisiel was hiding at the start of the fight, and rolls a
    17 for a total of 25! Erik rolls for the undead creature,
    getting a 12. Erik records all these totals, putting the
    characters in order from highest to lowest.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   Looks like Merisiel gets to act first. Whatever
            that thing is, you’re pretty sure it doesn’t know
            you are there.
    *   *   James:
        *   Awesome! For my first action, I want to draw a
            dagger. For my second, I want to move closer.
    *   *   Erik:
        *   You can get to within 15 feet of it with one
            Stride action.
    *   *   James:
        *   Perfect. For my final action, I’m going to throw
            my dagger at it!

.. epigraph::

    James rolls a 13 and adds 8, due to Merisiel’s skill at
    thrown daggers, for a total of 21, but the range means
    he takes a –2 penalty for a result of 19. Erik consults his
    notes to learn that the monster has an AC of 18.


.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   That’s a hit! Go ahead and roll damage.
    *   *   James:
        *   Okay, and I get to add extra damage due to
            sneak attack.

.. epigraph::

    Rogues have the ability to deal extra damage to foes that
    haven’t acted yet in an encounter. This extra damage also
    applies to attacks against enemies that are distracted. James
    rolls 1d4 for the dagger and 1d6 for the sneak attack, and
    he adds 4 for Merisiel’s Dexterity, getting a total of 9.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   It hisses as the blade sinks into its shoulder. That
            looks like it hurt, but the undead thing doesn’t
            appear to be slowing down. James, that was all
            three of your actions. Next up is Kyra!
    *   *   Judy:
        *   I think this is undead. What do I know about it?
    *   *   Erik:
        *   You use an action to recall your training about
            the living dead. Give me a Religion skill check.

.. epigraph::

    Judy rolls a 16, adding Kyra’s +8 with Religion to get a
    total of 24.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   At first, you thought this thing might be a
            ghoul, which is a type of undead that feasts
            on the flesh of the dead, but the terrible smell
            reveals the truth. This thing is a ghast, a more
            powerful type of ghoul. You are pretty sure
            that its stench can make you sick and that its
            claws can paralyze you with a touch.
    *   *   Judy:
        *   This is bad. I am going to spend my last two
            actions to cast bless. It gives anyone next to me
            a +1 bonus to attack rolls.

.. epigraph::

    Casting this spell is an activity that requires two actions
    to complete, and it has two components. The complex
    gestures needed to invoke the spell are the somatic
    component, and Kyra’s prayers to her deity are the
    verbal component.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *  Okay! The ghast leaps from the casket straight
            toward Merisiel. The stench of its rotting
            body is absolutely horrific up close. Attempt a
            Fortitude save!

.. epigraph::

    James rolls an 8, for a total of 14.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   Not quite enough—you gain the sickened 1
            condition, which is going to give you a –1
            penalty to most of your d20 rolls. Next, it
            lunges at you, trying to bite you!
    *   *   James:
        *   Oh no! I use my reaction to nimbly dodge out
            of the way.

.. epigraph::

    Erik rolls an attack roll for the ghast, getting an 9 on the
    die. Looking at the monster’s statistics, he adds 11 for a
    total of 20. Merisiel’s AC is normally 19, but the Nimble
    Dodge feat lets her use her reaction to increase her AC by 2
    against a single attack. In this case, it turns the ghast’s attack
    into a miss.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   Does a 20 hit you?
    *   *   James:
        *   Nope, just missed!
    *   *   Erik:
        *   You twist away from the ghast as its tongue
            leaves a slimy film on your armor. With its final
            action, the undead menace lashes out at you
            with its claw.

.. epigraph::

    Erik rolls a second attack with the ghast, this time with its
    claw. Normally this attack would take a –5 multiple attack
    penalty, but since the claw has the agile trait, the penalty is
    only –4. He rolls a 19 on the die, adds 11 for the ghoul’s
    attack modifier and subtracts 4, for a total of 26.


.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   You may have dodged the ghast’s bite, but the
            thing’s bony claw rakes across your face!

.. epigraph::

    Erik knows this is a hit and rolls the ghast’s claw damage,
    getting a total of 8.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   Take 8 points of damage, and I need you to
            attempt a Fortitude saving throw as a numbing
            sensation spreads from the wound.

.. epigraph::

    James rolls a Fortitude saving throw. He gets a 4 on the
    die, and after adding his bonus and the penalty from the
    sickened condition, it comes out to only a 9.

.. list-table::
    :stub-columns: 1

    *   *   James:
        *   This isn’t my day. I don’t suppose a 9 is good
            enough?
    *   *   Erik:
        *   I am afraid not. You are paralyzed!

.. epigraph::

    Erik notes that Merisiel is paralyzed, making her unable
    to act, but she will get a new saving throw at the end of
    each of her turns to shake off the effect.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   A dry, creaking laugh escapes the ghast’s curled
            lips, but that’s the end of its turn. Valeros, you
            are the last one to act this round.
    *   *   Lyz:
        *   About time, too! I raise my shield and use my
            final two actions to make a Sudden Charge!

.. epigraph::

    Sudden Charge is a fighter feat that lets Valeros move up
    to twice his Speed and attack at the end of his movement,
    all for only two actions.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   As you draw near, the smell is horrific. Attempt
            a Fortitude save.

.. epigraph::

    After rolling, Lyz gets a 19 on the Fortitude save.

.. list-table::
    :stub-columns: 1

    *   *   Erik:
        *   You fight off the nausea from this thing’s
            stench. Make your attack roll.

.. epigraph::

    Lyz rolls the die and it comes up a 20.

.. list-table::
    :stub-columns: 1

    *   *   Lyz:
        *   I got a 20! That must be a critical success!
    *   *   Erik:
        *   Your blade hits the vile creature right in the
            neck, dealing double damage!

.. epigraph::

    Lyz rolls a 5 on her d8, then adds 4 because of Valeros’s
    Strength modifier. Because it is a critical success, she then
    doubles the total.

.. list-table::
    :stub-columns: 1

    *   *   Lyz:
        *   A mighty 18 damage! That surely had to kill it!
    *   *   Erik:
        *   I’m afraid not. Black ichor runs from the deep
            wound on its neck, but it only turns to look at
            you. You can see burning hatred in its eyes! Uh-oh.

.. epigraph::

    That is the end of the first round of combat. The second
    round begins immediately after this, using the same
    initiative order as before. The fight is far from over...

Using This Book
===============

While this chapter is here to teach you the basics of
Pathfinder, the rest of this rulebook serves as a reference
manual during play, and it is organized to make finding
the rule you need as easy as possible. Rules are grouped
together in chapters, with the early chapters focusing on
character creation. The last two chapters contain rules
for GMs, with advice on how to run a game of Pathfinder
and a rich array of treasure. The following is a summary
of what you can expect to find in each chapter.

Chapter 1: :ref:`corerulebook_01_introduction`
----------------------------------------------

This introduction is designed to help you understand the
basics of Pathfinder. This chapter also includes the rules
for building and leveling up a character. The chapter ends
with an example of building a 1st-level character.

Chapter 2: :ref:`corerulebook_02_ancestries&backgrounds`
--------------------------------------------------------

The rules for the most common ancestries in the :world:age:`00001`
are in this chapter, including their ancestry feat options.
Backgrounds are at the end of this chapter, along with a section about
languages, as these are most often influenced by your choice of ancestry.

Chapter 3: :ref:`corerulebook_03_classes`
-----------------------------------------

This chapter contains the rules for all 12 classes. Each
class entry includes guidelines on playing the class, rules
for building and advancing a character of that class,
sample builds, and all of the class feats available to
members of that class. This chapter also includes rules
for animal companions and familiars, which can be
acquired by members of several different classes. At the
end of this chapter are the rules for archetypes—special
options available to characters as they increase in level.
These rules allow a character to dabble in the abilities of
another class or concept.

Chapter 4: :ref:`corerulebook_04_skills`
----------------------------------------

The rules for using skills are presented in this chapter,
and they detail what a character can do with a given
skill, based on that character’s proficiency rank.
Ancestry, background, and class can define some of
a character’s skill proficiencies, and each character
can also select a few additional skills to reflect their
personality and training.

Chapter 5: :ref:`corerulebook_05_feats`
---------------------------------------

As a character advances in level, they gain additional
feats to represent their growing abilities. General feats
and skill feats (which are a subset of general feats) are
presented in this chapter.

Chapter 6: :ref:`corerulebook_06_equipment`
-------------------------------------------

Armor, weapons, and other gear can all be found in this
chapter, along with the price for services, cost of living,
and animals (such as horses, dogs, and pack animals).

Chapter 7: :ref:`corerulebook_07_spells`
----------------------------------------

This chapter starts with rules for casting spells,
determining their effects, and getting rid of foes’ spells
(called counteracting). After that, the spell lists for
each spellcasting tradition are included, making it easy
to quickly find spells by their level. Next are rules for
every spell, presented in alphabetical order. Following
the spell descriptions are all of the focus spells—special
spells granted by specific class abilities and feats. While
most spells appear on multiple spell lists, focus spells
are granted only to members of a specific class and are
grouped together by class for ease of reference. Finally, at
the end of the chapter are rules for rituals, complicated
and risky spells that any character can cast.

Chapter 8: :ref:`corerulebook_08_pi`
------------------------------------

The setting of :world:location:`00001` is described in this chapter,
including a brief overview of the world and its people,
followed by a timeline of events. Most importantly,
characters who venerate a deity should look to this
chapter to find the rules associated with their faith.

Chapter 9: :ref:`corerulebook_09_playingthegame`
------------------------------------------------

This important chapter contains the universal rules
needed to play Pathfinder, including rules for the various
modes of play, the basic actions that every character can
perform, the rules for combat, and the rules for death and
dying. Every player should be familiar with this chapter,
especially the GM.

Chapter 10: :ref:`corerulebook_10_gamemastering`
------------------------------------------------

Packed full of guidelines and advice, this chapter helps
Game Masters tell an interesting and compelling story.
It also includes advice on creating a fun and encouraging
game space and guides for empowering players to create
characters they want to play. This chapter also includes
rules that are particularly important for the GM to know,
such as rules dealing with traps, environmental dangers,
and afflictions (such as curses, diseases, and poisons), as
well as guidance on setting DCs and handing out rewards
to player characters.

Chapter 11: :ref:`corerulebook_11_crafting&treasure`
----------------------------------------------------

The treasures characters find during their adventures
take many forms, from gold and gemstones to powerful
magical weapons. This chapter details guidelines for
distributing treasure to characters, as well as descriptions
of hundreds of magic items. This chapter also contains
the rules for alchemical items.

:ref:`corerulebook_12_appendices`
---------------------------------

The back of this book has an appendix with the rules
for all of the conditions that you will find in the game.
This section also includes a blank character sheet, and an
index with a comprehensive glossary of common terms
and traits that you’ll encounter in the game.


Format of Rules Elements
========================

.. todo:: Adjust for the website

Throughout this rulebook, you will see formatting standards that might look
a bit unusual at first.
Specifically, the game’s rules are set apart in this text using specialized
capitalization and italicization.
These standards are in place to make this book rules elements easier to recognize.
The names of specific statistics, skills, feats, actions, and some other
mechanical elements in Pathfinder are capitalized.
This way, when you see the statement "a Strike targets Armor Class," you
know that both Strike and Armor Class are referring to rules.

If a word or a phrase is italicized, it is describing a spell or a magic
item. This way, when you see the statement "the door is sealed by lock,"
you know that in this case the word denotes the lock spell, rather than a
physical item. Pathfinder also uses many terms that are typically expressed
as abbreviations, like AC for Armor Class, DC for Difficulty Class, and
HP for Hit Points.
If you’re ever confused about a game term or an abbreviation, you can always
turn to the Glossary and Index and look it up.

Understanding Actions
---------------------

.. todo:: finish Chapter 1 - Understanding Actions

Character Creation
==================

.. todo:: finish Chapter 1 - Character Creation

The Six Ability Scores
----------------------

One of the most important aspects of your character is
their ability scores. These scores represent your character’s
raw potential and influence nearly every other statistic on
your character sheet. Determining your ability scores is
not done all at once, but instead happens over several
steps during character creation.

Ability scores are split into two main groups: physical
and mental. :pf2srd:abilityscore:`Strength`, :pf2srd:abilityscore:`Dexterity`,
and :pf2srd:abilityscore:`Constitution` are
physical ability scores, measuring your character’s physical
power, agility, and stamina. In contrast, :pf2srd:abilityscore:`Intelligence`,
:pf2srd:abilityscore:`Wisdom`, and :pf2srd:abilityscore:`Charisma` are
mental ability scores and measure your character’s learned prowess, awareness,
and force of personality.

Excellence in an ability score improves the checks and
statistics related to that ability, as described below. When
imagining your character, you should also decide what
ability scores you want to focus on to give you the best
chance at success.

.. todo:: finish Chapter 1 - The Six Ability Scores

.. pf2srd:autoabilityscore:: Strength

.. pf2srd:autoabilityscore:: Dexterity

.. pf2srd:autoabilityscore:: Constitution

.. pf2srd:autoabilityscore:: Intelligence

.. pf2srd:autoabilityscore:: Wisdom

.. pf2srd:autoabilityscore:: Charisma
