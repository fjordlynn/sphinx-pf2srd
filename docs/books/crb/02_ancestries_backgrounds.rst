.. _corerulebook_02_ancestries&backgrounds:

########################
Ancestries & Backgrounds
########################

    Your character’s ancestry determines which people they call their own,
    whether it’s diverse and ambitious humans, insular but vivacious elves,
    traditionalist and family-focused dwarves, or any of the other folk
    who call Golarion home. A character’s ancestry and their experiences
    prior to their life as an adventurer—represented by a background—might
    be key parts of their identity, shape how they see the world, and help
    them find their place in it.

A character has one ancestry and one background, both
of which you select during character creation. You’ll also
select a number of languages for your character. Once
chosen, your ancestry and background can’t be changed.

This chapter is divided into three parts:

:ref:`Ancestries <ancestry_entries>` express the culture your character
hails from. Within many ancestries are heritages—subgroups that each have
their own characteristics. An ancestry provides ability boosts (and perhaps
ability flaws), Hit Points, ancestry feats, and sometimes additional abilities.

Backgrounds describe training
or environments your character experienced
before becoming an adventurer. Your character’s
background provides ability boosts, skill training,
and a skill feat.

Languages, starting on page 65, let your character
communicate with the wonderful and weird
people and creatures of the world.

.. _ancestry_entries:

Ancestry entries
================

Each entry includes details about the ancestry and
presents the rules elements described below (all of these
but heritages and ancestry feats are listed in a sidebar).

Hit Points
----------

This tells you how many Hit Points your character gains
from their ancestry at 1st level. You’ll add the Hit Points
from your character’s class (including their Constitution
modifier) to this number. For more on calculating Hit
Points, see Step 7: Record Class Details, on page 25.

Size
----

This tells you the physical size of members of the ancestry.
Medium corresponds roughly to the height and weight
range of a human adult, and Small is roughly half that.

Speed
-----

This entry lists how far a member of the ancestry can move
each time they spend an action (such as Stride) to do so.

Ability Boosts
--------------

This lists the ability scores you apply ability boosts to
when creating a character of this ancestry. Most ancestries
provide ability boosts to two specified ability scores, plus
a free ability boost that you can apply to any other score
of your choice. For more about ability boosts, see page 20.

Ability Flaw
------------

This lists the ability score to which you apply an ability
flaw when creating a character of this ancestry. Most
ancestries, with the exception of humans, include an ability
flaw. For more about applying ability flaws, see page 20.

Languages
---------

This tells you the languages that members of the ancestry
speak at 1st level. If your Intelligence modifier is +1 or
higher, you can select more languages from a list given
here. More about languages can be found on page 65.

Traits
------

These descriptors have no mechanical benefit, but they’re
important for determining how certain spells, effects, and
other aspects of the game interact with your character.

Special Abilities
-----------------

Any other entries in the sidebar represent abilities, senses,
and other qualities all members of the ancestry manifest.
These are omitted for ancestries with no special rules.

Heritages
---------

You select a heritage at 1st level to reflect abilities passed
down to you from your ancestors or common among
those of your ancestry in the environment where you were
born or grew up. You have only one heritage and can’t
change it later. A heritage is not the same as a culture or
ethnicity, though some cultures or ethnicities might have
more or fewer members from a particular heritage.

Ancestry Feats
--------------

This section presents ancestry feats, which allow you to
customize your character. You gain your first ancestry
feat at 1st level, and you gain another at 5th level, 9th
level, 13th level, and 17th level, as indicated in the class
advancement table in the descriptions of each class.
Ancestry feats are organized by level. As a starting
character, you can choose from only 1st-level ancestry
feats, but later choices can be made from any feat of
your level or lower. These feats also sometimes list
prerequisites—requirements that your character must
fulfill to select that feat.


.. pf2srd:autoancestry:: Dwarf

.. pf2srd:autoancestry:: Elf

.. pf2srd:autoancestry:: Gnome

.. pf2srd:autoancestry:: Goblin

.. pf2srd:autoancestry:: Halfling

.. pf2srd:autoancestry:: Human


Backgrounds
===========

    Backgrounds allow you to customize your character based on their life
    before adventuring. This is the next step in their life story after
    their ancestry, which reflects the circumstances of their birth.
    Your character’s background can help you learn or portray more about
    their personality while also suggesting what sorts of things they’re
    likely to know. Consider what events set your character on their path
    to the life of an adventurer and how those circumstances relate to their
    background.

At 1st level when you create your character, you gain a
background of your choice. This decision is permanent;
you can’t change it at later levels. Each background listed
here grants two ability boosts, a skill feat, and the trained
proficiency rank in two skills, one of which is a Lore skill.
If you gain the trained proficiency rank in a skill from your
background and would then gain the trained proficiency
rank in the same skill from your class at 1st level, you
instead become trained in another skill of your choice.

Lore skills represent deep knowledge of a specific
subject. If a Lore skill
involves a choice (for instance, a choice of terrain), explain
your preference to the GM, who has final say on whether
it’s acceptable or not. If you’d like some suggestions, the
Common Lore Subcategories sidebar on page 248 lists a
number of Lore skills that are suitable for most campaigns.
Skill feats expand the functions of your skills and
appear in :ref:`corerulebook_05_feats`


.. pf2srd:autokind:: background
