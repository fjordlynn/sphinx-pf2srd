#####
About
#####

Fjordlynn's SRD for Pathfinder Second Edition is

*   Managed as a `GitLab Project`_
*   Hosted on `GitLab Pages`_
*   Written in RestructuredText_, YAML_ and Python_
*   Built with Sphinx_
*   Open for :ref:`contribution <contributing>`

.. _GitLab Project: https://gitlab.com/fjordlynn/sphinx-pf2srd
.. _GitLab Pages: https://docs.gitlab.com/ee/user/project/pages/
.. _RestructuredText: https://docutils.sourceforge.io/rst.html
.. _YAML: https://yaml.org/
.. _Python: https://www.python.org/
.. _Sphinx: https://www.sphinx-doc.org/en/master/


.. toctree::

    sources
    licenses
