items:
  Cauthooj:
    __old:
      ability_mods:
        cha_mod: 0
        con_mod: 7
        dex_mod: 4
        int_mod: -3
        str_mod: 6
        wis_mod: 2
      ac: 33
      ac_special: null
      active_abilities:
        - actioncost: One Action
          critical_failure: null
          critical_success: null
          description: >-
            With subtle alterations in the pitch and tone of its song, the cauthooj
            directs one creature confused by its Warbling Song to make a Strike. This
            works like other Strikes made by confused creatures, except that the cauthooj
            chooses the target. If no target is in reach or range, or the creature
            is unable to Strike for any other reason, this ability has no effect.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                With subtle alterations in the pitch and tone of its song, the cauthooj
                directs one creature confused by its Warbling Song to make a Strike.
                This works like other Strikes made by confused creatures, except that
                the cauthooj chooses the target. If no target is in reach or range,
                or the creature is unable to Strike for any other reason, this ability
                has no effect.
              duration: null
              failure: null
              name: Staccato Strike
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - mental
                - primal
                - sonic
          failure: null
          frequency: null
          maximum_duration: null
          name: Staccato Strike
          range: null
          raw_description: >-
            **Staccato Strike** [One Action]  (__mental__, __primal__, __sonic__)
            With subtle alterations in the pitch and tone of its song, the cauthooj
            directs one creature confused by its Warbling Song to make a Strike. This
            works like other Strikes made by confused creatures, except that the cauthooj
            chooses the target. If no target is in reach or range, or the creature
            is unable to Strike for any other reason, this ability has no effect.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - mental
            - primal
            - sonic
          trigger: null
        - actioncost: Two Actions
          critical_failure: >-
            The target is __confused__ for 1 round and immediately attacks itself
            (in the normal fashion for attacking oneself while confused). This Strike
            doesn't give the creature a flat check to recover from the confusion.
          critical_success: >-
            The target is unaffected and is temporarily immune for 1 minute.
          description: >-
            The cauthooj gives a strange, ululating cry that causes nearby creatures
            to lash out violently and without control. Each creature within 120-foot
            emanation that can hear the cauthooj must attempt a DC 32 Will save to
            resist the effect.
          effect: null
          effects:
            - area: null
              critical_failure: >-
                The target is __confused__ for 1 round and immediately attacks itself
                (in the normal fashion for attacking oneself while confused). This
                Strike doesn't give the creature a flat check to recover from the
                confusion.
              critical_success: >-
                The target is unaffected and is temporarily immune for 1 minute.
              description: >-
                The cauthooj gives a strange, ululating cry that causes nearby creatures
                to lash out violently and without control. Each creature within 120-foot
                emanation that can hear the cauthooj must attempt a DC 32 Will save
                to resist the effect.
              duration: null
              failure: The target is __confused__ for 1 round.
              name: Warbling Song
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: The target is unaffected.
              target: null
              traits:
                - auditory
                - incapacitation
                - mental
                - primal
          failure: The target is __confused__ for 1 round.
          frequency: null
          maximum_duration: null
          name: Warbling Song
          range: null
          raw_description: >-
            **Warbling Song** [Two Actions]  (__auditory__, __incapacitation__, __mental__,
            __primal__) The cauthooj gives a strange, ululating cry that causes nearby
            creatures to lash out violently and without control. Each creature within
            120-foot emanation that can hear the cauthooj must attempt a DC 32 Will
            save to resist the effect.**Critical Success** The target is unaffected
            and is temporarily immune for 1 minute.**Success** The target is unaffected.**Failure**
            The target is __confused__ for 1 round.**Critical Failure** The target
            is __confused__ for 1 round and immediately attacks itself (in the normal
            fashion for attacking oneself while confused). This Strike doesn't give
            the creature a flat check to recover from the confusion.
          requirements: null
          saving_throw: null
          stages: null
          success: The target is unaffected.
          traits:
            - auditory
            - incapacitation
            - mental
            - primal
          trigger: null
      alignment: N
      automatic_abilities:
        - actioncost: Reaction
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The cauthooj nimbly hops aside, redirecting the triggering Strike against
            the adjacent enemy. The cauthooj Strides up to half its Speed, and this
            movement does not trigger reactions.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Hop-Dodge
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - move
          failure: null
          frequency: null
          maximum_duration: null
          name: Hop-Dodge
          range: null
          raw_description: >-
            **Hop-Dodge** [Reaction] (__move__) **Trigger** The cauthooj is the target
            of a melee Strike and is adjacent to another enemy that is also within
            the reach of the melee Strike. **Effect** The cauthooj nimbly hops aside,
            redirecting the triggering Strike against the adjacent enemy. The cauthooj
            Strides up to half its Speed, and this movement does not trigger reactions.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - move
          trigger: >-
            The cauthooj is the target of a melee Strike and is adjacent to another
            enemy that is also within the reach of the melee Strike.
      description: >-
        These large, flightless birds are deceptively agile, considering their long
        bodies and awkward, hopping gait. Solitary predators, they use their hypnotic
        warbling song to drive prey into a wild frenzy, manipulating them into attacking
        one another so that the cauthooj can then feast on the remains.

        Known to some scholars as the puppet master bird, and to others as the shrill
        shrike, cauthoojs are widely reviled by most intelligent humanoids, in part
        because the birds seem to prefer humanoids to other prey. Cauthooj sightings
        typically lead to the creation of hunting parties to track the creature down
        before it can kill again, with would-be hunters typically stuffing their ears
        full of wax in an effort to avoid being affected by its cry. Those who have
        survived the creature's song report that the experience is uniquely unnerving,
        and almost all accounts agree that there is no other sound as terrible.

        While one might assume the cauthooj is a dumb animal, these creatures are
        in fact quite a bit smarter than they look. Cauthoojs stalk the perimeter
        of remote settlements in hopes of finding a lone traveler they can feast upon.
        These patient creatures will wait in ambush as long as they must to sate their
        hunger. They can even understand a few rudimentary words in Sylvan, although
        they are incapable of clearly speaking themselves. This doesn't stop the cauthooj
        from attempting to mimic the sounds it hears, but when it does so, its eerie
        primal nature enhances the attempt, leading to the bird's signature ability
        to manipulate minds and encourage conflict, a trait the cauthooj is just barely
        smart enough to understand—and enjoy.
      hp: 215
      hp_misc: null
      languages:
        - Sylvan
        - (can't speak any language)
      level: 12
      melee_attacks:
        - actioncost: One Action
          damage:
            - formula: 2d12+12
              type: piercing
          name: beak
          to_hit: 26
          traits:
            - agile
            - deadly 1d12
            - reach 10 feet
      name: Cauthooj
      old_recall_knowledge:
        - dc: 30
          skill: ' - Beast__ (__Arcana__, __Nature__)'
      perception: 22
      ranged_attacks: null
      recall_knowledge:
        - dc: 30
          skill: Arcana
          trait: Beast
        - dc: 30
          skill: Nature
          trait: Beast
      resistances:
        - amount: 15
          misc: null
          type: sonic
      saves:
        fort: 25
        fort_misc: null
        misc: null
        ref: 20
        ref_misc: null
        will: 18
        will_misc: null
      sense_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The cauthooj senses a creatures mental essence at the listed ranged.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The cauthooj senses a creatures mental essence at the listed ranged.
              duration: null
              failure: null
              name: Thoughtsense
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - divination
                - mental
                - occult
          failure: null
          frequency: null
          maximum_duration: null
          name: Thoughtsense
          range: null
          raw_description: >-
            **Thoughtsense** (__divination__, __mental__, __occult__) The cauthooj
            senses a creatures mental essence at the listed ranged.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divination
            - mental
            - occult
          trigger: null
      senses:
        - thoughtsense (imprecise) 60 feet
      size: Medium
      skills:
        - bonus: 24
          misc: null
          name: Athletics
        - bonus: 25
          misc: null
          name: Stealth
      sources:
        - abbr: Bestiary
          page_start: '55'
          page_stop: null
      speed:
        - amount: 35
          misc: null
          type: Land
      spell_lists: null
      traits:
        - Beast
    ability_mods:
      cha_mod: 0
      con_mod: 7
      dex_mod: 4
      int_mod: -3
      str_mod: 6
      wis_mod: 2
    ac: 33
    activities:
      Hop-Dodge:
        __old:
          actioncost: Reaction
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The cauthooj nimbly hops aside, redirecting the triggering Strike against
            the adjacent enemy. The cauthooj Strides up to half its Speed, and this
            movement does not trigger reactions.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Hop-Dodge
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - move
          failure: null
          frequency: null
          maximum_duration: null
          name: Hop-Dodge
          range: null
          raw_description: >-
            **Hop-Dodge** [Reaction] (__move__) **Trigger** The cauthooj is the target
            of a melee Strike and is adjacent to another enemy that is also within
            the reach of the melee Strike. **Effect** The cauthooj nimbly hops aside,
            redirecting the triggering Strike against the adjacent enemy. The cauthooj
            Strides up to half its Speed, and this movement does not trigger reactions.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - move
          trigger: >-
            The cauthooj is the target of a melee Strike and is adjacent to another
            enemy that is also within the reach of the melee Strike.
        __original_key: automatic_abilities
        actioncost: Reaction
        effects:
          Hop-Dodge:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Hop-Dodge
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - move
            actioncost: Reaction
            name: Hop-Dodge
        name: Hop-Dodge
      Staccato Strike:
        __old:
          actioncost: One Action
          critical_failure: null
          critical_success: null
          description: >-
            With subtle alterations in the pitch and tone of its song, the cauthooj
            directs one creature confused by its Warbling Song to make a Strike. This
            works like other Strikes made by confused creatures, except that the cauthooj
            chooses the target. If no target is in reach or range, or the creature
            is unable to Strike for any other reason, this ability has no effect.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                With subtle alterations in the pitch and tone of its song, the cauthooj
                directs one creature confused by its Warbling Song to make a Strike.
                This works like other Strikes made by confused creatures, except that
                the cauthooj chooses the target. If no target is in reach or range,
                or the creature is unable to Strike for any other reason, this ability
                has no effect.
              duration: null
              failure: null
              name: Staccato Strike
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - mental
                - primal
                - sonic
          failure: null
          frequency: null
          maximum_duration: null
          name: Staccato Strike
          range: null
          raw_description: >-
            **Staccato Strike** [One Action]  (__mental__, __primal__, __sonic__)
            With subtle alterations in the pitch and tone of its song, the cauthooj
            directs one creature confused by its Warbling Song to make a Strike. This
            works like other Strikes made by confused creatures, except that the cauthooj
            chooses the target. If no target is in reach or range, or the creature
            is unable to Strike for any other reason, this ability has no effect.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - mental
            - primal
            - sonic
          trigger: null
        __original_key: active_abilities
        actioncost: One Action
        effects:
          Staccato Strike:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                With subtle alterations in the pitch and tone of its song, the cauthooj
                directs one creature confused by its Warbling Song to make a Strike.
                This works like other Strikes made by confused creatures, except that
                the cauthooj chooses the target. If no target is in reach or range,
                or the creature is unable to Strike for any other reason, this ability
                has no effect.
              duration: null
              failure: null
              name: Staccato Strike
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - mental
                - primal
                - sonic
            actioncost: One Action
            name: Staccato Strike
        name: Staccato Strike
      Thoughtsense:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The cauthooj senses a creatures mental essence at the listed ranged.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The cauthooj senses a creatures mental essence at the listed ranged.
              duration: null
              failure: null
              name: Thoughtsense
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - divination
                - mental
                - occult
          failure: null
          frequency: null
          maximum_duration: null
          name: Thoughtsense
          range: null
          raw_description: >-
            **Thoughtsense** (__divination__, __mental__, __occult__) The cauthooj
            senses a creatures mental essence at the listed ranged.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divination
            - mental
            - occult
          trigger: null
        __original_key: sense_abilities
        actioncost: None
        effects:
          Thoughtsense:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                The cauthooj senses a creatures mental essence at the listed ranged.
              duration: null
              failure: null
              name: Thoughtsense
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: null
              target: null
              traits:
                - divination
                - mental
                - occult
            actioncost: None
            name: Thoughtsense
        name: Thoughtsense
      Warbling Song:
        __old:
          actioncost: Two Actions
          critical_failure: >-
            The target is __confused__ for 1 round and immediately attacks itself
            (in the normal fashion for attacking oneself while confused). This Strike
            doesn't give the creature a flat check to recover from the confusion.
          critical_success: >-
            The target is unaffected and is temporarily immune for 1 minute.
          description: >-
            The cauthooj gives a strange, ululating cry that causes nearby creatures
            to lash out violently and without control. Each creature within 120-foot
            emanation that can hear the cauthooj must attempt a DC 32 Will save to
            resist the effect.
          effect: null
          effects:
            - area: null
              critical_failure: >-
                The target is __confused__ for 1 round and immediately attacks itself
                (in the normal fashion for attacking oneself while confused). This
                Strike doesn't give the creature a flat check to recover from the
                confusion.
              critical_success: >-
                The target is unaffected and is temporarily immune for 1 minute.
              description: >-
                The cauthooj gives a strange, ululating cry that causes nearby creatures
                to lash out violently and without control. Each creature within 120-foot
                emanation that can hear the cauthooj must attempt a DC 32 Will save
                to resist the effect.
              duration: null
              failure: The target is __confused__ for 1 round.
              name: Warbling Song
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: The target is unaffected.
              target: null
              traits:
                - auditory
                - incapacitation
                - mental
                - primal
          failure: The target is __confused__ for 1 round.
          frequency: null
          maximum_duration: null
          name: Warbling Song
          range: null
          raw_description: >-
            **Warbling Song** [Two Actions]  (__auditory__, __incapacitation__, __mental__,
            __primal__) The cauthooj gives a strange, ululating cry that causes nearby
            creatures to lash out violently and without control. Each creature within
            120-foot emanation that can hear the cauthooj must attempt a DC 32 Will
            save to resist the effect.**Critical Success** The target is unaffected
            and is temporarily immune for 1 minute.**Success** The target is unaffected.**Failure**
            The target is __confused__ for 1 round.**Critical Failure** The target
            is __confused__ for 1 round and immediately attacks itself (in the normal
            fashion for attacking oneself while confused). This Strike doesn't give
            the creature a flat check to recover from the confusion.
          requirements: null
          saving_throw: null
          stages: null
          success: The target is unaffected.
          traits:
            - auditory
            - incapacitation
            - mental
            - primal
          trigger: null
        __original_key: active_abilities
        actioncost: Two Actions
        effects:
          Warbling Song:
            __old:
              area: null
              critical_failure: >-
                The target is __confused__ for 1 round and immediately attacks itself
                (in the normal fashion for attacking oneself while confused). This
                Strike doesn't give the creature a flat check to recover from the
                confusion.
              critical_success: >-
                The target is unaffected and is temporarily immune for 1 minute.
              description: >-
                The cauthooj gives a strange, ululating cry that causes nearby creatures
                to lash out violently and without control. Each creature within 120-foot
                emanation that can hear the cauthooj must attempt a DC 32 Will save
                to resist the effect.
              duration: null
              failure: The target is __confused__ for 1 round.
              name: Warbling Song
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '55'
                  page_stop: null
              success: The target is unaffected.
              target: null
              traits:
                - auditory
                - incapacitation
                - mental
                - primal
            actioncost: Two Actions
            name: Warbling Song
        name: Warbling Song
    alignment: N
    description: >-
      These large, flightless birds are deceptively agile, considering their long
      bodies and awkward, hopping gait. Solitary predators, they use their hypnotic
      warbling song to drive prey into a wild frenzy, manipulating them into attacking
      one another so that the cauthooj can then feast on the remains.

      Known to some scholars as the puppet master bird, and to others as the shrill
      shrike, cauthoojs are widely reviled by most intelligent humanoids, in part
      because the birds seem to prefer humanoids to other prey. Cauthooj sightings
      typically lead to the creation of hunting parties to track the creature down
      before it can kill again, with would-be hunters typically stuffing their ears
      full of wax in an effort to avoid being affected by its cry. Those who have
      survived the creature's song report that the experience is uniquely unnerving,
      and almost all accounts agree that there is no other sound as terrible.

      While one might assume the cauthooj is a dumb animal, these creatures are in
      fact quite a bit smarter than they look. Cauthoojs stalk the perimeter of remote
      settlements in hopes of finding a lone traveler they can feast upon. These patient
      creatures will wait in ambush as long as they must to sate their hunger. They
      can even understand a few rudimentary words in Sylvan, although they are incapable
      of clearly speaking themselves. This doesn't stop the cauthooj from attempting
      to mimic the sounds it hears, but when it does so, its eerie primal nature enhances
      the attempt, leading to the bird's signature ability to manipulate minds and
      encourage conflict, a trait the cauthooj is just barely smart enough to understand—and
      enjoy.
    hp: 215
    hp_misc: null
    items: null
    languages:
      - Sylvan
      - (can't speak any language)
    level: 12
    name: Cauthooj
    perception: 22
    rarity: null
    recall_knowledge:
      - dc: 30
        skill: Arcana
        trait: Beast
      - dc: 30
        skill: Nature
        trait: Beast
    resistances:
      - amount: 15
        misc: null
        type: sonic
    saves:
      fort: 25
      fort_misc: null
      misc: null
      ref: 20
      ref_misc: null
      will: 18
      will_misc: null
    senses:
      - thoughtsense (imprecise) 60 feet
    size: Medium
    skills:
      - bonus: 24
        misc: null
        name: Athletics
      - bonus: 25
        misc: null
        name: Stealth
    sources:
      - abbr: Bestiary
        page_start: '55'
        page_stop: null
    speed:
      - amount: 35
        misc: null
        type: Land
    traits:
      - Beast
