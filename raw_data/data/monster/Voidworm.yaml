items:
  Voidworm:
    __old:
      ability_mods:
        cha_mod: 1
        con_mod: 0
        dex_mod: 4
        int_mod: -1
        str_mod: -1
        wis_mod: -1
      ac: 17
      ac_special: null
      active_abilities:
        - actioncost: One Action
          critical_failure: null
          critical_success: null
          description: >-
            The voidworm takes on the appearance of a Tiny animal. This doesn't change
            its Speed or its attack and damage bonuses with its Strikes, but might
            change the damage type its Strikes deal.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The voidworm takes on the appearance of a Tiny animal. This doesn't
                change its Speed or its attack and damage bonuses with its Strikes,
                but might change the damage type its Strikes deal.
              duration: null
              failure: null
              name: Change Shape
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - concentrate
                - divine
                - polymorph
                - transmutation
          failure: null
          frequency: null
          maximum_duration: null
          name: Change Shape
          range: null
          raw_description: >-
            **Change Shape** [One Action]  (__concentrate__, __divine__, __polymorph__,
            __transmutation__) The voidworm takes on the appearance of a Tiny animal.
            This doesn't change its Speed or its attack and damage bonuses with its
            Strikes, but might change the damage type its Strikes deal.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - concentrate
            - divine
            - polymorph
            - transmutation
          trigger: null
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A creature hit by the voidworm's tail Strike is stupefied 1 for 1 round
            (__stupefied 2__ on a critical hit). A successful DC 16 Will save negates
            this effect and grants temporary immunity to confounding lash for 1 minute.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A creature hit by the voidworm's tail Strike is stupefied 1 for 1
                round (__stupefied 2__ on a critical hit). A successful DC 16 Will
                save negates this effect and grants temporary immunity to confounding
                lash for 1 minute.
              duration: null
              failure: null
              name: Confounding Lash
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - emotion
                - enchantment
                - mental
          failure: null
          frequency: null
          maximum_duration: null
          name: Confounding Lash
          range: null
          raw_description: >-
            **Confounding Lash** (__divine__, __emotion__, __enchantment__, __mental__)
            A creature hit by the voidworm's tail Strike is stupefied 1 for 1 round
            (__stupefied 2__ on a critical hit). A successful DC 16 Will save negates
            this effect and grants temporary immunity to confounding lash for 1 minute.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - emotion
            - enchantment
            - mental
          trigger: null
      alignment: CN
      automatic_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A voidworm's vital organs shift and change shape and position constantly.
            Immediately after the voidworm takes acid, electricity, or sonic damage,
            it gains the listed amount of resistance to that damage type. This lasts
            for 1 hour or until the next time the protean takes damage of one of the
            other types (in which case its resistance changes to match that type),
            whichever comes first.

            The voidworm is immune to polymorph effects unless it is a willing target.
            If blinded or deafened, the voidworm automatically recovers at the end
            of its next turn as new sensory organs grow to replace the compromised
            ones.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A voidworm's vital organs shift and change shape and position constantly.
                Immediately after the voidworm takes acid, electricity, or sonic damage,
                it gains the listed amount of resistance to that damage type. This
                lasts for 1 hour or until the next time the protean takes damage of
                one of the other types (in which case its resistance changes to match
                that type), whichever comes first.

                The voidworm is immune to polymorph effects unless it is a willing
                target. If blinded or deafened, the voidworm automatically recovers
                at the end of its next turn as new sensory organs grow to replace
                the compromised ones.
              duration: null
              failure: null
              name: Protean Anatomy
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - transmutation
          failure: null
          frequency: null
          maximum_duration: null
          name: Protean Anatomy
          range: null
          raw_description: >-
            **Protean Anatomy** (__divine__, __transmutation__) A voidworm's vital
            organs shift and change shape and position constantly. Immediately after
            the voidworm takes acid, electricity, or sonic damage, it gains the listed
            amount of resistance to that damage type. This lasts for 1 hour or until
            the next time the protean takes damage of one of the other types (in which
            case its resistance changes to match that type), whichever comes first.

            The voidworm is immune to polymorph effects unless it is a willing target.
            If blinded or deafened, the voidworm automatically recovers at the end
            of its next turn as new sensory organs grow to replace the compromised
            ones.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - transmutation
          trigger: null
      description: >-
        Other proteans don't consider the flying, iridescent beings known as voidworms
        to be part of a protean caste at all, but instead merely a shameful side effect
        of the Maelstrom's constantly churning energy. To call a voidworm a protean
        in the presence of a more powerful protean is as sure a way to instigate combat
        as any.

        Voidworms themselves have little interest in whether anyone sees them as proteans.
        They maintain a thriving ecology in the Maelstrom, frolicking in schools of
        up to 20 and playing in the chaos of constantly shifting realities. Elsewhere
        (such as on the Material Plane), voidworms are mesmerized by the principle
        of object permanence; many latch onto specific features of a region (such
        as a hillside or pond) and flit through the air around it for months or even
        years as they wait for the object of their curiosity to change. Minor changes—
        such as a tree's change of color in the fall, a corpse's slow decay, or periodic
        venting of steam from a geyser—all fascinate voidworms. A voidworm is about
        2 feet long and weighs 2 pounds.
      hp: 16
      hp_misc: fast healing 1
      languages:
        - Abyssal
        - Protean
      level: 1
      melee_attacks:
        - actioncost: One Action
          damage:
            - formula: 1d8
              type: –1 piercing
            - formula: 1d4
              type: chaotic
          name: jaws
          to_hit: 9
          traits:
            - chaotic
            - finesse
            - magical
        - actioncost: One Action
          damage:
            - formula: 1d4
              type: –1 slashing
            - formula: 1d4
              type: chaotic
            - formula: null
              type: confounding lash
          name: tail
          to_hit: 9
          traits:
            - chaotic
            - finesse
            - magical
      name: Voidworm
      old_recall_knowledge:
        - dc: 15
          skill: ' - Monitor__ (__Religion__)'
      perception: 4
      ranged_attacks: null
      recall_knowledge:
        - dc: 15
          skill: Religion
          trait: Monitor
      resistances:
        - amount: 3
          misc: null
          type: precision
        - amount: 5
          misc: null
          type: protean anatomy
      saves:
        fort: 5
        fort_misc: null
        misc: null
        ref: 9
        ref_misc: null
        will: 6
        will_misc: null
      sense_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A voidworm can anticipate the most likely presence of a creature through
            a supernatural insight into chaotic probabilities and chance. This grants
            it the ability to sense creatures within the listed range. A creature
            under the effects of __nondetection__ or that is otherwise shielded from
            divinations and predictions cannot be noticed via entropy sense.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A voidworm can anticipate the most likely presence of a creature through
                a supernatural insight into chaotic probabilities and chance. This
                grants it the ability to sense creatures within the listed range.
                A creature under the effects of __nondetection__ or that is otherwise
                shielded from divinations and predictions cannot be noticed via entropy
                sense.
              duration: null
              failure: null
              name: Entropy Sense
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divination
                - divine
                - prediction
          failure: null
          frequency: null
          maximum_duration: null
          name: Entropy Sense
          range: null
          raw_description: >-
            **Entropy Sense** (__divination__, __divine__, __prediction__) A voidworm
            can anticipate the most likely presence of a creature through a supernatural
            insight into chaotic probabilities and chance. This grants it the ability
            to sense creatures within the listed range. A creature under the effects
            of __nondetection__ or that is otherwise shielded from divinations and
            predictions cannot be noticed via entropy sense.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divination
            - divine
            - prediction
          trigger: null
      senses:
        - entropy sense (imprecise) 30 feet
        - darkvision
      size: Tiny
      skills:
        - bonus: 7
          misc: null
          name: Acrobatics
        - bonus: 6
          misc: null
          name: Deception
        - bonus: 4
          misc: null
          name: Religion
        - bonus: 7
          misc: null
          name: Stealth
      sources:
        - abbr: Bestiary
          page_start: '266'
          page_stop: null
      speed:
        - amount: 20
          misc: null
          type: Land
        - amount: 40
          misc: null
          type: fly
        - amount: null
          misc: null
          type: freedom of movement
      spell_lists:
        - attack_bonus: null
          cantrips:
            - level: 4
              spells:
                - misc: null
                  name: dancing lights
                - misc: null
                  name: ghost sound
                - misc: null
                  name: prestidigitation
          constants:
            - level: 4
              spells:
                - misc: null
                  name: freedom of movement
          dc: 16
          focus_points: null
          misc: null
          spell_lists:
            - level: 4
              spells:
                - misc: null
                  name: read omens
            - level: 2
              spells:
                - misc: self only
                  name: blur
                - misc: null
                  name: obscuring mist
            - level: 1
              spells:
                - misc: at will; lawful only
                  name: detect alignment
          spells_source: Divine Innate Spells
      traits:
        - Monitor
        - Protean
    ability_mods:
      cha_mod: 1
      con_mod: 0
      dex_mod: 4
      int_mod: -1
      str_mod: -1
      wis_mod: -1
    ac: 17
    activities:
      Change Shape:
        __old:
          actioncost: One Action
          critical_failure: null
          critical_success: null
          description: >-
            The voidworm takes on the appearance of a Tiny animal. This doesn't change
            its Speed or its attack and damage bonuses with its Strikes, but might
            change the damage type its Strikes deal.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The voidworm takes on the appearance of a Tiny animal. This doesn't
                change its Speed or its attack and damage bonuses with its Strikes,
                but might change the damage type its Strikes deal.
              duration: null
              failure: null
              name: Change Shape
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - concentrate
                - divine
                - polymorph
                - transmutation
          failure: null
          frequency: null
          maximum_duration: null
          name: Change Shape
          range: null
          raw_description: >-
            **Change Shape** [One Action]  (__concentrate__, __divine__, __polymorph__,
            __transmutation__) The voidworm takes on the appearance of a Tiny animal.
            This doesn't change its Speed or its attack and damage bonuses with its
            Strikes, but might change the damage type its Strikes deal.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - concentrate
            - divine
            - polymorph
            - transmutation
          trigger: null
        __original_key: active_abilities
        actioncost: One Action
        effects:
          Change Shape:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                The voidworm takes on the appearance of a Tiny animal. This doesn't
                change its Speed or its attack and damage bonuses with its Strikes,
                but might change the damage type its Strikes deal.
              duration: null
              failure: null
              name: Change Shape
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - concentrate
                - divine
                - polymorph
                - transmutation
            actioncost: One Action
            name: Change Shape
        name: Change Shape
      Confounding Lash:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A creature hit by the voidworm's tail Strike is stupefied 1 for 1 round
            (__stupefied 2__ on a critical hit). A successful DC 16 Will save negates
            this effect and grants temporary immunity to confounding lash for 1 minute.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A creature hit by the voidworm's tail Strike is stupefied 1 for 1
                round (__stupefied 2__ on a critical hit). A successful DC 16 Will
                save negates this effect and grants temporary immunity to confounding
                lash for 1 minute.
              duration: null
              failure: null
              name: Confounding Lash
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - emotion
                - enchantment
                - mental
          failure: null
          frequency: null
          maximum_duration: null
          name: Confounding Lash
          range: null
          raw_description: >-
            **Confounding Lash** (__divine__, __emotion__, __enchantment__, __mental__)
            A creature hit by the voidworm's tail Strike is stupefied 1 for 1 round
            (__stupefied 2__ on a critical hit). A successful DC 16 Will save negates
            this effect and grants temporary immunity to confounding lash for 1 minute.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - emotion
            - enchantment
            - mental
          trigger: null
        __original_key: active_abilities
        actioncost: None
        effects:
          Confounding Lash:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                A creature hit by the voidworm's tail Strike is stupefied 1 for 1
                round (__stupefied 2__ on a critical hit). A successful DC 16 Will
                save negates this effect and grants temporary immunity to confounding
                lash for 1 minute.
              duration: null
              failure: null
              name: Confounding Lash
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - emotion
                - enchantment
                - mental
            actioncost: None
            name: Confounding Lash
        name: Confounding Lash
      Entropy Sense:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A voidworm can anticipate the most likely presence of a creature through
            a supernatural insight into chaotic probabilities and chance. This grants
            it the ability to sense creatures within the listed range. A creature
            under the effects of __nondetection__ or that is otherwise shielded from
            divinations and predictions cannot be noticed via entropy sense.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A voidworm can anticipate the most likely presence of a creature through
                a supernatural insight into chaotic probabilities and chance. This
                grants it the ability to sense creatures within the listed range.
                A creature under the effects of __nondetection__ or that is otherwise
                shielded from divinations and predictions cannot be noticed via entropy
                sense.
              duration: null
              failure: null
              name: Entropy Sense
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divination
                - divine
                - prediction
          failure: null
          frequency: null
          maximum_duration: null
          name: Entropy Sense
          range: null
          raw_description: >-
            **Entropy Sense** (__divination__, __divine__, __prediction__) A voidworm
            can anticipate the most likely presence of a creature through a supernatural
            insight into chaotic probabilities and chance. This grants it the ability
            to sense creatures within the listed range. A creature under the effects
            of __nondetection__ or that is otherwise shielded from divinations and
            predictions cannot be noticed via entropy sense.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divination
            - divine
            - prediction
          trigger: null
        __original_key: sense_abilities
        actioncost: None
        effects:
          Entropy Sense:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                A voidworm can anticipate the most likely presence of a creature through
                a supernatural insight into chaotic probabilities and chance. This
                grants it the ability to sense creatures within the listed range.
                A creature under the effects of __nondetection__ or that is otherwise
                shielded from divinations and predictions cannot be noticed via entropy
                sense.
              duration: null
              failure: null
              name: Entropy Sense
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divination
                - divine
                - prediction
            actioncost: None
            name: Entropy Sense
        name: Entropy Sense
      Protean Anatomy:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A voidworm's vital organs shift and change shape and position constantly.
            Immediately after the voidworm takes acid, electricity, or sonic damage,
            it gains the listed amount of resistance to that damage type. This lasts
            for 1 hour or until the next time the protean takes damage of one of the
            other types (in which case its resistance changes to match that type),
            whichever comes first.

            The voidworm is immune to polymorph effects unless it is a willing target.
            If blinded or deafened, the voidworm automatically recovers at the end
            of its next turn as new sensory organs grow to replace the compromised
            ones.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A voidworm's vital organs shift and change shape and position constantly.
                Immediately after the voidworm takes acid, electricity, or sonic damage,
                it gains the listed amount of resistance to that damage type. This
                lasts for 1 hour or until the next time the protean takes damage of
                one of the other types (in which case its resistance changes to match
                that type), whichever comes first.

                The voidworm is immune to polymorph effects unless it is a willing
                target. If blinded or deafened, the voidworm automatically recovers
                at the end of its next turn as new sensory organs grow to replace
                the compromised ones.
              duration: null
              failure: null
              name: Protean Anatomy
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - transmutation
          failure: null
          frequency: null
          maximum_duration: null
          name: Protean Anatomy
          range: null
          raw_description: >-
            **Protean Anatomy** (__divine__, __transmutation__) A voidworm's vital
            organs shift and change shape and position constantly. Immediately after
            the voidworm takes acid, electricity, or sonic damage, it gains the listed
            amount of resistance to that damage type. This lasts for 1 hour or until
            the next time the protean takes damage of one of the other types (in which
            case its resistance changes to match that type), whichever comes first.

            The voidworm is immune to polymorph effects unless it is a willing target.
            If blinded or deafened, the voidworm automatically recovers at the end
            of its next turn as new sensory organs grow to replace the compromised
            ones.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - transmutation
          trigger: null
        __original_key: automatic_abilities
        actioncost: None
        effects:
          Protean Anatomy:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                A voidworm's vital organs shift and change shape and position constantly.
                Immediately after the voidworm takes acid, electricity, or sonic damage,
                it gains the listed amount of resistance to that damage type. This
                lasts for 1 hour or until the next time the protean takes damage of
                one of the other types (in which case its resistance changes to match
                that type), whichever comes first.

                The voidworm is immune to polymorph effects unless it is a willing
                target. If blinded or deafened, the voidworm automatically recovers
                at the end of its next turn as new sensory organs grow to replace
                the compromised ones.
              duration: null
              failure: null
              name: Protean Anatomy
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '266'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - transmutation
            actioncost: None
            name: Protean Anatomy
        name: Protean Anatomy
    alignment: CN
    description: >-
      Other proteans don't consider the flying, iridescent beings known as voidworms
      to be part of a protean caste at all, but instead merely a shameful side effect
      of the Maelstrom's constantly churning energy. To call a voidworm a protean
      in the presence of a more powerful protean is as sure a way to instigate combat
      as any.

      Voidworms themselves have little interest in whether anyone sees them as proteans.
      They maintain a thriving ecology in the Maelstrom, frolicking in schools of
      up to 20 and playing in the chaos of constantly shifting realities. Elsewhere
      (such as on the Material Plane), voidworms are mesmerized by the principle of
      object permanence; many latch onto specific features of a region (such as a
      hillside or pond) and flit through the air around it for months or even years
      as they wait for the object of their curiosity to change. Minor changes— such
      as a tree's change of color in the fall, a corpse's slow decay, or periodic
      venting of steam from a geyser—all fascinate voidworms. A voidworm is about
      2 feet long and weighs 2 pounds.
    hp: 16
    hp_misc: fast healing 1
    items: null
    languages:
      - Abyssal
      - Protean
    level: 1
    name: Voidworm
    perception: 4
    rarity: null
    recall_knowledge:
      - dc: 15
        skill: Religion
        trait: Monitor
    resistances:
      - amount: 3
        misc: null
        type: precision
      - amount: 5
        misc: null
        type: protean anatomy
    saves:
      fort: 5
      fort_misc: null
      misc: null
      ref: 9
      ref_misc: null
      will: 6
      will_misc: null
    senses:
      - entropy sense (imprecise) 30 feet
      - darkvision
    size: Tiny
    skills:
      - bonus: 7
        misc: null
        name: Acrobatics
      - bonus: 6
        misc: null
        name: Deception
      - bonus: 4
        misc: null
        name: Religion
      - bonus: 7
        misc: null
        name: Stealth
    sources:
      - abbr: Bestiary
        page_start: '266'
        page_stop: null
    speed:
      - amount: 20
        misc: null
        type: Land
      - amount: 40
        misc: null
        type: fly
      - amount: null
        misc: null
        type: freedom of movement
    traits:
      - Monitor
      - Protean
