items:
  Deculi:
    __old:
      ability_mods:
        cha_mod: 5
        con_mod: 4
        dex_mod: 7
        int_mod: -3
        str_mod: 5
        wis_mod: 1
      ac: 33
      ac_special: null
      active_abilities:
        - actioncost: Two Actions
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The deculi creates an extradimensional space adjacent to itself in any
            10-foot-by-10-foot area of darkness and enters it. To non-deculis, the
            extradimensional space looks like an ordinary area of darkness (though
            effects such as __true seeing__ can see through this illusion). The deculi's
            infrared vision functions normally between this extradimensional space
            and the area outside its confines, allowing the deculi to sense what is
            going on around it, and its innate __darkness__ spell also extends to
            the area around the shadow sanctuary. Attacks cannot be made into or from
            the extradimensional space.

            The deculi can Dismiss this effect to emerge from its shadow sanctuary,
            but the space it emerges into must be in darkness and must be unobstructed.
            If the area is obstructed, the deculi is shunted to the nearest available
            space and takes 1d6 bludgeoning damage per 5 feet shunted. If the area
            overlaying the shadow sanctuary becomes illuminated by magical light,
            the extradimensional space can no longer be accessed and the deculi is
            trapped within until the area is once again in darkness. The deculi can
            try to extinguish a magical light effect obscuring its sanctuary once
            per day by attempting to counteract the light effect with its __darkness__
            innate spell.

            If a deculi dies (including from hunger, thirst, or asphyxiation) while
            within the extradimensional space, the shadow sanctuary immediately ends
            and the deculi's corpse is expelled. If the area is illuminated, the corpse
            is not expelled until the sanctuary entrance is in darkness once again.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Create Shadow Sanctuary
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits:
                - conjuration
                - darkness
                - extradimensional
                - occult
          failure: null
          frequency: once per day
          maximum_duration: null
          name: Create Shadow Sanctuary
          range: null
          raw_description: >-
            **Create Shadow Sanctuary** [Two Actions]  (__conjuration__, __darkness__,
            __extradimensional__, __occult__) **Frequency** once per day; **Effect**
            The deculi creates an extradimensional space adjacent to itself in any
            10-foot-by-10-foot area of darkness and enters it. To non-deculis, the
            extradimensional space looks like an ordinary area of darkness (though
            effects such as __true seeing__ can see through this illusion). The deculi's
            infrared vision functions normally between this extradimensional space
            and the area outside its confines, allowing the deculi to sense what is
            going on around it, and its innate __darkness__ spell also extends to
            the area around the shadow sanctuary. Attacks cannot be made into or from
            the extradimensional space.

            The deculi can Dismiss this effect to emerge from its shadow sanctuary,
            but the space it emerges into must be in darkness and must be unobstructed.
            If the area is obstructed, the deculi is shunted to the nearest available
            space and takes 1d6 bludgeoning damage per 5 feet shunted. If the area
            overlaying the shadow sanctuary becomes illuminated by magical light,
            the extradimensional space can no longer be accessed and the deculi is
            trapped within until the area is once again in darkness. The deculi can
            try to extinguish a magical light effect obscuring its sanctuary once
            per day by attempting to counteract the light effect with its __darkness__
            innate spell.

            If a deculi dies (including from hunger, thirst, or asphyxiation) while
            within the extradimensional space, the shadow sanctuary immediately ends
            and the deculi's corpse is expelled. If the area is illuminated, the corpse
            is not expelled until the sanctuary entrance is in darkness once again.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - conjuration
            - darkness
            - extradimensional
            - occult
          trigger: null
        - actioncost: Two Actions
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The deculi Dismisses its shadow sanctuary. It Strides, Climbs, or Flies
            up to its Speed, then makes a fangs Strike that deals an additional 4d6
            precision damage.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Shadow Strike
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Shadow Strike
          range: null
          raw_description: >-
            **Shadow Strike** [Two Actions]  **Requirements** The deculi is in its
            shadow sanctuary. **Effect** The deculi Dismisses its shadow sanctuary.
            It Strides, Climbs, or Flies up to its Speed, then makes a fangs Strike
            that deals an additional 4d6 precision damage.
          requirements: The deculi is in its shadow sanctuary.
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            When the deculi is Hiding in darkness and Sneaks, it can move up to its
            full Speed instead of half its Speed.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                When the deculi is Hiding in darkness and Sneaks, it can move up to
                its full Speed instead of half its Speed.
              duration: null
              failure: null
              name: Walk in Shadow
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Walk in Shadow
          range: null
          raw_description: >-
            **Walk in Shadow** When the deculi is Hiding in darkness and Sneaks, it
            can move up to its full Speed instead of half its Speed.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
      alignment: NE
      description: >-
        These batlike monstrosities inhabit the nooks and crevices of the Darklands,
        where they hang from stalactites and swoop down on unsuspecting prey. Though
        they are completely sightless, deculis possess rudimentary nerves in their
        skulls that allow them to detect the presence of infrared light—information
        they use to locate potential food and threats. They are deadly hunters, thanks
        largely to their magical ability to manipulate the ebb and flow of shadows.
        Deculis prefer to feed upon the warm blood of freshly killed prey, but they
        can subsist on carrion if they have to.
      hp: 215
      hp_misc: null
      immunities:
        - visual
      languages:
        - Undercommon
        - can't speak any language
      level: 12
      melee_attacks:
        - actioncost: One Action
          damage:
            - formula: 3d12+11
              type: piercing
          name: fangs
          to_hit: 24
          traits: null
        - actioncost: One Action
          damage:
            - formula: 3d8+11
              type: bludgeoning
          name: wing
          to_hit: 26
          traits:
            - agile
            - finesse
      name: Deculi
      old_recall_knowledge:
        - dc: 30
          skill: ' - Beast__ (__Arcana__, __Nature__)'
      perception: 23
      ranged_attacks: null
      recall_knowledge:
        - dc: 30
          skill: Arcana
          trait: Beast
        - dc: 30
          skill: Nature
          trait: Beast
      saves:
        fort: 21
        fort_misc: null
        misc: null
        ref: 26
        ref_misc: null
        will: 18
        will_misc: null
      sense_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A deculi can detect infrared radiation as a precise sense.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A deculi can detect infrared radiation as a precise sense.
              duration: null
              failure: null
              name: Infrared Vision
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Infrared Vision
          range: null
          raw_description: >-
            ** Infrared Vision** A deculi can detect infrared radiation as a precise
            sense.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
      senses:
        - infrared vision 60 feet
        - no vision
      size: Large
      skills:
        - bonus: 24
          misc: null
          name: Acrobatics
        - bonus: 24
          misc: null
          name: Athletics
        - bonus: 26
          misc: null
          name: Stealth
      sources:
        - abbr: 'Pathfinder #148: Fires of the Haunted City'
          page_start: '78'
          page_stop: null
      speed:
        - amount: 30
          misc: null
          type: Land
        - amount: 30
          misc: null
          type: climb
        - amount: 50
          misc: null
          type: fly
        - amount: null
          misc: null
          type: walk in shadow
      spell_lists:
        - attack_bonus: null
          cantrips: null
          constants: null
          dc: 32
          focus_points: null
          misc: null
          spell_lists:
            - level: 6
              spells:
                - misc: at will
                  name: darkness
          spells_source: Primal Innate Spells
      traits:
        - Beast
    ability_mods:
      cha_mod: 5
      con_mod: 4
      dex_mod: 7
      int_mod: -3
      str_mod: 5
      wis_mod: 1
    ac: 33
    activities:
      Create Shadow Sanctuary:
        __old:
          actioncost: Two Actions
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The deculi creates an extradimensional space adjacent to itself in any
            10-foot-by-10-foot area of darkness and enters it. To non-deculis, the
            extradimensional space looks like an ordinary area of darkness (though
            effects such as __true seeing__ can see through this illusion). The deculi's
            infrared vision functions normally between this extradimensional space
            and the area outside its confines, allowing the deculi to sense what is
            going on around it, and its innate __darkness__ spell also extends to
            the area around the shadow sanctuary. Attacks cannot be made into or from
            the extradimensional space.

            The deculi can Dismiss this effect to emerge from its shadow sanctuary,
            but the space it emerges into must be in darkness and must be unobstructed.
            If the area is obstructed, the deculi is shunted to the nearest available
            space and takes 1d6 bludgeoning damage per 5 feet shunted. If the area
            overlaying the shadow sanctuary becomes illuminated by magical light,
            the extradimensional space can no longer be accessed and the deculi is
            trapped within until the area is once again in darkness. The deculi can
            try to extinguish a magical light effect obscuring its sanctuary once
            per day by attempting to counteract the light effect with its __darkness__
            innate spell.

            If a deculi dies (including from hunger, thirst, or asphyxiation) while
            within the extradimensional space, the shadow sanctuary immediately ends
            and the deculi's corpse is expelled. If the area is illuminated, the corpse
            is not expelled until the sanctuary entrance is in darkness once again.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Create Shadow Sanctuary
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits:
                - conjuration
                - darkness
                - extradimensional
                - occult
          failure: null
          frequency: once per day
          maximum_duration: null
          name: Create Shadow Sanctuary
          range: null
          raw_description: >-
            **Create Shadow Sanctuary** [Two Actions]  (__conjuration__, __darkness__,
            __extradimensional__, __occult__) **Frequency** once per day; **Effect**
            The deculi creates an extradimensional space adjacent to itself in any
            10-foot-by-10-foot area of darkness and enters it. To non-deculis, the
            extradimensional space looks like an ordinary area of darkness (though
            effects such as __true seeing__ can see through this illusion). The deculi's
            infrared vision functions normally between this extradimensional space
            and the area outside its confines, allowing the deculi to sense what is
            going on around it, and its innate __darkness__ spell also extends to
            the area around the shadow sanctuary. Attacks cannot be made into or from
            the extradimensional space.

            The deculi can Dismiss this effect to emerge from its shadow sanctuary,
            but the space it emerges into must be in darkness and must be unobstructed.
            If the area is obstructed, the deculi is shunted to the nearest available
            space and takes 1d6 bludgeoning damage per 5 feet shunted. If the area
            overlaying the shadow sanctuary becomes illuminated by magical light,
            the extradimensional space can no longer be accessed and the deculi is
            trapped within until the area is once again in darkness. The deculi can
            try to extinguish a magical light effect obscuring its sanctuary once
            per day by attempting to counteract the light effect with its __darkness__
            innate spell.

            If a deculi dies (including from hunger, thirst, or asphyxiation) while
            within the extradimensional space, the shadow sanctuary immediately ends
            and the deculi's corpse is expelled. If the area is illuminated, the corpse
            is not expelled until the sanctuary entrance is in darkness once again.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - conjuration
            - darkness
            - extradimensional
            - occult
          trigger: null
        __original_key: active_abilities
        actioncost: Two Actions
        effects:
          Create Shadow Sanctuary:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Create Shadow Sanctuary
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits:
                - conjuration
                - darkness
                - extradimensional
                - occult
            actioncost: Two Actions
            name: Create Shadow Sanctuary
        name: Create Shadow Sanctuary
      Infrared Vision:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            A deculi can detect infrared radiation as a precise sense.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                A deculi can detect infrared radiation as a precise sense.
              duration: null
              failure: null
              name: Infrared Vision
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Infrared Vision
          range: null
          raw_description: >-
            ** Infrared Vision** A deculi can detect infrared radiation as a precise
            sense.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        __original_key: sense_abilities
        actioncost: None
        effects:
          Infrared Vision:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                A deculi can detect infrared radiation as a precise sense.
              duration: null
              failure: null
              name: Infrared Vision
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: None
            name: Infrared Vision
        name: Infrared Vision
      Shadow Strike:
        __old:
          actioncost: Two Actions
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The deculi Dismisses its shadow sanctuary. It Strides, Climbs, or Flies
            up to its Speed, then makes a fangs Strike that deals an additional 4d6
            precision damage.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Shadow Strike
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Shadow Strike
          range: null
          raw_description: >-
            **Shadow Strike** [Two Actions]  **Requirements** The deculi is in its
            shadow sanctuary. **Effect** The deculi Dismisses its shadow sanctuary.
            It Strides, Climbs, or Flies up to its Speed, then makes a fangs Strike
            that deals an additional 4d6 precision damage.
          requirements: The deculi is in its shadow sanctuary.
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        __original_key: active_abilities
        actioncost: Two Actions
        effects:
          Shadow Strike:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Shadow Strike
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: Two Actions
            name: Shadow Strike
        name: Shadow Strike
      Walk in Shadow:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            When the deculi is Hiding in darkness and Sneaks, it can move up to its
            full Speed instead of half its Speed.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                When the deculi is Hiding in darkness and Sneaks, it can move up to
                its full Speed instead of half its Speed.
              duration: null
              failure: null
              name: Walk in Shadow
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Walk in Shadow
          range: null
          raw_description: >-
            **Walk in Shadow** When the deculi is Hiding in darkness and Sneaks, it
            can move up to its full Speed instead of half its Speed.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        __original_key: active_abilities
        actioncost: None
        effects:
          Walk in Shadow:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                When the deculi is Hiding in darkness and Sneaks, it can move up to
                its full Speed instead of half its Speed.
              duration: null
              failure: null
              name: Walk in Shadow
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '78'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: None
            name: Walk in Shadow
        name: Walk in Shadow
    alignment: NE
    description: >-
      These batlike monstrosities inhabit the nooks and crevices of the Darklands,
      where they hang from stalactites and swoop down on unsuspecting prey. Though
      they are completely sightless, deculis possess rudimentary nerves in their skulls
      that allow them to detect the presence of infrared light—information they use
      to locate potential food and threats. They are deadly hunters, thanks largely
      to their magical ability to manipulate the ebb and flow of shadows. Deculis
      prefer to feed upon the warm blood of freshly killed prey, but they can subsist
      on carrion if they have to.
    hp: 215
    hp_misc: null
    items: null
    languages:
      - Undercommon
      - can't speak any language
    level: 12
    name: Deculi
    perception: 23
    rarity: null
    recall_knowledge:
      - dc: 30
        skill: Arcana
        trait: Beast
      - dc: 30
        skill: Nature
        trait: Beast
    resistances: null
    saves:
      fort: 21
      fort_misc: null
      misc: null
      ref: 26
      ref_misc: null
      will: 18
      will_misc: null
    senses:
      - infrared vision 60 feet
      - no vision
    size: Large
    skills:
      - bonus: 24
        misc: null
        name: Acrobatics
      - bonus: 24
        misc: null
        name: Athletics
      - bonus: 26
        misc: null
        name: Stealth
    sources:
      - abbr: 'Pathfinder #148: Fires of the Haunted City'
        page_start: '78'
        page_stop: null
    speed:
      - amount: 30
        misc: null
        type: Land
      - amount: 30
        misc: null
        type: climb
      - amount: 50
        misc: null
        type: fly
      - amount: null
        misc: null
        type: walk in shadow
    traits:
      - Beast
