items:
  Dragonscarred Dead:
    __old:
      ability_mods:
        cha_mod: 2
        con_mod: 4
        dex_mod: 5
        int_mod: 1
        str_mod: 6
        wis_mod: 1
      ac: 33
      ac_special: null
      active_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The dragonscarred dead's weapon is magically scorched by a dragon's breath.
            When wielded by the dragonscarred dead who carried it in life, the sword
            deals an extra 2d6 negative damage and 3d6 fire damage. In the hands of
            any other creature, it is merely a broken greatsword.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The dragonscarred dead's weapon is magically scorched by a dragon's
                breath. When wielded by the dragonscarred dead who carried it in life,
                the sword deals an extra 2d6 negative damage and 3d6 fire damage.
                In the hands of any other creature, it is merely a broken greatsword.
              duration: null
              failure: null
              name: Breath-Seared Sword
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Breath-Seared Sword
          range: null
          raw_description: >-
            **Breath-Seared Sword** The dragonscarred dead's weapon is magically scorched
            by a dragon's breath. When wielded by the dragonscarred dead who carried
            it in life, the sword deals an extra 2d6 negative damage and 3d6 fire
            damage. In the hands of any other creature, it is merely a broken greatsword.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        - actioncost: Two Actions
          critical_failure: The creature is stunned for 1d4 rounds and is fatigued.
          critical_success: The creature is unaffected.
          description: >-
            The dragonscarred dead focuses its boundless frustration in a gaze targeting
            one creature it can see within 30 feet. That creature must attempt a DC
            30 Will save.
          effect: null
          effects:
            - area: null
              critical_failure: The creature is stunned for 1d4 rounds and is fatigued.
              critical_success: The creature is unaffected.
              description: >-
                The dragonscarred dead focuses its boundless frustration in a gaze
                targeting one creature it can see within 30 feet. That creature must
                attempt a DC 30 Will save.
              duration: null
              failure: The creature is stunned for 1 round.
              name: Glare of Rage
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: The creature is __stunned 1__.
              target: null
              traits:
                - arcane
                - enchantment
                - mental
                - visual
          failure: The creature is stunned for 1 round.
          frequency: null
          maximum_duration: null
          name: Glare of Rage
          range: null
          raw_description: >-
            **Glare of Rage** [Two Actions]  (__arcane__, __enchantment__, __mental__,
            __visual__) The dragonscarred dead focuses its boundless frustration in
            a gaze targeting one creature it can see within 30 feet. That creature
            must attempt a DC 30 Will save.

            **Critical Success** The creature is unaffected.

            **Success** The creature is __stunned 1__.

            **Failure** The creature is stunned for 1 round.

            **Critical Failure** The creature is stunned for 1d4 rounds and is fatigued.
          requirements: null
          saving_throw: null
          stages: null
          success: The creature is __stunned 1__.
          traits:
            - arcane
            - enchantment
            - mental
            - visual
          trigger: null
      alignment: NE
      automatic_abilities:
        - actioncost: None
          critical_failure: The creature is __frightened 4__.
          critical_success: The creature is unaffected by the presence.
          description: >-
            A creature that first enters the area must attempt a Will save. Regardless
            of the result of the saving throw, the creature is temporarily immune
            to this monster's Frightful Presence for 1 minute.
          effect: null
          effects:
            - area: null
              critical_failure: The creature is __frightened 4__.
              critical_success: The creature is unaffected by the presence.
              description: >-
                A creature that first enters the area must attempt a Will save. Regardless
                of the result of the saving throw, the creature is temporarily immune
                to this monster's Frightful Presence for 1 minute.
              duration: null
              failure: The creature is __frightened 2__.
              name: Frightful Presence
              range: 60 feet
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: The creature is __frightened 1__.
              target: null
              traits:
                - aura
                - emotion
                - fear
                - mental
          failure: The creature is __frightened 2__.
          frequency: null
          maximum_duration: null
          name: Frightful Presence
          range: 60 feet
          raw_description: >-
            **Frightful Presence** A creature that first enters the area must attempt
            a Will save. Regardless of the result of the saving throw, the creature
            is temporarily immune to this monster's Frightful Presence for 1 minute.

            **Critical Success** The creature is unaffected by the presence.

            **Success** The creature is __frightened 1__.

            **Failure** The creature is __frightened 2__.

            **Critical Failure** The creature is __frightened 4__.
          requirements: null
          saving_throw: DC 29
          stages: null
          success: The creature is __frightened 1__.
          traits:
            - aura
            - emotion
            - fear
            - mental
          trigger: null
      description: >-
        Not all would-be dragonslayers who die a valiant death rest peacefully amid
        the annals of legend. Sometimes, a draconic necromancer is so impressed by
        its fallen foe that the dragon ritually raises the dead warrior to serve the
        dragon in undeath. These undead guardians are infused with a fraction of the
        power of the dragon who slew them, channeling the same energy as their draconic
        masters' breath.

        Dragonscarred dead are similar to skeletal champions or graveknights in that
        they are not merely reanimated servants who mindlessly follow orders, but
        are instead powerful wardens who maintain the same agency they bore in life.
        While a dragonscarred dead is often magically compelled to follow its reanimator's
        commands, some of these undead dragonslayers manage to escape their masters
        and pursue their own vile agendas—which may involve rallying allies to challenge
        their draconic creator once more and finish the dire mission they started
        in life.
      hp: 210
      hp_misc: regeneration 20 (deactivated by cold)
      immunities:
        - death effects
        - disease
        - fire
        - paralyzed
        - poison
        - unconscious
      items:
        - broken greatsword
      languages:
        - Common
        - Draconic
        - Necril
      level: 13
      melee_attacks:
        - actioncost: One Action
          damage:
            - formula: 1d12+9
              type: slashing
            - formula: 2d6
              type: negative
          name: breath-seared greatsword
          to_hit: 27
          traits:
            - magical
            - versatile P
      name: Dragonscarred Dead
      old_recall_knowledge:
        - dc: 33
          skill: ' - Undead__ (__Religion__)'
      perception: 24
      ranged_attacks: null
      rarity: Uncommon
      recall_knowledge:
        - dc: 33
          skill: Religion
          trait: Undead
      saves:
        fort: 25
        fort_misc: null
        misc: +1 status to all saves vs. magic
        ref: 20
        ref_misc: null
        will: 22
        will_misc: null
      sense_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The dragonscarred dead is immediately aware of any intruder that enters
            the lair of its creator dragon. It does not know the exact location of
            the intruder but remains aware of the intruder's presence until the intruder
            either leaves or is destroyed.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The dragonscarred dead is immediately aware of any intruder that enters
                the lair of its creator dragon. It does not know the exact location
                of the intruder but remains aware of the intruder's presence until
                the intruder either leaves or is destroyed.
              duration: null
              failure: null
              name: Guardian Sense
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: null
              target: null
              traits:
                - arcane
                - detection
                - divination
          failure: null
          frequency: null
          maximum_duration: null
          name: Guardian Sense
          range: null
          raw_description: >-
            **Guardian Sense** (__arcane__, __detection__, __divination__) The dragonscarred
            dead is immediately aware of any intruder that enters the lair of its
            creator dragon. It does not know the exact location of the intruder but
            remains aware of the intruder's presence until the intruder either leaves
            or is destroyed.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - arcane
            - detection
            - divination
          trigger: null
      senses:
        - darkvision
        - guardian sense
      size: Medium
      skills:
        - bonus: 25
          misc: null
          name: Athletics
        - bonus: 22
          misc: null
          name: Dragon Lore
        - bonus: 21
          misc: null
          name: Intimidation
      sources:
        - abbr: 'Pathfinder #148: Fires of the Haunted City'
          page_start: '82'
          page_stop: null
      speed:
        - amount: 25
          misc: null
          type: Land
      spell_lists: null
      traits:
        - Fire
        - Undead
      weaknesses:
        - amount: 10
          misc: null
          type: cold
        - amount: 10
          misc: null
          type: good
    ability_mods:
      cha_mod: 2
      con_mod: 4
      dex_mod: 5
      int_mod: 1
      str_mod: 6
      wis_mod: 1
    ac: 33
    activities:
      Breath-Seared Sword:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The dragonscarred dead's weapon is magically scorched by a dragon's breath.
            When wielded by the dragonscarred dead who carried it in life, the sword
            deals an extra 2d6 negative damage and 3d6 fire damage. In the hands of
            any other creature, it is merely a broken greatsword.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The dragonscarred dead's weapon is magically scorched by a dragon's
                breath. When wielded by the dragonscarred dead who carried it in life,
                the sword deals an extra 2d6 negative damage and 3d6 fire damage.
                In the hands of any other creature, it is merely a broken greatsword.
              duration: null
              failure: null
              name: Breath-Seared Sword
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Breath-Seared Sword
          range: null
          raw_description: >-
            **Breath-Seared Sword** The dragonscarred dead's weapon is magically scorched
            by a dragon's breath. When wielded by the dragonscarred dead who carried
            it in life, the sword deals an extra 2d6 negative damage and 3d6 fire
            damage. In the hands of any other creature, it is merely a broken greatsword.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        __original_key: active_abilities
        actioncost: None
        effects:
          Breath-Seared Sword:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                The dragonscarred dead's weapon is magically scorched by a dragon's
                breath. When wielded by the dragonscarred dead who carried it in life,
                the sword deals an extra 2d6 negative damage and 3d6 fire damage.
                In the hands of any other creature, it is merely a broken greatsword.
              duration: null
              failure: null
              name: Breath-Seared Sword
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: None
            name: Breath-Seared Sword
        name: Breath-Seared Sword
      Frightful Presence:
        __old:
          actioncost: None
          critical_failure: The creature is __frightened 4__.
          critical_success: The creature is unaffected by the presence.
          description: >-
            A creature that first enters the area must attempt a Will save. Regardless
            of the result of the saving throw, the creature is temporarily immune
            to this monster's Frightful Presence for 1 minute.
          effect: null
          effects:
            - area: null
              critical_failure: The creature is __frightened 4__.
              critical_success: The creature is unaffected by the presence.
              description: >-
                A creature that first enters the area must attempt a Will save. Regardless
                of the result of the saving throw, the creature is temporarily immune
                to this monster's Frightful Presence for 1 minute.
              duration: null
              failure: The creature is __frightened 2__.
              name: Frightful Presence
              range: 60 feet
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: The creature is __frightened 1__.
              target: null
              traits:
                - aura
                - emotion
                - fear
                - mental
          failure: The creature is __frightened 2__.
          frequency: null
          maximum_duration: null
          name: Frightful Presence
          range: 60 feet
          raw_description: >-
            **Frightful Presence** A creature that first enters the area must attempt
            a Will save. Regardless of the result of the saving throw, the creature
            is temporarily immune to this monster's Frightful Presence for 1 minute.

            **Critical Success** The creature is unaffected by the presence.

            **Success** The creature is __frightened 1__.

            **Failure** The creature is __frightened 2__.

            **Critical Failure** The creature is __frightened 4__.
          requirements: null
          saving_throw: DC 29
          stages: null
          success: The creature is __frightened 1__.
          traits:
            - aura
            - emotion
            - fear
            - mental
          trigger: null
        __original_key: automatic_abilities
        actioncost: None
        effects:
          Frightful Presence:
            __old:
              area: null
              critical_failure: The creature is __frightened 4__.
              critical_success: The creature is unaffected by the presence.
              description: >-
                A creature that first enters the area must attempt a Will save. Regardless
                of the result of the saving throw, the creature is temporarily immune
                to this monster's Frightful Presence for 1 minute.
              duration: null
              failure: The creature is __frightened 2__.
              name: Frightful Presence
              range: 60 feet
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: The creature is __frightened 1__.
              target: null
              traits:
                - aura
                - emotion
                - fear
                - mental
            actioncost: None
            name: Frightful Presence
        name: Frightful Presence
      Glare of Rage:
        __old:
          actioncost: Two Actions
          critical_failure: The creature is stunned for 1d4 rounds and is fatigued.
          critical_success: The creature is unaffected.
          description: >-
            The dragonscarred dead focuses its boundless frustration in a gaze targeting
            one creature it can see within 30 feet. That creature must attempt a DC
            30 Will save.
          effect: null
          effects:
            - area: null
              critical_failure: The creature is stunned for 1d4 rounds and is fatigued.
              critical_success: The creature is unaffected.
              description: >-
                The dragonscarred dead focuses its boundless frustration in a gaze
                targeting one creature it can see within 30 feet. That creature must
                attempt a DC 30 Will save.
              duration: null
              failure: The creature is stunned for 1 round.
              name: Glare of Rage
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: The creature is __stunned 1__.
              target: null
              traits:
                - arcane
                - enchantment
                - mental
                - visual
          failure: The creature is stunned for 1 round.
          frequency: null
          maximum_duration: null
          name: Glare of Rage
          range: null
          raw_description: >-
            **Glare of Rage** [Two Actions]  (__arcane__, __enchantment__, __mental__,
            __visual__) The dragonscarred dead focuses its boundless frustration in
            a gaze targeting one creature it can see within 30 feet. That creature
            must attempt a DC 30 Will save.

            **Critical Success** The creature is unaffected.

            **Success** The creature is __stunned 1__.

            **Failure** The creature is stunned for 1 round.

            **Critical Failure** The creature is stunned for 1d4 rounds and is fatigued.
          requirements: null
          saving_throw: null
          stages: null
          success: The creature is __stunned 1__.
          traits:
            - arcane
            - enchantment
            - mental
            - visual
          trigger: null
        __original_key: active_abilities
        actioncost: Two Actions
        effects:
          Glare of Rage:
            __old:
              area: null
              critical_failure: The creature is stunned for 1d4 rounds and is fatigued.
              critical_success: The creature is unaffected.
              description: >-
                The dragonscarred dead focuses its boundless frustration in a gaze
                targeting one creature it can see within 30 feet. That creature must
                attempt a DC 30 Will save.
              duration: null
              failure: The creature is stunned for 1 round.
              name: Glare of Rage
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: The creature is __stunned 1__.
              target: null
              traits:
                - arcane
                - enchantment
                - mental
                - visual
            actioncost: Two Actions
            name: Glare of Rage
        name: Glare of Rage
      Guardian Sense:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The dragonscarred dead is immediately aware of any intruder that enters
            the lair of its creator dragon. It does not know the exact location of
            the intruder but remains aware of the intruder's presence until the intruder
            either leaves or is destroyed.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The dragonscarred dead is immediately aware of any intruder that enters
                the lair of its creator dragon. It does not know the exact location
                of the intruder but remains aware of the intruder's presence until
                the intruder either leaves or is destroyed.
              duration: null
              failure: null
              name: Guardian Sense
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: null
              target: null
              traits:
                - arcane
                - detection
                - divination
          failure: null
          frequency: null
          maximum_duration: null
          name: Guardian Sense
          range: null
          raw_description: >-
            **Guardian Sense** (__arcane__, __detection__, __divination__) The dragonscarred
            dead is immediately aware of any intruder that enters the lair of its
            creator dragon. It does not know the exact location of the intruder but
            remains aware of the intruder's presence until the intruder either leaves
            or is destroyed.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - arcane
            - detection
            - divination
          trigger: null
        __original_key: sense_abilities
        actioncost: None
        effects:
          Guardian Sense:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                The dragonscarred dead is immediately aware of any intruder that enters
                the lair of its creator dragon. It does not know the exact location
                of the intruder but remains aware of the intruder's presence until
                the intruder either leaves or is destroyed.
              duration: null
              failure: null
              name: Guardian Sense
              range: null
              sources:
                - abbr: 'Pathfinder #148: Fires of the Haunted City'
                  page_start: '82'
                  page_stop: null
              success: null
              target: null
              traits:
                - arcane
                - detection
                - divination
            actioncost: None
            name: Guardian Sense
        name: Guardian Sense
    alignment: NE
    description: >-
      Not all would-be dragonslayers who die a valiant death rest peacefully amid
      the annals of legend. Sometimes, a draconic necromancer is so impressed by its
      fallen foe that the dragon ritually raises the dead warrior to serve the dragon
      in undeath. These undead guardians are infused with a fraction of the power
      of the dragon who slew them, channeling the same energy as their draconic masters'
      breath.

      Dragonscarred dead are similar to skeletal champions or graveknights in that
      they are not merely reanimated servants who mindlessly follow orders, but are
      instead powerful wardens who maintain the same agency they bore in life. While
      a dragonscarred dead is often magically compelled to follow its reanimator's
      commands, some of these undead dragonslayers manage to escape their masters
      and pursue their own vile agendas—which may involve rallying allies to challenge
      their draconic creator once more and finish the dire mission they started in
      life.
    hp: 210
    hp_misc: regeneration 20 (deactivated by cold)
    items:
      - broken greatsword
    languages:
      - Common
      - Draconic
      - Necril
    level: 13
    name: Dragonscarred Dead
    perception: 24
    rarity: Uncommon
    recall_knowledge:
      - dc: 33
        skill: Religion
        trait: Undead
    resistances: null
    saves:
      fort: 25
      fort_misc: null
      misc: +1 status to all saves vs. magic
      ref: 20
      ref_misc: null
      will: 22
      will_misc: null
    senses:
      - darkvision
      - guardian sense
    size: Medium
    skills:
      - bonus: 25
        misc: null
        name: Athletics
      - bonus: 22
        misc: null
        name: Dragon Lore
      - bonus: 21
        misc: null
        name: Intimidation
    sources:
      - abbr: 'Pathfinder #148: Fires of the Haunted City'
        page_start: '82'
        page_stop: null
    speed:
      - amount: 25
        misc: null
        type: Land
    traits:
      - Fire
      - Undead
