items:
  Remnant of Barzillai:
    __old:
      ability_mods:
        cha_mod: 5
        con_mod: 0
        dex_mod: 3
        int_mod: 3
        str_mod: -5
        wis_mod: 7
      ac: 29
      ac_special: null
      active_abilities:
        - actioncost: Three Actions
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The remnant of Barzillai stirs up and amplifies unsettled spirits in the
            area to create a single haunt of 6th level or lower at his present location.
            This haunt is permanent until it is disabled or the remnant of Barzillai
            is destroyed.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Create Haunt
              range: null
              sources:
                - abbr: 'Pathfinder #147: Tomorrow Must Burn'
                  page_start: '80'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - necromancy
          failure: null
          frequency: three times per day
          maximum_duration: null
          name: Create Haunt
          range: null
          raw_description: >-
            **Create Haunt** [Three Actions]  (__divine__, __necromancy__) **Frequency**
            three times per day; **Requirements** The remnant of Barzillai is within
            Ravounel. **Effect** The remnant of Barzillai stirs up and amplifies unsettled
            spirits in the area to create a single haunt of 6th level or lower at
            his present location. This haunt is permanent until it is disabled or
            the remnant of Barzillai is destroyed.
          requirements: The remnant of Barzillai is within Ravounel.
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - necromancy
          trigger: null
      alignment: LE
      automatic_abilities:
        - actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The remnant of Barzillai draws on the power of haunts, reinforcing his
            form when they are nearby. His resistance to all damage increases to 12
            while he's within 60 feet of a haunt. When a haunt within 60 feet of Barzillai
            is disabled, he takes 20 force damage from the backlash.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The remnant of Barzillai draws on the power of haunts, reinforcing
                his form when they are nearby. His resistance to all damage increases
                to 12 while he's within 60 feet of a haunt. When a haunt within 60
                feet of Barzillai is disabled, he takes 20 force damage from the backlash.
              duration: null
              failure: null
              name: Haunted Form
              range: null
              sources:
                - abbr: 'Pathfinder #147: Tomorrow Must Burn'
                  page_start: '80'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - necromancy
          failure: null
          frequency: null
          maximum_duration: null
          name: Haunted Form
          range: null
          raw_description: >-
            **Haunted Form** (__divine__, __necromancy__) The remnant of Barzillai
            draws on the power of haunts, reinforcing his form when they are nearby.
            His resistance to all damage increases to 12 while he's within 60 feet
            of a haunt. When a haunt within 60 feet of Barzillai is disabled, he takes
            20 force damage from the backlash.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - necromancy
          trigger: null
      description: >-
        Though the Silver Ravens defeated the evil tyrant Barzillai Thrune before
        he could fully realize his goal of becoming a genius loci, remnants of his
        spirit remain throughout the Ravounel region. On occasion, these scraps of
        sentience form a vague, Barzillai-shaped body from bits of the nearby terrain,
        but when near haunted locations, the spectral force can coalesce into a more
        precise body using ectoplasm siphoned from these haunts. In either form, the
        remnant carries a form of Barzillai's own burning mace, which dissolves into
        a thin mist 1 hour after the remnant is destroyed.

        Remnants of Barzillai can manifest anywhere throughout Ravounel, and even
        after one remnant is defeated, another might arise later on. The undead inquisitor
        can be put to rest permanently only by retrieving his mace before it disappears,
        then burying it in a location consecrated by good clerics, where it must remain
        for at least a year undisturbed.

        While a remnant of Barzillai can draw power from any haunt it is associated
        with, remnants tend to do so with specific haunts. The haunts presented on
        pages 27–28 are commonly associated with remnants of Barzillai. These are
        far from the only haunts Barzillai can create, however; the Silver Ravens'
        records mention many phenomena associated with the undead being, some of which
        deal with themes of various birds, the scents of mint and tea, and ghostly
        figures in heavy armor.
      hp: 135
      hp_misc: null
      immunities:
        - death effects
        - disease
        - paralyzed
        - poison
        - precision
        - unconscious
      languages:
        - Aklo
        - Azlanti
        - Common
        - Draconic
        - Elven
        - Gnomish
        - Halfling
        - Infernal
        - Jotun
        - Shadowtongue
        - Strix
        - Varisian
      level: 10
      melee_attacks:
        - actioncost: One Action
          damage:
            - formula: 2d6+8
              type: bludgeoning
            - formula: 2d6
              type: fire
          name: burning mace
          to_hit: 22
          traits:
            - magical
            - fire
            - shove
      name: Remnant of Barzillai
      old_recall_knowledge:
        - dc: 32
          skill: ' - Undead__ (__Religion__)'
      perception: 22
      ranged_attacks: null
      rarity: Rare
      recall_knowledge:
        - dc: 32
          skill: Religion
          trait: Undead
      resistances:
        - amount: 8
          misc: null
          type: all damage
      saves:
        fort: 14
        fort_misc: null
        misc: null
        ref: 21
        ref_misc: null
        will: 23
        will_misc: null
      senses:
        - darkvision
      size: Medium
      skills:
        - bonus: 21
          misc: null
          name: Arcana
        - bonus: 23
          misc: null
          name: Deception
        - bonus: 21
          misc: null
          name: Religion
        - bonus: 19
          misc: null
          name: Society
        - bonus: 21
          misc: null
          name: Torture Lore
      sources:
        - abbr: 'Pathfinder #147: Tomorrow Must Burn'
          page_start: '80'
          page_stop: null
      speed:
        - amount: 25
          misc: null
          type: fly
      spell_lists: null
      traits:
        - Incorporeal
        - Undead
    ability_mods:
      cha_mod: 5
      con_mod: 0
      dex_mod: 3
      int_mod: 3
      str_mod: -5
      wis_mod: 7
    ac: 29
    activities:
      Create Haunt:
        __old:
          actioncost: Three Actions
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            The remnant of Barzillai stirs up and amplifies unsettled spirits in the
            area to create a single haunt of 6th level or lower at his present location.
            This haunt is permanent until it is disabled or the remnant of Barzillai
            is destroyed.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Create Haunt
              range: null
              sources:
                - abbr: 'Pathfinder #147: Tomorrow Must Burn'
                  page_start: '80'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - necromancy
          failure: null
          frequency: three times per day
          maximum_duration: null
          name: Create Haunt
          range: null
          raw_description: >-
            **Create Haunt** [Three Actions]  (__divine__, __necromancy__) **Frequency**
            three times per day; **Requirements** The remnant of Barzillai is within
            Ravounel. **Effect** The remnant of Barzillai stirs up and amplifies unsettled
            spirits in the area to create a single haunt of 6th level or lower at
            his present location. This haunt is permanent until it is disabled or
            the remnant of Barzillai is destroyed.
          requirements: The remnant of Barzillai is within Ravounel.
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - necromancy
          trigger: null
        __original_key: active_abilities
        actioncost: Three Actions
        effects:
          Create Haunt:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Create Haunt
              range: null
              sources:
                - abbr: 'Pathfinder #147: Tomorrow Must Burn'
                  page_start: '80'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - necromancy
            actioncost: Three Actions
            name: Create Haunt
        name: Create Haunt
      Haunted Form:
        __old:
          actioncost: None
          critical_failure: null
          critical_success: null
          description: >-
            The remnant of Barzillai draws on the power of haunts, reinforcing his
            form when they are nearby. His resistance to all damage increases to 12
            while he's within 60 feet of a haunt. When a haunt within 60 feet of Barzillai
            is disabled, he takes 20 force damage from the backlash.
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: >-
                The remnant of Barzillai draws on the power of haunts, reinforcing
                his form when they are nearby. His resistance to all damage increases
                to 12 while he's within 60 feet of a haunt. When a haunt within 60
                feet of Barzillai is disabled, he takes 20 force damage from the backlash.
              duration: null
              failure: null
              name: Haunted Form
              range: null
              sources:
                - abbr: 'Pathfinder #147: Tomorrow Must Burn'
                  page_start: '80'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - necromancy
          failure: null
          frequency: null
          maximum_duration: null
          name: Haunted Form
          range: null
          raw_description: >-
            **Haunted Form** (__divine__, __necromancy__) The remnant of Barzillai
            draws on the power of haunts, reinforcing his form when they are nearby.
            His resistance to all damage increases to 12 while he's within 60 feet
            of a haunt. When a haunt within 60 feet of Barzillai is disabled, he takes
            20 force damage from the backlash.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits:
            - divine
            - necromancy
          trigger: null
        __original_key: automatic_abilities
        actioncost: None
        effects:
          Haunted Form:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: >-
                The remnant of Barzillai draws on the power of haunts, reinforcing
                his form when they are nearby. His resistance to all damage increases
                to 12 while he's within 60 feet of a haunt. When a haunt within 60
                feet of Barzillai is disabled, he takes 20 force damage from the backlash.
              duration: null
              failure: null
              name: Haunted Form
              range: null
              sources:
                - abbr: 'Pathfinder #147: Tomorrow Must Burn'
                  page_start: '80'
                  page_stop: null
              success: null
              target: null
              traits:
                - divine
                - necromancy
            actioncost: None
            name: Haunted Form
        name: Haunted Form
    alignment: LE
    description: >-
      Though the Silver Ravens defeated the evil tyrant Barzillai Thrune before he
      could fully realize his goal of becoming a genius loci, remnants of his spirit
      remain throughout the Ravounel region. On occasion, these scraps of sentience
      form a vague, Barzillai-shaped body from bits of the nearby terrain, but when
      near haunted locations, the spectral force can coalesce into a more precise
      body using ectoplasm siphoned from these haunts. In either form, the remnant
      carries a form of Barzillai's own burning mace, which dissolves into a thin
      mist 1 hour after the remnant is destroyed.

      Remnants of Barzillai can manifest anywhere throughout Ravounel, and even after
      one remnant is defeated, another might arise later on. The undead inquisitor
      can be put to rest permanently only by retrieving his mace before it disappears,
      then burying it in a location consecrated by good clerics, where it must remain
      for at least a year undisturbed.

      While a remnant of Barzillai can draw power from any haunt it is associated
      with, remnants tend to do so with specific haunts. The haunts presented on pages
      27–28 are commonly associated with remnants of Barzillai. These are far from
      the only haunts Barzillai can create, however; the Silver Ravens' records mention
      many phenomena associated with the undead being, some of which deal with themes
      of various birds, the scents of mint and tea, and ghostly figures in heavy armor.
    hp: 135
    hp_misc: null
    items: null
    languages:
      - Aklo
      - Azlanti
      - Common
      - Draconic
      - Elven
      - Gnomish
      - Halfling
      - Infernal
      - Jotun
      - Shadowtongue
      - Strix
      - Varisian
    level: 10
    name: Remnant of Barzillai
    perception: 22
    rarity: Rare
    recall_knowledge:
      - dc: 32
        skill: Religion
        trait: Undead
    resistances:
      - amount: 8
        misc: null
        type: all damage
    saves:
      fort: 14
      fort_misc: null
      misc: null
      ref: 21
      ref_misc: null
      will: 23
      will_misc: null
    senses:
      - darkvision
    size: Medium
    skills:
      - bonus: 21
        misc: null
        name: Arcana
      - bonus: 23
        misc: null
        name: Deception
      - bonus: 21
        misc: null
        name: Religion
      - bonus: 19
        misc: null
        name: Society
      - bonus: 21
        misc: null
        name: Torture Lore
    sources:
      - abbr: 'Pathfinder #147: Tomorrow Must Burn'
        page_start: '80'
        page_stop: null
    speed:
      - amount: 25
        misc: null
        type: fly
    traits:
      - Incorporeal
      - Undead
