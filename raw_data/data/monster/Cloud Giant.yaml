items:
  Cloud Giant:
    __old:
      ability_mods:
        cha_mod: 1
        con_mod: 5
        dex_mod: 0
        int_mod: 1
        str_mod: 7
        wis_mod: 3
      ac: 30
      ac_special: null
      active_abilities:
        - actioncost: One Action
          critical_failure: null
          critical_success: null
          description: ''
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Throw Rock
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Throw Rock
          range: null
          raw_description: '**Throw Rock** [One Action]'
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        - actioncost: Two Actions
          critical_failure: >-
            As failure, but double damage and also knocked __prone__.
          critical_success: The creature is unaffected.
          description: >-
            The cloud giant Strikes a creature with its ranseur, surrounded in a roar
            of rushing air. On a hit, the target takes an additional 4d8 bludgeoning
            damage and is __deafened__ for 1 minute. Whether or not the Strike hits,
            each non-cloud giant within a 20-foot emanation, including the target
            of the Strike, is buffeted by roaring winds and must attempt a DC 30 Fortitude
            saving throw.
          effect: null
          effects:
            - area: null
              critical_failure: >-
                As failure, but double damage and also knocked __prone__.
              critical_success: The creature is unaffected.
              description: >-
                The cloud giant Strikes a creature with its ranseur, surrounded in
                a roar of rushing air. On a hit, the target takes an additional 4d8
                bludgeoning damage and is __deafened__ for 1 minute. Whether or not
                the Strike hits, each non-cloud giant within a 20-foot emanation,
                including the target of the Strike, is buffeted by roaring winds and
                must attempt a DC 30 Fortitude saving throw.
              duration: null
              failure: >-
                The creature takes 4d8 sonic damage and is __deafened__ until the
                end of its next turn.
              name: Wind Strike
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: The creature takes 2d8 sonic damage.
              target: null
              traits:
                - air
                - evocation
                - primal
          failure: >-
            The creature takes 4d8 sonic damage and is __deafened__ until the end
            of its next turn.
          frequency: null
          maximum_duration: null
          name: Wind Strike
          range: null
          raw_description: >-
            **Wind Strike** [Two Actions]  (__air__, __evocation__, __primal__) The
            cloud giant Strikes a creature with its ranseur, surrounded in a roar
            of rushing air. On a hit, the target takes an additional 4d8 bludgeoning
            damage and is __deafened__ for 1 minute. Whether or not the Strike hits,
            each non-cloud giant within a 20-foot emanation, including the target
            of the Strike, is buffeted by roaring winds and must attempt a DC 30 Fortitude
            saving throw.

            **Critical Success** The creature is unaffected.

            **Success** The creature takes 2d8 sonic damage.

            **Failure** The creature takes 4d8 sonic damage and is __deafened__ until
            the end of its next turn.

            **Critical Failure** As failure, but double damage and also knocked __prone__.
          requirements: null
          saving_throw: null
          stages: null
          success: The creature takes 2d8 sonic damage.
          traits:
            - air
            - evocation
            - primal
          trigger: null
      alignment: N
      automatic_abilities:
        - actioncost: Reaction
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            You lash out at a foe that leaves an opening. Make a melee Strike against
            the triggering creature. If your attack is a critical hit and the trigger
            was a manipulate action, you disrupt that action. This Strike doesn't
            count toward your multiple attack penalty, and your multiple attack penalty
            doesn't apply to this Strike.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Attack of Opportunity
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Attack of Opportunity
          range: null
          raw_description: >-
            **Attack of Opportunity** [Reaction] **Trigger** A creature within your
            reach uses a manipulate action or a move action, makes a ranged attack,
            or leaves a square during a move action it's using. **Effect** You lash
            out at a foe that leaves an opening. Make a melee Strike against the triggering
            creature. If your attack is a critical hit and the trigger was a manipulate
            action, you disrupt that action. This Strike doesn't count toward your
            multiple attack penalty, and your multiple attack penalty doesn't apply
            to this Strike.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: >-
            A creature within your reach uses a manipulate action or a move action,
            makes a ranged attack, or leaves a square during a move action it's using.
        - actioncost: Reaction
          critical_failure: null
          critical_success: null
          description: ''
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Catch Rock
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Catch Rock
          range: null
          raw_description: '**Catch Rock** [Reaction]'
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
      description: >-
        The graceful and regal cloud giants have skin of milky white to powdery blue.
        Unlike most giants, cloud giants are quite morally diverse. A handful are
        neutral, but of the others, roughly half are good, while the other half are
        evil. Good cloud giants are often civic-minded builders of roads and settlements,
        and are interested in trading goods as well as cultural innovations. It's
        not uncommon for such giants to approach their neighbors with diplomacy and
        build strong relationships with other peaceful peoples. Conversely, evil cloud
        giants are often isolationist and xenophobic. Preferring hidden mountain valleys
        and settlements in the caves and among the crags of lonely peaks, they raid
        for what they want and treat other creatures like inconsequential insects.
        These opposing philosophies can generate a great deal of strife among neighboring
        cloud giant communities, and the raiders often threaten the trade deals of
        their peaceful cousins.

        Legends persist of floating cities ruled by magically gifted cloud giant queens
        and kings. While most cloud giants plainly state that such claims are pure
        fantasy, others are mysteriously tight-lipped or evasive about the matter.
      hp: 220
      hp_misc: null
      items:
        - +1 striking ranseur
        - sack with 5 rocks
      languages:
        - Common
        - Jotun
      level: 11
      melee_attacks:
        - actioncost: One Action
          damage:
            - formula: 2d10+13
              type: piercing
          name: ranseur
          to_hit: 25
          traits:
            - disarm
            - magical
            - reach 20 feet
        - actioncost: One Action
          damage:
            - formula: 2d8+13
              type: bludgeoning
          name: fist
          to_hit: 24
          traits:
            - agile
            - reach 15 feet
      name: Cloud Giant
      old_recall_knowledge:
        - dc: 28
          skill: ' - Humanoid__ (__Society__)'
      perception: 22
      ranged_attacks:
        - actioncost: One Action
          damage:
            - formula: 2d10+13
              type: bludgeoning
          name: rock
          to_hit: 24
          traits:
            - brutal
            - range increment 120 feet
      recall_knowledge:
        - dc: 28
          skill: Society
          trait: Humanoid
      saves:
        fort: 25
        fort_misc: null
        misc: null
        ref: 18
        ref_misc: null
        will: 21
        will_misc: null
      senses:
        - low-light vision
        - scent (imprecise) 30 feet
      size: Huge
      skills:
        - bonus: 26
          misc: null
          name: Athletics
        - bonus: 21
          misc: null
          name: Crafting
        - bonus: 24
          misc: null
          name: Diplomacy
        - bonus: 26
          misc: null
          name: Intimidation
        - bonus: 21
          misc: null
          name: Performance
      sources:
        - abbr: Bestiary
          page_start: '173'
          page_stop: null
      speed:
        - amount: 30
          misc: null
          type: Land
      spell_lists:
        - attack_bonus: null
          cantrips: null
          constants: null
          dc: 30
          focus_points: null
          misc: null
          spell_lists:
            - level: 4
              spells:
                - misc: null
                  name: solid fog
            - level: 3
              spells:
                - misc: at will
                  name: levitate
            - level: 2
              spells:
                - misc: at will
                  name: obscuring mist
          spells_source: Primal Innate Spells
      traits:
        - Air
        - Giant
        - Humanoid
    ability_mods:
      cha_mod: 1
      con_mod: 5
      dex_mod: 0
      int_mod: 1
      str_mod: 7
      wis_mod: 3
    ac: 30
    activities:
      Attack of Opportunity:
        __old:
          actioncost: Reaction
          critical_failure: null
          critical_success: null
          description: ''
          effect: >-
            You lash out at a foe that leaves an opening. Make a melee Strike against
            the triggering creature. If your attack is a critical hit and the trigger
            was a manipulate action, you disrupt that action. This Strike doesn't
            count toward your multiple attack penalty, and your multiple attack penalty
            doesn't apply to this Strike.
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Attack of Opportunity
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Attack of Opportunity
          range: null
          raw_description: >-
            **Attack of Opportunity** [Reaction] **Trigger** A creature within your
            reach uses a manipulate action or a move action, makes a ranged attack,
            or leaves a square during a move action it's using. **Effect** You lash
            out at a foe that leaves an opening. Make a melee Strike against the triggering
            creature. If your attack is a critical hit and the trigger was a manipulate
            action, you disrupt that action. This Strike doesn't count toward your
            multiple attack penalty, and your multiple attack penalty doesn't apply
            to this Strike.
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: >-
            A creature within your reach uses a manipulate action or a move action,
            makes a ranged attack, or leaves a square during a move action it's using.
        __original_key: automatic_abilities
        actioncost: Reaction
        effects:
          Attack of Opportunity:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Attack of Opportunity
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: Reaction
            name: Attack of Opportunity
        name: Attack of Opportunity
      Catch Rock:
        __old:
          actioncost: Reaction
          critical_failure: null
          critical_success: null
          description: ''
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Catch Rock
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Catch Rock
          range: null
          raw_description: '**Catch Rock** [Reaction]'
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        __original_key: automatic_abilities
        actioncost: Reaction
        effects:
          Catch Rock:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Catch Rock
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: Reaction
            name: Catch Rock
        name: Catch Rock
      Throw Rock:
        __old:
          actioncost: One Action
          critical_failure: null
          critical_success: null
          description: ''
          effect: null
          effects:
            - area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Throw Rock
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
          failure: null
          frequency: null
          maximum_duration: null
          name: Throw Rock
          range: null
          raw_description: '**Throw Rock** [One Action]'
          requirements: null
          saving_throw: null
          stages: null
          success: null
          traits: null
          trigger: null
        __original_key: active_abilities
        actioncost: One Action
        effects:
          Throw Rock:
            __old:
              area: null
              critical_failure: null
              critical_success: null
              description: ''
              duration: null
              failure: null
              name: Throw Rock
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: null
              target: null
              traits: null
            actioncost: One Action
            name: Throw Rock
        name: Throw Rock
      Wind Strike:
        __old:
          actioncost: Two Actions
          critical_failure: >-
            As failure, but double damage and also knocked __prone__.
          critical_success: The creature is unaffected.
          description: >-
            The cloud giant Strikes a creature with its ranseur, surrounded in a roar
            of rushing air. On a hit, the target takes an additional 4d8 bludgeoning
            damage and is __deafened__ for 1 minute. Whether or not the Strike hits,
            each non-cloud giant within a 20-foot emanation, including the target
            of the Strike, is buffeted by roaring winds and must attempt a DC 30 Fortitude
            saving throw.
          effect: null
          effects:
            - area: null
              critical_failure: >-
                As failure, but double damage and also knocked __prone__.
              critical_success: The creature is unaffected.
              description: >-
                The cloud giant Strikes a creature with its ranseur, surrounded in
                a roar of rushing air. On a hit, the target takes an additional 4d8
                bludgeoning damage and is __deafened__ for 1 minute. Whether or not
                the Strike hits, each non-cloud giant within a 20-foot emanation,
                including the target of the Strike, is buffeted by roaring winds and
                must attempt a DC 30 Fortitude saving throw.
              duration: null
              failure: >-
                The creature takes 4d8 sonic damage and is __deafened__ until the
                end of its next turn.
              name: Wind Strike
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: The creature takes 2d8 sonic damage.
              target: null
              traits:
                - air
                - evocation
                - primal
          failure: >-
            The creature takes 4d8 sonic damage and is __deafened__ until the end
            of its next turn.
          frequency: null
          maximum_duration: null
          name: Wind Strike
          range: null
          raw_description: >-
            **Wind Strike** [Two Actions]  (__air__, __evocation__, __primal__) The
            cloud giant Strikes a creature with its ranseur, surrounded in a roar
            of rushing air. On a hit, the target takes an additional 4d8 bludgeoning
            damage and is __deafened__ for 1 minute. Whether or not the Strike hits,
            each non-cloud giant within a 20-foot emanation, including the target
            of the Strike, is buffeted by roaring winds and must attempt a DC 30 Fortitude
            saving throw.

            **Critical Success** The creature is unaffected.

            **Success** The creature takes 2d8 sonic damage.

            **Failure** The creature takes 4d8 sonic damage and is __deafened__ until
            the end of its next turn.

            **Critical Failure** As failure, but double damage and also knocked __prone__.
          requirements: null
          saving_throw: null
          stages: null
          success: The creature takes 2d8 sonic damage.
          traits:
            - air
            - evocation
            - primal
          trigger: null
        __original_key: active_abilities
        actioncost: Two Actions
        effects:
          Wind Strike:
            __old:
              area: null
              critical_failure: >-
                As failure, but double damage and also knocked __prone__.
              critical_success: The creature is unaffected.
              description: >-
                The cloud giant Strikes a creature with its ranseur, surrounded in
                a roar of rushing air. On a hit, the target takes an additional 4d8
                bludgeoning damage and is __deafened__ for 1 minute. Whether or not
                the Strike hits, each non-cloud giant within a 20-foot emanation,
                including the target of the Strike, is buffeted by roaring winds and
                must attempt a DC 30 Fortitude saving throw.
              duration: null
              failure: >-
                The creature takes 4d8 sonic damage and is __deafened__ until the
                end of its next turn.
              name: Wind Strike
              range: null
              sources:
                - abbr: Bestiary
                  page_start: '173'
                  page_stop: null
              success: The creature takes 2d8 sonic damage.
              target: null
              traits:
                - air
                - evocation
                - primal
            actioncost: Two Actions
            name: Wind Strike
        name: Wind Strike
    alignment: N
    description: >-
      The graceful and regal cloud giants have skin of milky white to powdery blue.
      Unlike most giants, cloud giants are quite morally diverse. A handful are neutral,
      but of the others, roughly half are good, while the other half are evil. Good
      cloud giants are often civic-minded builders of roads and settlements, and are
      interested in trading goods as well as cultural innovations. It's not uncommon
      for such giants to approach their neighbors with diplomacy and build strong
      relationships with other peaceful peoples. Conversely, evil cloud giants are
      often isolationist and xenophobic. Preferring hidden mountain valleys and settlements
      in the caves and among the crags of lonely peaks, they raid for what they want
      and treat other creatures like inconsequential insects. These opposing philosophies
      can generate a great deal of strife among neighboring cloud giant communities,
      and the raiders often threaten the trade deals of their peaceful cousins.

      Legends persist of floating cities ruled by magically gifted cloud giant queens
      and kings. While most cloud giants plainly state that such claims are pure fantasy,
      others are mysteriously tight-lipped or evasive about the matter.
    hp: 220
    hp_misc: null
    items:
      - +1 striking ranseur
      - sack with 5 rocks
    languages:
      - Common
      - Jotun
    level: 11
    name: Cloud Giant
    perception: 22
    rarity: null
    recall_knowledge:
      - dc: 28
        skill: Society
        trait: Humanoid
    resistances: null
    saves:
      fort: 25
      fort_misc: null
      misc: null
      ref: 18
      ref_misc: null
      will: 21
      will_misc: null
    senses:
      - low-light vision
      - scent (imprecise) 30 feet
    size: Huge
    skills:
      - bonus: 26
        misc: null
        name: Athletics
      - bonus: 21
        misc: null
        name: Crafting
      - bonus: 24
        misc: null
        name: Diplomacy
      - bonus: 26
        misc: null
        name: Intimidation
      - bonus: 21
        misc: null
        name: Performance
    sources:
      - abbr: Bestiary
        page_start: '173'
        page_stop: null
    speed:
      - amount: 30
        misc: null
        type: Land
    traits:
      - Air
      - Giant
      - Humanoid
