from __future__ import annotations

from pathlib import Path
from typing import TYPE_CHECKING

import pytest

from pf2._compat.yaml import make_yaml
from pf2.dto import Database, Entity

if TYPE_CHECKING:
    from typing import Callable, Iterable, Type, TypeVar

    T = TypeVar("T", bound=Entity)


@pytest.fixture
def yaml():
    return make_yaml()


@pytest.fixture
def data_root():
    return Path(__file__).parent.parent.joinpath("raw_data").resolve()


@pytest.fixture
def database(data_root, yaml):
    return Database(data_root, yaml)


@pytest.fixture
def entities_reader(yaml, data_root: Path) -> Callable[[Type[T]], Iterable[T]]:
    def reader(cls: Type[T]) -> Iterable[T]:
        directory = data_root.joinpath(cls.__collection__)
        filepath = data_root.joinpath(f"{cls.__collection__}.yaml")
        if directory.is_dir():
            for source_file in directory.glob("*.yaml"):
                raw_content = yaml.load(source_file)
                for _, entry in raw_content["items"].items():
                    yield cls.parse_obj(entry)
        elif filepath.is_file():
            raw_content = yaml.load(filepath)
            for _, entry in raw_content["items"].items():
                yield cls.parse_obj(entry)
        else:
            raise ValueError(f"{directory} or {filepath} not found.")

    return reader
