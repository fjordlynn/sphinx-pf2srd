from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Callable, Iterable, Type, TypeVar

    from pf2.dto import Entity

    T = TypeVar("T", bound=Entity)


def test_load_all(
    entities_reader: Callable[[Type[T]], Iterable[T]], all_kinds: list[Type[T]]
):
    all_the_things = {
        cls.__collection__: {entity.name: entity for entity in entities_reader(cls)}
        for cls in all_kinds
    }

    assert all_the_things


def test_unique_names(all_kinds, entities_reader: Callable[[Type[T]], Iterable[T]]):
    for kind in all_kinds:
        _check_names(entities_reader, kind)


def _check_names(entities_reader, cls):
    entity_names = set()
    for entity in entities_reader(cls):
        assert entity.name

        assert entity.names not in entity_names
        entity_names.add(entity.name)
