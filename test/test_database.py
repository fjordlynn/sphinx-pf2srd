from pf2.dto import EntityNotFound, ReferenceToEntity


def test_load(all_kinds, database, yaml):
    not_found = []

    all_entities = []

    for kind in all_kinds:
        entities = list(database.get_all(kind))
        assert entities

        all_entities.extend(entities)

    for entity in all_entities:
        reference_fields = {}
        for name, field in entity.__fields__.items():
            try:
                if issubclass(field.type_, ReferenceToEntity):
                    reference_fields[name] = field
            except TypeError:
                continue
        for field_name in reference_fields:
            fields = getattr(entity, field_name)
            if fields is None:
                continue
            if not isinstance(fields, list):
                fields = [fields]

            for field in fields:
                if field is None:
                    continue
                try:
                    _ = field.resolve(database)
                except EntityNotFound as e:
                    not_found.append(
                        ((entity.kind, entity.name), (e.kind.__collection__, e.name))
                    )

    assert not not_found
