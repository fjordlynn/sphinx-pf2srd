from __future__ import annotations

import shutil
from collections import defaultdict
from datetime import datetime
from logging import getLogger
from pathlib import Path
from typing import (Any, Callable, Iterable, Literal, Sequence, Type, TypeVar,
                    overload)

from pf2._compat.yaml import (JsonLike, Yaml, make_yaml, make_yaml_dump,
                              make_yaml_load)
from pf2.database import (Kind, ReferenceToRule, Rule, RuleId, RuleNotFound,
                          SourceFile, log)
from pf2.database.model import (AbilityScore, ActionCategory, ActionCost,
                                Activity, Alignment, Ammunition, Ancestry,
                                Armor, ArmorCategory, ArmorGroup, Background,
                                BonusType, Bulk, Class, Condition,
                                DamageCategory, DamageType, Effect, Feat,
                                Frequency, Gear, Heritage, ItemCategory,
                                Language, LanguageRarity, MovementType,
                                Publisher, SavingThrow, Sense, Size, Skill,
                                Source, Spell, SpellComponent, SpellSchool,
                                SpellTradition, SpellType, Staff, Trait,
                                TraitType, Weapon, WeaponCategory, WeaponGroup)

E = TypeVar("E", bound="Entity")

log = getLogger(__name__)


class Database:
    map = {
        "publisher": Publisher,
        "source": Source,
        "traittype": TraitType,
        "trait": Trait,
        "alignment": Alignment,
        "abilityscore": AbilityScore,
        "actioncategory": ActionCategory,
        "actioncost": ActionCost,
        "effect": Effect,
        "bulk": Bulk,
        "itemcategory": ItemCategory,
        "armorcategory": ArmorCategory,
        "armorgroup": ArmorGroup,
        "weapongroup": WeaponGroup,
        "weaponcategory": WeaponCategory,
        "damagecategory": DamageCategory,
        "damagetype": DamageType,
        "frequency": Frequency,
        "activity": Activity,
        "ammunition": Ammunition,
        "gear": Gear,
        "armor": Armor,
        "weapon": Weapon,
        "size": Size,
        "languagerarity": LanguageRarity,
        "language": Language,
        "sense": Sense,
        "skill": Skill,
        "condition": Condition,
        "movementtype": MovementType,
        "bonustype": BonusType,
        "feat": Feat,
        "ancestry": Ancestry,
        "heritage": Heritage,
        "background": Background,
        "spellschool": SpellSchool,
        "spellcomponent": SpellComponent,
        "spelltradition": SpellTradition,
        "spelltype": SpellType,
        "savingthrow": SavingThrow,
        "spell": Spell,
        "class": Class,
        "staff": Staff,
        "sourcefile": SourceFile,
        "rule": Rule,
    }

    @overload
    def get_model_cls(self, kind: Literal["publisher"]) -> Publisher:
        ...

    @overload
    def get_model_cls(self, kind: Literal["source"]) -> Source:
        ...

    @overload
    def get_model_cls(self, kind: Literal["traittype"]) -> TraitType:
        ...

    @overload
    def get_model_cls(self, kind: Literal["trait"]) -> Trait:
        ...

    @overload
    def get_model_cls(self, kind: Literal["alignment"]) -> Alignment:
        ...

    @overload
    def get_model_cls(self, kind: Literal["abilityscore"]) -> AbilityScore:
        ...

    @overload
    def get_model_cls(self, kind: Literal["actioncategory"]) -> ActionCategory:
        ...

    @overload
    def get_model_cls(self, kind: Literal["actioncost"]) -> ActionCost:
        ...

    @overload
    def get_model_cls(self, kind: Literal["effect"]) -> Effect:
        ...

    @overload
    def get_model_cls(self, kind: Literal["bulk"]) -> Bulk:
        ...

    @overload
    def get_model_cls(self, kind: Literal["itemcategory"]) -> ItemCategory:
        ...

    @overload
    def get_model_cls(self, kind: Literal["armorcategory"]) -> ArmorCategory:
        ...

    @overload
    def get_model_cls(self, kind: Literal["armorgroup"]) -> ArmorGroup:
        ...

    @overload
    def get_model_cls(self, kind: Literal["weapongroup"]) -> WeaponGroup:
        ...

    @overload
    def get_model_cls(self, kind: Literal["weaponcategory"]) -> WeaponCategory:
        ...

    @overload
    def get_model_cls(self, kind: Literal["damagecategory"]) -> DamageCategory:
        ...

    @overload
    def get_model_cls(self, kind: Literal["damagetype"]) -> DamageType:
        ...

    @overload
    def get_model_cls(self, kind: Literal["frequency"]) -> Frequency:
        ...

    @overload
    def get_model_cls(self, kind: Literal["activity"]) -> Activity:
        ...

    @overload
    def get_model_cls(self, kind: Literal["ammunition"]) -> Ammunition:
        ...

    @overload
    def get_model_cls(self, kind: Literal["gear"]) -> Gear:
        ...

    @overload
    def get_model_cls(self, kind: Literal["armor"]) -> Armor:
        ...

    @overload
    def get_model_cls(self, kind: Literal["weapon"]) -> Weapon:
        ...

    @overload
    def get_model_cls(self, kind: Literal["size"]) -> Size:
        ...

    @overload
    def get_model_cls(self, kind: Literal["languagerarity"]) -> LanguageRarity:
        ...

    @overload
    def get_model_cls(self, kind: Literal["language"]) -> Language:
        ...

    @overload
    def get_model_cls(self, kind: Literal["sense"]) -> Sense:
        ...

    @overload
    def get_model_cls(self, kind: Literal["skill"]) -> Skill:
        ...

    @overload
    def get_model_cls(self, kind: Literal["condition"]) -> Condition:
        ...

    @overload
    def get_model_cls(self, kind: Literal["movementtype"]) -> MovementType:
        ...

    @overload
    def get_model_cls(self, kind: Literal["bonustype"]) -> BonusType:
        ...

    @overload
    def get_model_cls(self, kind: Literal["feat"]) -> Feat:
        ...

    @overload
    def get_model_cls(self, kind: Literal["ancestry"]) -> Ancestry:
        ...

    @overload
    def get_model_cls(self, kind: Literal["heritage"]) -> Heritage:
        ...

    @overload
    def get_model_cls(self, kind: Literal["background"]) -> Background:
        ...

    @overload
    def get_model_cls(self, kind: Literal["spellschool"]) -> SpellSchool:
        ...

    @overload
    def get_model_cls(self, kind: Literal["spellcomponent"]) -> SpellComponent:
        ...

    @overload
    def get_model_cls(self, kind: Literal["spelltradition"]) -> SpellTradition:
        ...

    @overload
    def get_model_cls(self, kind: Literal["spelltype"]) -> SpellType:
        ...

    @overload
    def get_model_cls(self, kind: Literal["savingthrow"]) -> SavingThrow:
        ...

    @overload
    def get_model_cls(self, kind: Literal["spell"]) -> Spell:
        ...

    @overload
    def get_model_cls(self, kind: Literal["class"]) -> Class:
        ...

    @overload
    def get_model_cls(self, kind: Literal["staff"]) -> Staff:
        ...

    @overload
    def get_model_cls(self, kind: Literal["sourcefile"]) -> SourceFile:
        ...

    @overload
    def get_model_cls(self, kind: Literal["rule"]) -> Rule:
        ...

    def get_model_cls(self, kind):
        return self.map[kind]

    def __init__(
        self,
        root_path: Path | str | Sequence[Path | str],
        yaml_load: str | Callable[[Path], JsonLike] | None,
        yaml_dumps: str | Callable[[JsonLike], str] | None,
    ):
        log.debug(f"Database initialisation")
        paths = [root_path] if isinstance(root_path, (Path, str)) else root_path
        self.root_paths = [Path(path) for path in paths]
        self.yaml_load = yaml_load or make_yaml_load().load
        self.yaml_dumps = yaml_dumps or make_yaml_dump().dumps

        self._files: dict[Path, Any] = {}
        self._cache: dict[RuleId, Rule] = {}
        self._cls_index: dict[Type[E], list[RuleId]] = defaultdict(list)
        self._names_index: dict[RuleId, RuleId] = {}
        self._sourcename_index: dict[RuleId, tuple[Path, int, int]] = {}

        for filepath in self.root_paths:
            self.load_from_file(filepath)

    def load_from_file(self, filepath: Path) -> None:
        log.debug(f"loading: {filepath}")
        raw_data = self.yaml_load(filepath)

        sourcefile = SourceFile.parse_obj(raw_data)
        self._register_one(sourcefile)

        # Some rules are hidden within others and are implicitely
        # registered under _instances. Ouch, FIXME
        for cls, rules in Rule._instances.items():
            for rule in rules.values():
                self._register_one(rule)

        self.root_paths.append(filepath)

    def _register_one(self, rule: Rule) -> None:
        rule_primary_id = RuleId(str(rule.kind), str(rule.name))

        if rule_primary_id in self._cache:
            return

        # Main index for rules, primary name
        self._cache[rule_primary_id] = rule

        # Index under all parent classes
        for cls in [cls for cls in rule.__class__.__mro__ if issubclass(cls, Rule)]:
            self._cls_index[cls].append(rule_primary_id)

        # Index primary and additional names
        self._names_index[rule_primary_id] = rule_primary_id
        for alternate_name in rule.names:
            rule_atlernate_id = RuleId(str(rule_primary_id.kind), str(alternate_name))
            self._names_index[rule_atlernate_id] = rule_primary_id

    def find_all(self, kind: Type[E]):
        yield from self._cls_index[kind]

    def find_one(self, kind: Type[E], name: str) -> Rule:
        rule_id = self._names_index[RuleId(str(kind.get_kind()), str(name))]
        return self._cache[rule_id]

    def get_sourcename(self, kind: Type[E], name: str) -> tuple[Path, int, int]:
        return self._sourcename_index[
            self._names_index[RuleId(str(kind.get_kind()), str(name))]
        ]

    def check(self) -> Report:
        return Report.parse_obj(
            {
                "name": "report",
                "kind": "report",
                "files": self.root_paths,
                "items": {rule.name: rule for rule in self._cache.values()},
            }
        )


class Report(Rule):
    files: tuple[Path, ...]


def check(kinds: list[Type[Rule]]):
    log.debug("Running database checks.")
    not_found = []

    all_entities = []

    log.debug("Loading entities...")
    for kind in kinds:
        log.debug("  Loading %s", self.get_kind(kind))
        entities = list(self.get_all(kind))
        log.debug("    %s %s items", len(entities), self.get_kind(kind))
        all_entities.extend(entities)
    log.debug("Loaded %s entities.", len(all_entities))
    log.debug("Loading entities: Done.")

    log.debug("Checking references...")
    all_references = []
    for entity in all_entities:
        log.debug("  Checking references from '%s.%s'", entity.kind, entity.name)
        reference_fields = {}
        for name, field in entity.__fields__.items():
            try:
                if issubclass(field.type_, ReferenceToRule):
                    reference_fields[name] = field
            except TypeError:
                continue
        log.debug("    %s references found", len(reference_fields))
        for field_name in reference_fields:
            log.debug("      Field '%s:%s.%s'", entity.kind, entity.name, field_name)
            fields = getattr(entity, field_name)
            if fields is None:
                continue
            if not isinstance(fields, list):
                fields = [fields]

            for field in fields:
                if field is None:
                    continue
                try:
                    log.debug(
                        "      Resolving references from '%s:%s.%s' to '%s:%s'",
                        entity.kind,
                        entity.name,
                        field_name,
                        field.kind,
                        field.key(),
                    )
                    all_references.append(
                        (entity.kind, entity.name, field_name, field.kind, field.key())
                    )
                    _ = field.resolve(self)

                    log.debug(
                        "      Resolved",
                    )
                except RuleNotFound as e:
                    log.warning(
                        "  Unresolved reference from '%s:%s.%s' to '%s:%s'",
                        entity.kind,
                        entity.name,
                        field_name,
                        field.kind,
                        field.key(),
                    )
                    not_found.append(
                        ((entity.kind, entity.name), (e.kind.__collection__, e.name))
                    )
    log.debug("Checked %s references.", len(all_references))
    log.debug("%s references were not found.", len(not_found))
    log.debug("Checking references: Done.")

    return not_found
