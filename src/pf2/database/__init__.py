from __future__ import annotations

import logging
from collections import defaultdict
from pathlib import Path
from typing import (Annotated, Any, ClassVar, Dict, Generic, List, Literal,
                    NamedTuple, Optional, Self, Set, Type, TypeVar, Union)

from pydantic import BaseModel as _BaseModel
from pydantic import Field, root_validator
from pydantic.class_validators import Validator
from pydantic.fields import ModelField, Undefined
from pydantic.utils import lenient_issubclass

from pf2._compat.yaml import Yaml, make_yaml

log = logging.getLogger(__name__)


class BaseModel(_BaseModel):
    ...


class SphinxPfrsrdError(RuntimeError):
    ...


class RuleNotFound(SphinxPfrsrdError):
    def __init__(self, cls, name):
        if isinstance(cls, type):
            self.kind_name = __name__
        else:
            self.kind_name = cls
        self.name = name
        super().__init__(
            f"The '{self.kind_name}' entity could not be found with name '{name}'."
        )


def _set_field_on_model(
    model: type[BaseModel],
    name: str,
    annotation: Any,
    value: Any = Undefined,
) -> None:
    """
    Inspired heavily by what is done inside `ModelMetaclass.__new__`.
    May be incomplete!
    """
    model.__fields__[name] = ModelField.infer(
        name=name,
        value=value,
        annotation=annotation,
        class_validators=None,
        config=model.__config__,
    )
    model.__annotations__[name] = annotation


class Rule(BaseModel):
    class Config:
        arbitrary_types_allowed = True

    kind: Literal["rule"] = "rule"

    name: Annotated[
        str,
        Field(
            ...,
            description=(
                "The primary name for this entity, used as a primary key. "
                "It must be unique accros entities of the same kind."
            ),
        ),
    ] = ""  # Should not have a default value, see https://github.com/pydantic/pydantic/issues/3955

    description: Annotated[
        str,
        Field(
            description=("A human-readable description for this entity."),
        ),
    ] = ""

    flavor_text: Annotated[
        Optional[str],
        Field(
            description=(
                "Flavor text is any text in a game that is completely "
                "unrelated to actual rules or gameplay, and is included "
                "merely for effect."
            ),
        ),
    ] = None

    names: Annotated[
        Set[str],
        Field(
            default_factory=set,
            description=(
                "Other names for this entity. "
                "Within a collection, all names must be unique."
            ),
        ),
    ]

    has_been_manually_proofread: Annotated[
        bool,
        Field(description="Whether this entity has been manually proof read."),
    ] = False

    sourcefile: Annotated[Optional[Path], Field(description="", exclude=True)] = None
    line_start: Annotated[Optional[int], Field(description="", exclude=True)] = None
    line_end: Annotated[Optional[int], Field(description="", exclude=True)] = None

    items: dict[str, Rule]

    _kind_to_cls: ClassVar[dict[str, Type[R]]] = {}
    _cls_to_kind: ClassVar[dict[Type[R], str]] = {}

    _instances: ClassVar[dict[Type[Self], dict[str, Self]]] = defaultdict(dict)

    def rst_role(self, title="", domain="pf2srd"):
        if title:
            f":{domain}:{self.kind}:`{title} <{self.name.strip()}>`"
        else:
            return f":{domain}:{self.kind}:`{self.name.strip()}`"

    def __init__(self, **data: Any):
        # Change in Pydantic v2
        super().__init__(**data)
        self.__class__._instances[self.__class__][self.name] = self

    @classmethod
    def get_kind(cls) -> str:
        return cls._cls_to_kind[cls]

    def __init_subclass__(cls, /, __kind__: str = "", **kwargs):
        super().__init_subclass__(**kwargs)
        Rule._kind_to_cls["rule"] = Rule
        Rule._cls_to_kind[Rule] = "rule"
        if not cls.__name__.startswith("_"):
            __kind__ = str(__kind__ or cls.__name__.lower())

            # Register a new kind
            Rule._kind_to_cls[__kind__] = cls
            Rule._cls_to_kind[cls] = __kind__
            # Force the "kind" field to be set to the correct Literal kind
            # This is used as a discriminator
            _set_field_on_model(cls, "kind", Literal[__kind__])

            # All models have an "entities" field that is a discriminated union
            # and this type annotation needs to be have all the classes
            # in the union.

            if len(Rule._kind_to_cls) > 1:
                # Create the updated type annotation for the field
                sub_models_annotation = dict[  # type: ignore[index, misc, valid-type]
                    str,
                    Annotated[
                        Union[tuple(Rule._kind_to_cls.values())],
                        Field(discriminator="kind"),
                    ],
                ]

                # Set the new annotated field on all model classes
                for model in {Rule} | set(Rule._kind_to_cls.values()):
                    _set_field_on_model(
                        model,
                        "items",
                        sub_models_annotation,
                        Field(default_factory=dict),
                    )

        # Kinds can have fields that are references to entities.
        # Most reference are basically strings, but some are
        # pydantic models with multiple properties.
        # Without using a Root model with pydantic, we can simply make
        # sure the right factory is used:
        # constructor with strings,
        # BaseModel.parse_obj() with models.

        def _make_reference_coercer(factory):
            # We will create pre-validators that use a specific
            # factory function
            def _cast_reference(value, values, config, field):
                if value is None:
                    return None
                elif field.sub_fields is None:
                    return factory(value)
                else:
                    return [factory(subvalue) for subvalue in value]

            return _cast_reference

        for name, f in cls.__fields__.items():
            # Look for reference fields
            if not lenient_issubclass(f.type_, ReferenceToRule):
                continue

            # Select the right factory for complex or string references
            if lenient_issubclass(f.type_, ComplexReferenceToRule):
                factory = f.type_.parse_obj
            else:
                factory = f.type_

            # Use a pre-validator to coerce the raw value through the
            # selected factory.
            f.class_validators.update(
                {
                    f"_coerce_reference_for_{name}": Validator(
                        _make_reference_coercer(factory), pre=True
                    )
                }
            )
            f.prepare()

    _additional_name_fields: ClassVar[tuple[str, ...]] = (
        "name",
        "short_name",
        "abbreviation",
        "long_name",
    )

    @root_validator(pre=True, allow_reuse=True)
    def _add_alternate_names(cls, values):
        """
        Add alternate names from fields commonly used as names.
        """
        values.setdefault(
            "names",
            [
                values.get(key, None)
                for key in cls._additional_name_fields
                if values.get(key, None)
            ],
        )
        return values

    @root_validator(pre=True, allow_reuse=True)
    def _add_sourcefile(cls, values):
        """
        Add alternate names from fields commonly used as names.
        """
        values.setdefault("sourcefile", values.get("__sourcefile__", None))
        values.setdefault("line_start", values.get("__line_start__", None))
        values.setdefault("line_end", values.get("__line_end__", None))

        if "items" in values:
            if "__sourcefile__" in values["items"]:
                del values["items"]["__sourcefile__"]
            if "__line_start__" in values["items"]:
                del values["items"]["__line_start__"]
            if "__line_end__" in values["items"]:
                del values["items"]["__line_end__"]

        return values

    def as_dict(
        self,
        *,
        by_alias: bool = False,
        exclude_unset: bool = False,
        exclude_defaults: bool = False,
        exclude_none: bool = False,
        models_as_dict: bool = True,
    ):
        """
        Provide a serializable dict representation of this.

        Note:
            This need to change in Pydantic v2.
        """
        data = dict(
            self._iter(
                to_dict=models_as_dict,
                by_alias=by_alias,
                exclude_unset=exclude_unset,
                exclude_defaults=exclude_defaults,
                exclude_none=exclude_none,
            )
        )
        return data

    def yaml(
        self,
        *,
        yaml: Yaml | None = None,
        by_alias: bool = False,
        exclude_unset: bool = False,
        exclude_defaults: bool = False,
        exclude_none: bool = False,
        models_as_dict: bool = True,
        **dumps_kwargs: Any,
    ) -> str:
        """
        Serialize as YAML.

        Notes:
            This needs to change with Pydantic v2.
        """
        yaml = yaml or make_yaml()
        return yaml.dumps(
            self.as_dict(
                by_alias=by_alias,
                exclude_unset=exclude_unset,
                exclude_defaults=exclude_defaults,
                exclude_none=exclude_none,
                models_as_dict=models_as_dict,
            ),
            **dumps_kwargs,
        )


class RuleId(NamedTuple):
    kind: str
    name: str


R = TypeVar("R", bound=Rule)


class ReferenceToRule(Generic[R]):
    """
    Represent a reference to a rule.

    Some references are simply a pointer to an existing rule, other may
    involve some additional information. Different type of references are
    implemented in subclasses.
    """

    target_rule_cls: Type[R]
    """
    The rule class this reference class will refer to.

    This is a class attribute that is set at the class creation, using
    the `target` class argument and must match the generic type for annotations
    to work properly in type-checking.
    """

    @property
    def key(self) -> str:
        """
        The identifier string for the referred rule.
        """
        raise NotImplementedError

    def rst_role(self, title="", domain="pf2srd") -> str:
        """
        Provide the Sphinx role that this reference represents.

        Notes:
            This class should provide an API that enable the generation
            of roles instead of implementing here. The responsability is
            not properly separated.
        """
        if title:
            f":{domain}:{self.target_rule_cls.get_kind()}:`{title} <{self.key}>`"
        else:
            return f":{domain}:{self.target_rule_cls.get_kind()}:`{self.key}`"

    def __init_subclass__(cls, /, target: Type[R] | None = None, **kwargs):
        super().__init_subclass__(**kwargs)
        if not target:
            raise ValueError("Reference needs a target class")
        cls.target_rule_cls = target


class ScalarReferenceToRule(ReferenceToRule, Generic[R], str, target=Rule):
    """
    A scalar (string) reference to a rule.

    For instance for a reference to a source rulebook, the key could be
    the book's title.
    """

    @property
    def key(self) -> str:
        return str(self)

    @classmethod
    def __get_validators__(cls):
        return tuple()


class ComplexReferenceToRule(ReferenceToRule, Generic[R], BaseModel, target=Rule):
    """
    A complex reference to a rule.
    """


class SourceFile(Rule):
    ...

    @root_validator(pre=True, allow_reuse=True)
    def _name_from_sourcefile(cls, values):
        """
        Add alternate names from fields commonly used as names.
        """
        values.setdefault("name", values.get("__sourcefile__", None))
        return values

    _additional_name_fields: ClassVar[tuple[str, ...]] = ("__sourcefile__",)


class Kind(Rule):
    """ """
