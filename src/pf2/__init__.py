from __future__ import annotations

from typing import Any, Optional, Tuple, TypeVar

from sphinx.util.logging import getLogger

log = getLogger(__name__)

T = TypeVar("T", contravariant=True)


class Options(dict):
    """
    A dict/attribute hybrid that returns None on nonexisting keys.
    """

    def copy(self) -> Options:
        return Options(super().copy())

    def __getattr__(self, name: str) -> Any:
        try:
            return self[name.replace("_", "-")]
        except KeyError:
            return None
