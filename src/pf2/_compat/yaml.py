from __future__ import annotations

from io import StringIO
from pathlib import Path
from typing import Any, Dict, Protocol, Sequence, Union, overload

import yaml as pyyaml


class JsonArray(Protocol):
    def __getitem__(self, idx: int) -> "JsonLike":
        ...

    # hack to enforce an actual list
    def sort(self) -> None:
        ...


class JsonDict(Protocol):
    def __getitem__(self, key: str) -> "JsonLike":
        ...

    # hack to enforce an actual dict
    @staticmethod
    @overload
    def fromkeys(seq: Sequence[Any]) -> Dict[Any, Any]:
        ...

    @staticmethod
    @overload
    def fromkeys(seq: Sequence[Any], value: Any) -> Dict[Any, Any]:
        ...


JsonLike = Union[str, int, float, bool, None, JsonArray, JsonDict]


class Yaml(Protocol):
    def load(self, stream: Union[Path, Any]) -> JsonLike:
        ...

    def dumps(self, data: Any) -> str:
        ...

    def _make_scalar_node(self, tag, value, start_mark=None, end_mark=None, style=None):
        ...


def make_yaml(implementation: str | Yaml = "pyyaml") -> Yaml:
    if not isinstance(implementation, str):
        return implementation
    choices = {"pyyaml": _PyYaml, "ruamel": _RuamelYaml}
    return choices[implementation]()


def make_yaml_dump(implementation: str = "ruamel") -> Yaml:
    return make_yaml(implementation)


def make_yaml_load(implementation: str = "pyyaml") -> Yaml:
    return make_yaml(implementation)


def _should_use_block(value):
    as_str = str(value)
    for c in "\u000a\u000d\u001c\u001d\u001e\u0085\u2028\u2029":
        if c in as_str:
            return True
    if len(as_str) > 55:
        return True
    if len(as_str.splitlines()) > 1:
        return True
    return False


def _make_str_representer(yaml_impl: Yaml):
    def represent_str(dumper, value):
        if _should_use_block(value):
            style = ">"
        else:
            style = dumper.default_style

        node = yaml_impl._make_scalar_node(
            "tag:yaml.org,2002:str", str(value), style=style
        )
        if dumper.alias_key is not None:
            dumper.represented_objects[dumper.alias_key] = node
        return node

    return represent_str


def represent_set_as_list(dumper, data):
    return dumper.represent_sequence("tag:yaml.org,2002:seq", list(data))


class _RuamelYaml(Yaml):
    def __init__(self) -> None:
        from ruamel.yaml import YAML

        # Only use primitives (safe)
        # The C implementation may misbehave with indentation,
        # use pure python (pure=True) if needed.
        self._impl = YAML(pure=False)
        self._impl.Representer.ignore_aliases = lambda x, y: True
        # Preserve order, otherwise it is sort()ed
        self._impl.sort_base_mapping_type_on_output = False  # type: ignore
        # Indent 2 character, with sequences indented also
        self._impl.indent(mapping=2, sequence=4, offset=2)
        # Try to preserve original quotes
        self._impl.preserve_quotes = False
        # self._impl.width = 70
        # Prevent inline mappings and sequences with JSON-like representation
        self._impl.default_flow_style = False

        self._impl.representer.add_representer(str, _make_str_representer(self))
        self._impl.representer.add_representer(None, _make_str_representer(self))
        self._impl.representer.add_representer(set, represent_set_as_list)

    def _make_scalar_node(self, tag, value, start_mark=None, end_mark=None, style=None):
        from ruamel.yaml import ScalarNode

        return ScalarNode(
            tag, value, start_mark=start_mark, end_mark=end_mark, style=style
        )

    def dumps(self, data: Any) -> str:
        stream = StringIO()
        self._impl.dump(data, stream=stream)
        return stream.getvalue()

    def load(self, stream: Union[Path, Any]) -> JsonLike:
        """
        A safe YAML loader.
        """
        return self._impl.load(stream)  # type: ignore


class _PyYaml(Yaml):
    class MyDumper(pyyaml.SafeDumper):
        def increase_indent(self, flow=False, indentless=False):
            return super().increase_indent(flow, False)

        def ignore_aliases(self, data):
            return True

    class MyLoader(pyyaml.SafeLoader):
        def construct_mapping(self, node, deep=False):
            mapping = super().construct_mapping(node, deep=deep)
            # Add 1 so line numbering starts at 1
            mapping["__line_start__"] = node.start_mark.line
            mapping["__line_end__"] = node.end_mark.line
            mapping["__sourcefile__"] = self.name
            return mapping

    def __init__(self):
        import yaml

        self._impl = yaml
        self._dumper = self.MyDumper
        self._loader = self.MyLoader

        self._dumper.add_representer(set, represent_set_as_list)
        self._dumper.add_representer(str, _make_str_representer(self))
        self._dumper.add_multi_representer(str, _make_str_representer(self))
        self._dumper.add_representer(None, _make_str_representer(self))
        self._dumper.add_multi_representer(None, _make_str_representer(self))

    def _make_scalar_node(self, tag, value, start_mark=None, end_mark=None, style=None):
        return self._impl.representer.ScalarNode(
            tag,
            value,
            start_mark=start_mark,
            end_mark=end_mark,
            style=style,
        )

    def dumps(self, data: Any) -> str:
        return self._impl.dump(
            data,
            Dumper=self._dumper,
            allow_unicode=True,
            default_flow_style=False,
            indent=2,
            # width=72,
        )

    def load(self, stream: Path) -> JsonLike:
        """
        A safe YAML loader.
        """
        with stream.open("r+t", encoding="utf-8") as opened:
            return self._impl.load(opened, Loader=self._loader)  # type: ignore
