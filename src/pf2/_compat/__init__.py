from __future__ import annotations

import re


def snake_case(string: str) -> str:
    """
    Convert a string to ``snake_case``

    .. warning:: The algorithm used here is destructive.  That means
        that the reconstruction of specific samples to, for instance,
        ``camelCase``, would be impossible or at least very difficult
        and involving a database of known acronyms or the like.

    Examples:

        >>> snake_case("aA")
        'a_a'
        >>> snake_case("aAa")
        'a_aa'
        >>> snake_case("snake2_snake")
        'snake_2_snake'
        >>> snake_case("get200HTTPResponseCode")
        'get_200_http_response_code'
        >>> snake_case("10CoolDudes")
        '10_cool_dudes'
        >>> snake_case("Camel2WARNING_Case_CASE")
        'camel_2_warning_case_case'

    Args:
        string: Some string in any case.

    Returns:
        A nice and neat camel case string.
    """
    pattern = re.compile(
        r"[A-Z]?[a-z]+|[A-Z]{2,}(?=[A-Z][a-z]|\d|\W|$)|\d+|[A-Z]{2,}|[A-Z]$"
    )
    words = pattern.findall(string)
    return "_".join(map(str.lower, words))
