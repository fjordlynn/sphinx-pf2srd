"""
######################
Command line interface
######################

This module provides argument parsing, shell completion and other CLI
goodies.
"""

from __future__ import annotations

import logging.config
from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Sequence, Tuple

import click
from click import ParamType
from click.shell_completion import CompletionItem

log = logging.getLogger()


PROJECT = Path(__file__).parent.parent.parent


class App:
    def __init__(self):
        self.clean_path = Path(
            "/home/cblegare/git/gitlab.com/fjordlynn/pathfinder-2-sqlite/clean"
        )
        self.out_path = Path(PROJECT, "build")
        self.db = Database(self.clean_path)

    @property
    def collections(self):
        return [kind.__collection__ for kind in self.db.kinds]

    def collection_completion(
        self, ctx: click.Context, args: List[str], incomplete: str
    ) -> List[Tuple[str, str]]:
        suggestions = [
            # workaround bug httqps://github.com/pallets/click/issues/1812
            (
                kind.__collection__,
                help_from_object(kind, default="Undocumented template"),
            )
            for kind in self.collections
            if incomplete in kind.__collection__
        ]
        return suggestions


class CollectionType(ParamType):
    name = "collection"

    def __init__(self, app: App):
        self._app = app

    def shell_complete(self, ctx, param, incomplete):
        return [
            CompletionItem(
                kind.__collection__,
                help=help_from_object(kind, default="Undocumented template"),
            )
            for kind in self._app.collections
            if incomplete in kind.__collection__
        ]


@click.group(invoke_without_command=True)
@click.version_option(version="0.1")
@click.option(
    "-v",
    "--verbose",
    count=True,
    help="Increase log level. '-v' will output INFO messages. "
    "More than -vv is useless.",
)
@click.option(
    "-q",
    "--quiet",
    count=True,
    help="Reduce log level. '-q' will suppress WARNING messages. "
    "More than -qq is useless.",
)
@click.option(
    "-l",
    "--log-file",
    help="Reduce log level. '-q' will suppress WARNING messages. "
    "More than -qq is useless.",
)
@click.pass_context
def main(ctx: click.Context, verbose: int, quiet: int, log_file: str) -> None:
    """
    outil, the One UTIL package to rule them all.
    """
    verbosity: int = int(logging.INFO / 10) + verbose - quiet
    logging.config.dictConfig(
        logging_configuration(verbosity, Path(log_file) if log_file else None)
    )

    if ctx.invoked_subcommand is None:
        click.echo("not really")


@main.command()
@click.argument("directory", metavar="DIRECTORY", nargs=1)
def labz(directory):
    yaml = make_yaml()
    for sub in Path(directory).resolve().iterdir():
        for entry_name, entry in read_entries(sub, yaml):
            print(entry_name)


@main.command()
@click.argument("directory", metavar="DIRECTORY", nargs=1)
def checkdb(directory):
    from pf2._compat.yaml import Yaml, make_yaml
    from pf2.database.database import Database

    yaml_load = make_yaml()
    yaml_dumps = make_yaml()

    directory = Path(directory)

    database = Database([], yaml_load=yaml_load.load, yaml_dumps=yaml_dumps.dumps)

    for filepath in directory.rglob("*.yaml"):
        if (
            filepath.name
            in (
                "activity.yaml",
                "feat.yaml",
                "spell.yaml",
            )
            or "monster" in filepath.parts
        ):
            continue
        database.load_from_file(filepath)

    report = database.check()
    print(report.yaml())


@main.command()
@click.argument("directory", metavar="DIRECTORY", nargs=1)
def rewrite(directory):
    all_kinds()
    yaml = make_yaml()
    database = Database(Path(directory), yaml)
    database.write("skill")


def read_entries(source: Path, yaml: Yaml) -> Iterable[tuple[str, Any]]:
    if source.is_dir():
        for source_file in source.glob("*.yaml"):
            raw_content = yaml.load(source_file)
            for entry_name, entry in raw_content["items"].items():
                yield entry_name, entry
    else:
        raw_content = yaml.load(source)
        for entry_name, entry in raw_content["items"].items():
            yield entry_name, entry


def read_monsters(source: Path, yaml: Yaml) -> Iterable[tuple[str, Any]]:
    if source.is_dir():
        for source_file in source.glob("*.yaml"):
            raw_content = yaml.load(source_file)
            for monster_name, monster_entry in raw_content["items"].items():
                yield monster_name, monster_entry
    else:
        raw_content = yaml.load(source)
        for monster_name, monster_entry in raw_content["items"].items():
            yield monster_name, monster_entry


@main.command()
@click.option("--structure", "-s", "structure", default="kind", show_default=True)
@click.option("--force", "-f", is_flag=True, default=False, show_default=True)
@click.argument(
    "collection",
    metavar="COLLECTION",
    # type=CollectionType(_app),
    nargs=-1,
)
@click.pass_obj
def export(obj: App, collection: Sequence[str], structure: str, force: bool):
    ...


def logging_configuration(
    verbosity: int, logfile: Optional[Path] = None
) -> Dict[str, Any]:
    """
    Build logging configuration based on a verbosity level.

    Args:
        verbosity: Verbosity 0 is means "CRITICAL", and each
            increments move toward "DEBUG".
    """
    level = max(logging.CRITICAL - logging.DEBUG * verbosity, logging.DEBUG)

    formatters = {
        "standard": {"format": "%(levelname)8s: %(message)s"},
        "debug": {"format": "%(levelname)8s %(name)-24s> %(message)s"},
    }

    active_formatter = "debug" if level < logging.DEBUG else "standard"

    handlers = {
        "console": {
            "level": level,
            "formatter": active_formatter,
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stderr",  # Default is stderr
        },
    }

    if logfile:
        handlers["file"] = {
            "level": level,
            "formatter": active_formatter,
            "class": "logging.FileHandler",
            "filename": str(logfile),
        }

    loggers = {
        "": {
            # root logger
            "handlers": handlers.keys(),
            "level": level,
            "propagate": True,
        }
    }

    return dict(
        version=1,
        disable_existing_loggers=False,
        formatters=formatters,
        handlers=handlers,
        loggers=loggers,
    )


def help_from_object(obj: Any, default=""):
    return docstring_summary(obj) or default


def docstring_summary(obj: Any) -> str:
    """
    Extract the docstring summary from any object.

    Args:
        obj: Any python object.
        default: Default value if not summary could be extracted.

    Returns:

    """
    doc = obj.__doc__ or ""
    for line in doc.splitlines():
        stripped = line.strip()
        if stripped:
            return stripped
    else:
        return ""


if __name__ == "__main__":
    main(prog_name="pf2srd")
