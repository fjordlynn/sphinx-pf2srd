from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pathlib import Path


class Identification:
    def __init__(
        self,
        kind: str,
        names: list[str],
    ) -> None:
        ...


class DataSource:
    def __init__(self, path: Path):
        ...
