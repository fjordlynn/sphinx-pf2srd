import sphinx_compendia
from sphinx.application import Sphinx


def setup(app: Sphinx) -> None:
    sphinx_compendia.make_compendium(
        "pf2srd",
        [sphinx_compendia.Constituent("skill", admissible_parent_objtypes=False)],
        display_name="Pathfinder 2e System Reference Document",
        index_localname="PFSRD Index",
        app=app,
    )

    app.add_autodocumenter()
