import textwrap
from pathlib import Path
from typing import Generic, Iterable, Iterator, Sequence, Type

from docutils.nodes import Node
from docutils.utils import assemble_option_dict
from sphinx.ext.autodoc import Options
from sphinx.ext.autodoc.directive import (DocumenterBridge, DummyOptionSpec,
                                          parse_generated_content)
from sphinx.util.docutils import SphinxDirective
from sphinx.util.logging import getLogger

from pf2._compat.yaml import make_yaml
from pf2.database import Rule, SourceFile
from pf2.database.database import Database
from pf2.database.model import (AbilityScore, ActionCategory, ActionCost,
                                Activity, Alignment, Ammunition, Ancestry,
                                Armor, ArmorCategory, ArmorGroup, Background,
                                BonusType, Bulk, Class, Condition,
                                DamageCategory, DamageType, Effect, Feat,
                                Frequency, Gear, Heritage, ItemCategory,
                                Language, LanguageRarity, MovementType,
                                Publisher, SavingThrow, Sense, Size, Skill,
                                Source, Spell, SpellComponent, SpellSchool,
                                SpellTradition, SpellType, Staff, Trait,
                                TraitType, Weapon, WeaponCategory, WeaponGroup)
from pf2.sphinx.compendia import (AutodocDirective, ConstituentDocumenter,
                                  DocumenterAdapter, ObjectIdentifier)

CONTENT_INDENT = "    "


from typing import TypeVar

O = TypeVar("O", bound=Rule)

log = getLogger(__name__)


class Pf2SrdDocumenterAdapter(DocumenterAdapter):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        directory = Path.cwd().joinpath("raw_data/data")

        yaml_load = make_yaml()
        yaml_dumps = make_yaml()
        self.database = Database(
            [], yaml_load=yaml_load.load, yaml_dumps=yaml_dumps.dumps
        )
        for filepath in directory.rglob("*.yaml"):
            if (
                filepath.name
                in (
                    "feat.yaml",
                    "spell.yaml",
                )
                or "monster" in filepath.parts
            ):
                continue
            self.database.load_from_file(filepath)

    def get_documenter_class(
        self, object_id: ObjectIdentifier
    ) -> ConstituentDocumenter:
        if not object_id.name:
            documenter = objtypes_to_documenter[f"kind{object_id.objtype}"]
        else:
            documenter = objtypes_to_documenter[object_id.objtype]

        return documenter

    def get_children(self, object_id: ObjectIdentifier) -> Iterable[ObjectIdentifier]:
        if not object_id.name:
            for obj in self.database.find_all(
                self.database.get_model_cls(object_id.objtype)
            ):
                # FIXME domain
                yield ObjectIdentifier(object_id.domain_name, obj.kind, obj.name)

        else:
            object = self.database.find_one(
                self.database.get_model_cls(object_id.objtype), object_id.name
            )
            for child in object.items.values():
                # FIXME domain
                yield ObjectIdentifier(object_id, child.kind, child.name)

    def get_sourcename(self, object_id: ObjectIdentifier) -> str:
        if object_id.name:
            cls = self.database.get_model_cls(object_id.objtype)
            object = self.database.find_one(cls, object_id.name)
            return str(object.sourcefile)
        else:
            return object_id.objtype

    def get_signatures(self, object_id: ObjectIdentifier) -> tuple[str, ...]:
        if object_id.name:
            obj = self.database.find_one(
                self.database.get_model_cls(object_id.objtype), object_id.name
            )

            return tuple(dict.fromkeys([obj.name, *obj.names]))
        else:
            return tuple([""])

    def get_object(self, object_id: ObjectIdentifier, cls: type[O] = object) -> O:
        return self.database.find_one(cls, object_id.name)


class AutodocSourceFile(AutodocDirective):
    option_spec = DummyOptionSpec()
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True

    documenter_adapter_class = Pf2SrdDocumenterAdapter

    def get_documented_object_identifier(self) -> ObjectIdentifier:
        return ObjectIdentifier(
            self.domain_name,
            self.directive_name[4:],
            str(Path(self.arguments[0]).resolve()),
        )


class AutodocRule(AutodocDirective):
    option_spec = DummyOptionSpec()
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True

    documenter_adapter_class = Pf2SrdDocumenterAdapter


class AutodocKind(AutodocDirective):
    option_spec = DummyOptionSpec()
    has_content = True
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True

    documenter_adapter_class = Pf2SrdDocumenterAdapter

    def get_documented_object_identifier(self) -> ObjectIdentifier:
        """
        Get the currently documented constituent.

        Returns:
            Enough to identify a single documented object.
        """
        return ObjectIdentifier(self.domain_name, self.arguments[0], "")


class Pf2srdDocumenter(Generic[O], ConstituentDocumenter):
    target_rule_cls: Type[O]

    def __init_subclass__(cls, /, target: Type[O] | None = None, **kwargs):
        super().__init_subclass__(**kwargs)
        if not target and not cls.__name__.startswith("_"):
            raise ValueError(f"Reference needs a target class in {cls.__name__}")
        cls.target_rule_cls = target

    def generate_table_of_children(self):
        for line in generate_table(
            self.get_child_table_headers(),
            sorted(
                self.get_row_from_child(child_identifier)
                for child_identifier in self.documenter_adapter.get_children(
                    self.documented_object_identifier
                )
            ),
            content_indent=self.content_indent,
        ):
            self.yield_line(line)

    def get_child_table_headers(self) -> Sequence[str]:
        return "Name", "Description", "Source"

    def get_row_from_child(self, child: ObjectIdentifier) -> Sequence[str]:
        rule: Rule = self.documenter_adapter.get_object(
            child, self.documenter_adapter.database.get_model_cls(child.objtype)
        )
        sources = getattr(rule, "sources", [])
        return (
            rule.rst_role(),
            rule.description,
            # textwrap.shorten(rule.description, width=50, placeholder="..."),
            ", ".join(source.rst_role() for source in sources),
        )

    def document_object(self, thing: ObjectIdentifier | Rule):
        if isinstance(thing, Rule):
            object_id = ObjectIdentifier(
                self.documented_object_identifier.domain_name, thing.kind, thing.name
            )
        else:
            object_id = thing

        documenter_cls = self.documenter_adapter.get_documenter_class(object_id)
        documenter = documenter_cls(
            self.bridge, object_id, self.documenter_adapter, self.indent
        )
        documenter.generate()


class _KindDocumenter(Generic[O], Pf2srdDocumenter[O]):
    """
    Document all rules of a kind.
    """

    def prepare_documentation_scope(self):
        pass

    def generate_content(self) -> None:
        self.generate_table_of_children()


class _RuleDocumenter(Generic[O], Pf2srdDocumenter[O]):
    """
    Document one rule of a kind.
    """

    def generate_content(self) -> None:
        self.generate_data_fields()
        self.generate_description()
        self.generate_table_of_children()

    def generate_description(self):
        rule = self.documenter_adapter.get_object(
            self.documented_object_identifier, self.target_rule_cls
        )

        if not rule.description:
            return ()

        for line_of_description in rule.description.splitlines():
            self.yield_line(line_of_description)

        for line in _generate_directive_footer(
            "description",
            content_indent=self.content_indent,
        ):
            self.yield_line(line)

    def generate_data_fields(self):
        data_fields = self.get_data_fields()
        for line in generate_datafields(
            tuple(data_fields.keys()),
            tuple(data_fields.values()),
            content_indent=self.content_indent,
        ):
            self.yield_line(line)

    def get_data_fields(self) -> dict[str, str]:
        return {}


class PublisherKindDocumenter(_KindDocumenter[Publisher], target=Publisher):
    objtype = directivetype = "kindpublisher"

    def get_child_table_headers(self) -> Sequence[str]:
        return "Name", "Description"

    def get_row_from_child(self, child: ObjectIdentifier) -> Sequence[str]:
        rule: Rule = self.documenter_adapter.get_object(
            child, self.documenter_adapter.database.get_model_cls(child.objtype)
        )
        return (
            rule.rst_role(),
            rule.description,
            # textwrap.shorten(rule.description, width=50, placeholder="...")
        )


class PublisherDocumenter(_RuleDocumenter[Publisher], target=Publisher):
    objtype = directivetype = "publisher"


class SourceKindDocumenter(_KindDocumenter[Source], target=Source):
    objtype = directivetype = "kindsource"

    def get_child_table_headers(self) -> Sequence[str]:
        return "Name", "Description"

    def get_row_from_child(self, child: ObjectIdentifier) -> Sequence[str]:
        rule: Rule = self.documenter_adapter.get_object(
            child, self.documenter_adapter.database.get_model_cls(child.objtype)
        )
        return (
            rule.rst_role(),
            rule.description,
            # textwrap.shorten(rule.description, width=50, placeholder="...")
        )


class SourceDocumenter(_RuleDocumenter[Source], target=Source):
    objtype = directivetype = "source"

    def generate_content(self) -> None:
        super().generate_content()

        rule = self.documenter_adapter.get_object(
            self.documented_object_identifier, Source
        )

        for line in generate_rubric(rule.copyright_block, "Copyright block"):
            self.yield_line(line)

    def get_data_fields(self) -> dict[str, str]:
        entity = self.documenter_adapter.get_object(
            self.documented_object_identifier, Source
        )

        return {
            "Title": entity.title,
            "Publisher": entity.publisher.rst_role(),
            "Release": entity.release_date,
            "PZO Code": entity.pzocode,
            "ISBN": entity.isbn if entity.isbn else "",
        }


class TraitTypeKindDocumenter(_KindDocumenter[TraitType], target=TraitType):
    objtype = directivetype = "kindtraittype"


class TraitTypeDocumenter(_RuleDocumenter[TraitType], target=TraitType):
    objtype = directivetype = "traittype"


class TraitKindDocumenter(_KindDocumenter[Trait], target=Trait):
    objtype = directivetype = "kindtrait"

    def get_child_table_headers(self) -> Sequence[str]:
        return "Type", "Name", "Description"

    def get_row_from_child(self, child: ObjectIdentifier) -> Sequence[str]:
        trait = self.documenter_adapter.get_object(child, Trait)
        return (
            trait.type,
            trait.rst_role(),
            trait.description,
            # textwrap.shorten(trait.description, width=50, placeholder="...")
        )


class TraitDocumenter(_RuleDocumenter[Trait], target=Trait):
    objtype = directivetype = "trait"


class AlignmentKindDocumenter(_KindDocumenter[Alignment], target=Alignment):
    objtype = directivetype = "kindalignment"


class AlignmentDocumenter(_RuleDocumenter[Alignment], target=Alignment):
    objtype = directivetype = "alignment"


class AbilityScoreKindDocumenter(_KindDocumenter[AbilityScore], target=AbilityScore):
    objtype = directivetype = "kindabilityscore"


class AbilityScoreDocumenter(_RuleDocumenter[AbilityScore], target=AbilityScore):
    objtype = directivetype = "abilityscore"


class ActionCategoryKindDocumenter(
    _KindDocumenter[ActionCategory], target=ActionCategory
):
    objtype = directivetype = "kindactioncategory"


class ActionCategoryDocumenter(_RuleDocumenter[ActionCategory], target=ActionCategory):
    objtype = directivetype = "actioncategory"


class ActionCostKindDocumenter(_KindDocumenter[ActionCost], target=ActionCost):
    objtype = directivetype = "kindactioncost"


class ActionCostDocumenter(_RuleDocumenter[ActionCost], target=ActionCost):
    objtype = directivetype = "actioncost"


class EffectKindDocumenter(_KindDocumenter[Effect], target=Effect):
    objtype = directivetype = "kindeffect"


class EffectDocumenter(_RuleDocumenter[Effect], target=Effect):
    objtype = directivetype = "effect"

    def get_data_fields(self) -> dict[str, str]:
        effect = self.documenter_adapter.get_object(
            self.documented_object_identifier, Effect
        )
        fields = {}

        if effect.range is not None:
            fields["Range"] = effect.range
        if effect.area is not None:
            fields["Area"] = effect.area
        if effect.target is not None:
            fields["Target"] = effect.target
        if effect.duration is not None:
            fields["Duration"] = effect.duration
        if effect.critical_success is not None:
            fields["Critical success"] = effect.critical_success
        if effect.success is not None:
            fields["Success"] = effect.success
        if effect.failure is not None:
            fields["Failure"] = effect.failure
        if effect.critical_failure is not None:
            fields["Critical failure"] = effect.critical_failure

        return fields


class BulkKindDocumenter(_KindDocumenter[Bulk], target=Bulk):
    objtype = directivetype = "kindbulk"


class BulkDocumenter(_RuleDocumenter[Bulk], target=Bulk):
    objtype = directivetype = "bulk"


class ItemCategoryKindDocumenter(_KindDocumenter[ItemCategory], target=ItemCategory):
    objtype = directivetype = "kinditemcategory"


class ItemCategoryDocumenter(_RuleDocumenter[ItemCategory], target=ItemCategory):
    objtype = directivetype = "itemcategory"


class ArmorCategoryKindDocumenter(_KindDocumenter[ArmorCategory], target=ArmorCategory):
    objtype = directivetype = "kindarmorcategory"


class ArmorCategoryDocumenter(_RuleDocumenter[ArmorCategory], target=ArmorCategory):
    objtype = directivetype = "armorcategory"


class ArmorGroupKindDocumenter(_KindDocumenter[ArmorGroup], target=ArmorGroup):
    objtype = directivetype = "kindarmorgroup"


class ArmorGroupDocumenter(_RuleDocumenter[ArmorGroup], target=ArmorGroup):
    objtype = directivetype = "armorgroup"


class WeaponGroupKindDocumenter(_KindDocumenter[WeaponGroup], target=WeaponGroup):
    objtype = directivetype = "kindweapongroup"


class WeaponGroupDocumenter(_RuleDocumenter[WeaponGroup], target=WeaponGroup):
    objtype = directivetype = "weapongroup"


class WeaponCategoryKindDocumenter(
    _KindDocumenter[WeaponCategory], target=WeaponCategory
):
    objtype = directivetype = "kindweaponcategory"


class WeaponCategoryDocumenter(_RuleDocumenter[WeaponCategory], target=WeaponCategory):
    objtype = directivetype = "weaponcategory"


class DamageCategoryKindDocumenter(
    _KindDocumenter[DamageCategory], target=DamageCategory
):
    objtype = directivetype = "kinddamagecategory"


class DamageCategoryDocumenter(_RuleDocumenter[DamageCategory], target=DamageCategory):
    objtype = directivetype = "damagecategory"


class DamageTypeKindDocumenter(_KindDocumenter[DamageType], target=DamageType):
    objtype = directivetype = "kinddamagetype"


class DamageTypeDocumenter(_RuleDocumenter[DamageType], target=DamageType):
    objtype = directivetype = "damagetype"


class FrequencyKindDocumenter(_KindDocumenter[Frequency], target=Frequency):
    objtype = directivetype = "kindfrequency"


class FrequencyDocumenter(_RuleDocumenter[Frequency], target=Frequency):
    objtype = directivetype = "frequency"


class ActivityKindDocumenter(_KindDocumenter[Activity], target=Activity):
    objtype = directivetype = "kindactivity"

    def generate_table_of_children(self):
        for line in generate_table(
            self.get_child_table_headers(),
            sorted(
                self.get_row_from_child(child_identifier)
                for child_identifier in self.documenter_adapter.get_children(
                    self.documented_object_identifier
                )
            ),
            content_indent=self.content_indent,
        ):
            self.yield_line(line)

    def document_members(self) -> None:
        """
        Run child documenters.

        For each child constituent, this method will pick the right
        documenter, create it and generate its documentation nested in
        the one from this documenter.

        Notes:
            The need to overwrite this property in subclasses is unlikely.
        """

        for child in self.documenter_adapter.get_children(
            self.documented_object_identifier
        ):
            if (
                self.documenter_adapter.get_object(child, Activity).sourcefile.stem
                != "activity"
            ):
                continue
            documenter_class = self.documenter_adapter.get_documenter_class(child)
            if issubclass(documenter_class, ConstituentDocumenter):
                documenter = documenter_class(  # type: ignore
                    self.bridge, child, self.documenter_adapter, self.indent  # type: ignore
                )
                documenter.generate()
            else:
                raise RuntimeError(
                    f"Documenter of child '{child}' was of unexpected "
                    f"type '{documenter_class}'."
                )


class ActivityDocumenter(_RuleDocumenter[Activity], target=Activity):
    objtype = directivetype = "activity"

    def generate_content(self) -> None:
        super().generate_content()

        activity = self.documenter_adapter.get_object(
            self.documented_object_identifier, Activity
        )

        success = {}
        if activity.critical_success:
            success["Critical success"] = activity.critical_success
        if activity.success:
            success["Success"] = activity.success
        if activity.failure:
            success["Failure"] = activity.failure
        if activity.critical_failure:
            success["Critical failure"] = activity.critical_failure

        for line in generate_datafields(
            tuple(success.keys()),
            tuple(success.values()),
        ):
            self.yield_line(line)

        for effect in activity.effects:
            self.document_object(effect)

        for line in generate_datafields(
            [key.title() for key in activity.sample_tasks.keys()],
            tuple(activity.sample_tasks.values()),
            f"Sample {activity.name} tasks",
        ):
            self.yield_line(line)

        if activity.additional_description:
            for line in activity.additional_description.splitlines():
                self.yield_line(line)

    def get_data_fields(self):
        activity = self.documenter_adapter.get_object(
            self.documented_object_identifier, Activity
        )
        fields = {}

        if activity.requirement:
            fields["Requirement"] = activity.requirement

        if activity.trigger:
            fields["Trigger"] = activity.trigger

        if activity.frequency:
            fields["Frequency"] = activity.frequency

        return fields


class AmmunitionKindDocumenter(_KindDocumenter[Ammunition], target=Ammunition):
    objtype = directivetype = "kindammunition"


class AmmunitionDocumenter(_RuleDocumenter[Ammunition], target=Ammunition):
    objtype = directivetype = "ammunition"


class GearKindDocumenter(_KindDocumenter[Gear], target=Gear):
    objtype = directivetype = "kindgear"


class GearDocumenter(_RuleDocumenter[Gear], target=Gear):
    objtype = directivetype = "gear"


class ArmorKindDocumenter(_KindDocumenter[Armor], target=Armor):
    objtype = directivetype = "kindarmor"


class ArmorDocumenter(_RuleDocumenter[Armor], target=Armor):
    objtype = directivetype = "armor"


class WeaponKindDocumenter(_KindDocumenter[Weapon], target=Weapon):
    objtype = directivetype = "kindweapon"


class WeaponDocumenter(_RuleDocumenter[Weapon], target=Weapon):
    objtype = directivetype = "weapon"


class SizeKindDocumenter(_KindDocumenter[Size], target=Size):
    objtype = directivetype = "kindsize"


class SizeDocumenter(_RuleDocumenter[Size], target=Size):
    objtype = directivetype = "size"


class LanguageRarityKindDocumenter(
    _KindDocumenter[LanguageRarity], target=LanguageRarity
):
    objtype = directivetype = "kindlanguagerarity"


class LanguageRarityDocumenter(_RuleDocumenter[LanguageRarity], target=LanguageRarity):
    objtype = directivetype = "languagerarity"


class LanguageKindDocumenter(_KindDocumenter[Language], target=Language):
    objtype = directivetype = "kindlanguage"


class LanguageDocumenter(_RuleDocumenter[Language], target=Language):
    objtype = directivetype = "language"


class SenseKindDocumenter(_KindDocumenter[Sense], target=Sense):
    objtype = directivetype = "kindsense"


class SenseDocumenter(_RuleDocumenter[Sense], target=Sense):
    objtype = directivetype = "sense"


class SkillKindDocumenter(_KindDocumenter[Skill], target=Skill):
    objtype = directivetype = "kindskill"

    def get_child_table_headers(self) -> Sequence[str]:
        return "Name", "Key Ability", "Description"

    def get_row_from_child(self, child: ObjectIdentifier) -> Sequence[str]:
        rule: Skill = self.documenter_adapter.get_object(child, Skill)
        return (
            rule.rst_role(),
            rule.key_ability.rst_role(),
            rule.description
            # textwrap.shorten(rule.description, width=50, placeholder="...")
        )


class SkillDocumenter(_RuleDocumenter[Skill], target=Skill):
    objtype = directivetype = "skill"

    def generate_content(self) -> None:
        super().generate_content()
        skill = self.documenter_adapter.get_object(
            self.documented_object_identifier, Skill
        )

        if skill.trained_activities:
            for line in generate_rubric(
                "", f"{skill.name} trained actions", make_footer=False
            ):
                self.yield_line(line)

            if skill.trained_activities_description:
                for line in skill.trained_activities_description.splitlines():
                    self.yield_line(line)

            for activity in skill.trained_activities:
                self.document_object(activity)

        if skill.untrained_activities:
            for line in generate_rubric(
                "", f"{skill.name} untrained actions", make_footer=False
            ):
                self.yield_line(line)

            if skill.untrained_activities_description:
                for line in skill.untrained_activities_description.splitlines():
                    self.yield_line(line)

            for activity in skill.untrained_activities:
                self.document_object(activity)

    def get_data_fields(self) -> dict[str, str]:
        skill = self.documenter_adapter.get_object(
            self.documented_object_identifier, Skill
        )

        return {
            "Key Ability": skill.key_ability.rst_role(),
            "Trained Actions": ", ".join(
                [activity.rst_role() for activity in skill.trained_activities]
            ),
            "Untrained Actions": ", ".join(
                [activity.rst_role() for activity in skill.untrained_activities]
            ),
        }


class ConditionKindDocumenter(_KindDocumenter[Condition], target=Condition):
    objtype = directivetype = "kindcondition"


class ConditionDocumenter(_RuleDocumenter[Condition], target=Condition):
    objtype = directivetype = "condition"


class MovementTypeKindDocumenter(_KindDocumenter[MovementType], target=MovementType):
    objtype = directivetype = "kindmovementtype"


class MovementTypeDocumenter(_RuleDocumenter[MovementType], target=MovementType):
    objtype = directivetype = "movementtype"


class BonusTypeKindDocumenter(_KindDocumenter[BonusType], target=BonusType):
    objtype = directivetype = "kindbonustype"


class BonusTypeDocumenter(_RuleDocumenter[BonusType], target=BonusType):
    objtype = directivetype = "bonustype"


class FeatKindDocumenter(_KindDocumenter[Feat], target=Feat):
    objtype = directivetype = "kindfeat"


class FeatDocumenter(_RuleDocumenter[Feat], target=Feat):
    objtype = directivetype = "feat"


class AncestryKindDocumenter(_KindDocumenter[Ancestry], target=Ancestry):
    objtype = directivetype = "kindancestry"


class AncestryDocumenter(_RuleDocumenter[Ancestry], target=Ancestry):
    objtype = directivetype = "ancestry"


class HeritageKindDocumenter(_KindDocumenter[Heritage], target=Heritage):
    objtype = directivetype = "kindheritage"


class HeritageDocumenter(_RuleDocumenter[Heritage], target=Heritage):
    objtype = directivetype = "heritage"


class BackgroundKindDocumenter(_KindDocumenter[Background], target=Background):
    objtype = directivetype = "kindbackground"


class BackgroundDocumenter(_RuleDocumenter[Background], target=Background):
    objtype = directivetype = "background"


class SpellSchoolKindDocumenter(_KindDocumenter[SpellSchool], target=SpellSchool):
    objtype = directivetype = "kindspellschool"


class SpellSchoolDocumenter(_RuleDocumenter[SpellSchool], target=SpellSchool):
    objtype = directivetype = "spellschool"


class SpellComponentKindDocumenter(
    _KindDocumenter[SpellComponent], target=SpellComponent
):
    objtype = directivetype = "kindspellcomponent"


class SpellComponentDocumenter(_RuleDocumenter[SpellComponent], target=SpellComponent):
    objtype = directivetype = "spellcomponent"


class SpellTraditionKindDocumenter(
    _KindDocumenter[SpellTradition], target=SpellTradition
):
    objtype = directivetype = "kindspelltradition"


class SpellTraditionDocumenter(_RuleDocumenter[SpellTradition], target=SpellTradition):
    objtype = directivetype = "spelltradition"


class SpellTypeKindDocumenter(_KindDocumenter[SpellType], target=SpellType):
    objtype = directivetype = "kindspelltype"


class SpellTypeDocumenter(_RuleDocumenter[SpellType], target=SpellType):
    objtype = directivetype = "spelltype"


class SavingThrowKindDocumenter(_KindDocumenter[SavingThrow], target=SavingThrow):
    objtype = directivetype = "kindsavingthrow"


class SavingThrowDocumenter(_RuleDocumenter[SavingThrow], target=SavingThrow):
    objtype = directivetype = "savingthrow"


class SpellKindDocumenter(_KindDocumenter[Spell], target=Spell):
    objtype = directivetype = "kindspell"


class SpellDocumenter(_RuleDocumenter[Spell], target=Spell):
    objtype = directivetype = "spell"


class ClassKindDocumenter(_KindDocumenter[Class], target=Class):
    objtype = directivetype = "kindclass"


class ClassDocumenter(_RuleDocumenter[Class], target=Class):
    objtype = directivetype = "class"


class StaffKindDocumenter(_KindDocumenter[Staff], target=Staff):
    objtype = directivetype = "kindstaff"


class StaffDocumenter(_RuleDocumenter[Staff], target=Staff):
    objtype = directivetype = "staff"


class SourceFileKindDocumenter(_KindDocumenter[SourceFile], target=SourceFile):
    objtype = directivetype = "kindsourcefile"


class SourceFileDocumenter(_RuleDocumenter[SourceFile], target=SourceFile):
    objtype = directivetype = "sourcefile"


class RuleKindDocumenter(_KindDocumenter[Rule], target=Rule):
    objtype = directivetype = "kindrule"


class RuleDocumenter(_RuleDocumenter[Rule], target=Rule):
    objtype = directivetype = "rule"


def generate_rubric(
    content: str,
    title: str,
    content_intent: str = CONTENT_INDENT,
    make_footer: bool = True,
) -> Iterator:
    yield ""
    yield f".. rubric:: {title}"
    yield ""
    yield from content.splitlines()
    yield ""
    if make_footer:
        yield from _generate_directive_footer(title, content_indent=content_intent)


def generate_table(
    headers: Sequence[str],
    values: Sequence[Sequence[str]],
    title: str | None = None,
    options: dict[str, str] | None = None,
    content_indent: str = CONTENT_INDENT,
) -> Iterator[str]:
    if any(len(row) != len(headers) for row in values):
        raise ValueError(
            f"One row do not have the same length as headers " f"({len(headers)})."
        )

    if not headers or not values:
        return ()

    options = options or {}
    options.setdefault("header-rows", "1")
    options.setdefault("widths", "auto")
    options.setdefault("widths", "auto")
    options.setdefault("class", "datatable")

    yield from _generate_table(
        table=[headers, *values],
        title=title,
        options=options,
        content_indent=content_indent,
    )


def generate_datafields(
    fields: Sequence[str],
    values: Sequence[str],
    title: str | None = None,
    options: dict[str, str] | None = None,
    content_indent: str = CONTENT_INDENT,
) -> Iterator[str]:
    if not fields or not values:
        return ()
    options = options or {}

    options.setdefault("widths", "30 70")
    options.setdefault("stub-columns", "1")
    options.setdefault("header-rows", "0")

    if len(fields) != len(values):
        raise ValueError(
            f"There should be as many fields ({len(fields)} as values "
            f"({len(values)})."
        )

    yield from _generate_table(
        table=list(zip(fields, values)),
        title=title,
        options=options,
        content_indent=content_indent,
    )


def _generate_table(
    table: Sequence[Sequence[str]],
    title: str | None = None,
    footer_comment: str = "",
    options: dict[str, str] | None = None,
    content_indent: str = CONTENT_INDENT,
):
    yield from _generate_list_header(
        title=title, options=options, content_indent=content_indent
    )
    yield from _generate_list_content(table=table, content_indent=content_indent)
    yield from _generate_directive_footer(
        title="table", comment=footer_comment, content_indent=content_indent
    )


def _generate_list_header(
    title: str | None = None,
    options: dict[str, str] | None = None,
    content_indent: str = CONTENT_INDENT,
) -> Iterator[str]:
    options = options or {}
    yield f".. list-table:: {title}" if title else ".. list-table::"
    for name, value in options.items():
        yield f"{content_indent}:{name}: {value}"
    yield ""


def _generate_list_content(
    table: Sequence[Sequence[str]],
    content_indent: str = CONTENT_INDENT,
):
    lenghts = {len(i) for i in table}
    if not len(lenghts) == 1:
        raise ValueError(f"Some rows have different lenghts {lenghts}")

    for row in table:
        yield f"{content_indent}*"
        yield ""
        first_line_prefix = f"{content_indent * 2}*{' ' * (len(CONTENT_INDENT) - 1)}"
        next_line_prefix = f"{content_indent * 2}{' ' * len(CONTENT_INDENT)}"
        line_prefix = first_line_prefix
        for cell in row:
            markup_lines = str(cell).splitlines() if cell else [""]
            for line in markup_lines:
                yield f"{line_prefix}{line}"
                line_prefix = next_line_prefix
            line_prefix = first_line_prefix
            yield ""


def _generate_directive_footer(
    title: str, comment: str = "", content_indent: str = CONTENT_INDENT
) -> Iterator[str]:
    yield ""
    yield f".. end of {title}"
    if comment:
        yield ".."
        for line in comment.splitlines():
            yield f"{content_indent}{line}"
    yield ""


objtypes_to_documenter = {
    "kindpublisher": PublisherKindDocumenter,
    "publisher": PublisherDocumenter,
    "kindsource": SourceKindDocumenter,
    "source": SourceDocumenter,
    "kindtraittype": TraitTypeKindDocumenter,
    "traittype": TraitTypeDocumenter,
    "kindtrait": TraitKindDocumenter,
    "trait": TraitDocumenter,
    "kindalignment": AlignmentKindDocumenter,
    "alignment": AlignmentDocumenter,
    "kindabilityscore": AbilityScoreKindDocumenter,
    "abilityscore": AbilityScoreDocumenter,
    "kindactioncategory": ActionCategoryKindDocumenter,
    "actioncategory": ActionCategoryDocumenter,
    "kindactioncost": ActionCostKindDocumenter,
    "actioncost": ActionCostDocumenter,
    "kindeffect": EffectKindDocumenter,
    "effect": EffectDocumenter,
    "kindbulk": BulkKindDocumenter,
    "bulk": BulkDocumenter,
    "kinditemcategory": ItemCategoryKindDocumenter,
    "itemcategory": ItemCategoryDocumenter,
    "kindarmorcategory": ArmorCategoryKindDocumenter,
    "armorcategory": ArmorCategoryDocumenter,
    "kindarmorgroup": ArmorGroupKindDocumenter,
    "armorgroup": ArmorGroupDocumenter,
    "kindweapongroup": WeaponGroupKindDocumenter,
    "weapongroup": WeaponGroupDocumenter,
    "kindweaponcategory": WeaponCategoryKindDocumenter,
    "weaponcategory": WeaponCategoryDocumenter,
    "kinddamagecategory": DamageCategoryKindDocumenter,
    "damagecategory": DamageCategoryDocumenter,
    "kinddamagetype": DamageTypeKindDocumenter,
    "damagetype": DamageTypeDocumenter,
    "kindfrequency": FrequencyKindDocumenter,
    "frequency": FrequencyDocumenter,
    "kindactivity": ActivityKindDocumenter,
    "activity": ActivityDocumenter,
    "kindammunition": AmmunitionKindDocumenter,
    "ammunition": AmmunitionDocumenter,
    "kindgear": GearKindDocumenter,
    "gear": GearDocumenter,
    "kindarmor": ArmorKindDocumenter,
    "armor": ArmorDocumenter,
    "kindweapon": WeaponKindDocumenter,
    "weapon": WeaponDocumenter,
    "kindsize": SizeKindDocumenter,
    "size": SizeDocumenter,
    "kindlanguagerarity": LanguageRarityKindDocumenter,
    "languagerarity": LanguageRarityDocumenter,
    "kindlanguage": LanguageKindDocumenter,
    "language": LanguageDocumenter,
    "kindsense": SenseKindDocumenter,
    "sense": SenseDocumenter,
    "kindskill": SkillKindDocumenter,
    "skill": SkillDocumenter,
    "kindcondition": ConditionKindDocumenter,
    "condition": ConditionDocumenter,
    "kindmovementtype": MovementTypeKindDocumenter,
    "movementtype": MovementTypeDocumenter,
    "kindbonustype": BonusTypeKindDocumenter,
    "bonustype": BonusTypeDocumenter,
    "kindfeat": FeatKindDocumenter,
    "feat": FeatDocumenter,
    "kindancestry": AncestryKindDocumenter,
    "ancestry": AncestryDocumenter,
    "kindheritage": HeritageKindDocumenter,
    "heritage": HeritageDocumenter,
    "kindbackground": BackgroundKindDocumenter,
    "background": BackgroundDocumenter,
    "kindspellschool": SpellSchoolKindDocumenter,
    "spellschool": SpellSchoolDocumenter,
    "kindspellcomponent": SpellComponentKindDocumenter,
    "spellcomponent": SpellComponentDocumenter,
    "kindspelltradition": SpellTraditionKindDocumenter,
    "spelltradition": SpellTraditionDocumenter,
    "kindspelltype": SpellTypeKindDocumenter,
    "spelltype": SpellTypeDocumenter,
    "kindsavingthrow": SavingThrowKindDocumenter,
    "savingthrow": SavingThrowDocumenter,
    "kindspell": SpellKindDocumenter,
    "spell": SpellDocumenter,
    "kindclass": ClassKindDocumenter,
    "class": ClassDocumenter,
    "kindstaff": StaffKindDocumenter,
    "staff": StaffDocumenter,
    "kindsourcefile": SourceFileKindDocumenter,
    "sourcefile": SourceFileDocumenter,
    "kindrule": RuleKindDocumenter,
    "rule": RuleDocumenter,
}
